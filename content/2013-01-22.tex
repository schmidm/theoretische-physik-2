% Henri Menke, Michael Schmid, Marcel Klett 2013 Universität Stuttgart.
%
% Dieses Werk ist unter einer Creative Commons Lizenz vom Typ
% Namensnennung - Nicht-kommerziell - Weitergabe unter gleichen Bedingungen 3.0 Deutschland
% zugänglich. Um eine Kopie dieser Lizenz einzusehen, konsultieren Sie
% http://creativecommons.org/licenses/by-nc-sa/3.0/de/ oder wenden Sie sich
% brieflich an Creative Commons, 444 Castro Street, Suite 900, Mountain View,
% California, 94041, USA.

%
% Vorlesung am 22.01.2013
%
\renewcommand{\printfile}{2013-01-22}

Auch wenn uns die analytische Betrachtung von einigen quantenmechanischen Problemen gelungen ist, sind jedoch komplexere Probleme nicht mehr exakt lösbar, sondern müssen mit Hilfe von Näherungsmethoden untersucht werden.

\section{Variationsrechnung}

Der Grundgedanke der Näherungsmethode der \acct{Variationsrechung} basiert im Grunde darauf, dass die >>beste Annäherung<< an die Grundzustandsenergie und Wellenfunktion durch Optimierung eines geeigneten Ansatzes erzielt wird. 

\begin{theorem*}
Sei $\psi$ ein nicht normierter Vektor aus dem Hilbertraum $\mathcal{H}$. Dann gilt
\[
  \bar{H} \equiv \frac{\Braket{\psi|H|\psi}}{\Braket{\psi|\psi}} \geq E_0 \qquad \text{$E_0$ entspricht Grundzustandsenergie} .
\]

\begin{proof*}
\[
  H \Ket{n} = E_n \Ket{n}, \quad \Ket{\psi} = \sum\limits_{n} a_n \Ket{n}, \quad \text{mit} \quad a_n \equiv \Braket{n|\psi}.
\]
Somit folgt für $\bar{H}$:
%
\begin{align*}
  \bar{H} &= \frac{\sum\limits_{m,n} a_m^\ast a_n \Braket{m|H|n} }{\sum\limits_{k}a_k a_k^\ast} \\
  &= \frac{\sum\limits_{n}|a_n|^2 E_n}{\sum\limits_{n}|a_n|^2} 
\intertext{Mit $n=0,1,2,\ldots$ folgt:}
  &= \frac{E_0 \sum\limits_{n=0}|a_n|^2 + \sum\limits_{n=0}\left(E_n - E_0 \right)^2 |a_n|^2  }{\sum\limits_{n}|a_n|^2} \\
  &= E_0 + \frac{\sum\limits_{n=0}\left(E_n - E_0\right)^2 |a_n|^2}{\sum\limits_{n}|a_n|^2} \\
  &\geq E_0
\end{align*}
Damit wäre der Beweis erbracht.
\end{proof*}
\end{theorem*}

Unsere Strategie zum Auffinden möglichst guter Ergebnisse ist also:
\begin{enum-roman}
  \item Parametrisierung von $\Ket{\psi}$ mit Variationsparametern $\left\{ \alpha_i \right\}$:
    \[
      \Ket{\psi} = \Ket{\psi\left(\left\{ \alpha_i \right\}\right)} .
    \] 
  \item $\bar{H} = \bar{H}\left(\{\alpha_i\}\right)$ aufstellen nach obiger Formel und das Extremum bestimmen:
    \[
      \frac{\partial \bar{H}}{\partial \alpha_j}\bigg|_{\{\alpha_i^\ast\}} \stackrel{!}{=} 0 \qquad \forall j.
    \]
  \item Somit zeigt sich, dass
    \[
      E^\ast \equiv \bar{H}\left(\left\{ \alpha_i^\ast \right\}\right)
    \]
  die Beste Abschätzung an $E_0$ ist.
\end{enum-roman}

\begin{notice*}
Ein Fehler in der Wellenfunktion äußert sich bei diesem Verfahren in quadratischer Ordnung in der Energie. Sei 
\[
  \Ket{\psi} = \Ket{0} + \mathcal{O}(\varepsilon^2),
\]
dann gilt
\[
  \bar{H} = E_0 + \mathcal{O}(\varepsilon^2).
\]
Das heißt, dass die Energie beim Variationsproblem >>besser<< bestimmt wird als der Zustand selber.
\end{notice*}

\begin{example*} Unendlich hoher Potentialkasten.

Wie wir bereits aus früherer Betrachtung dieses Problems wissen, ergibt sich für die Grundzustandsfunktion $\phi_0$ und der Grundzustandsenergie $E_0$:
\[
  \phi_0(x) = \frac{1}{\sqrt{a}} \cos\left(\frac{x}{2a} \pi\right), \quad  E_0 = \frac{\hbar^2}{2m}\left(\frac{\pi}{2 a}\right)^2.
\]

\begin{figure}[H]
  \centering
  \begin{pspicture}(-2,-0.5)(2,2)
    \psaxes[labels=none,ticks=none]{->}(0,0)(-2,-0.5)(2,2)[\color{DimGray} $x$, 0][\color{DimGray} $V(x)$, 0]
    \psframe[linestyle=none,fillstyle=hlines,hatchcolor=DimGray](-1.5,0)(-1.8,2)
    \psframe[linestyle=none,fillstyle=hlines,hatchcolor=DimGray](1.5,0)(1.8,2)
    \psline(-1.5,0)(-1.5,2)
    \psline(1.5,0)(1.5,2)
    \psxTick(-1.5){\color{DimGray} -a}
    \psxTick(1.5){\color{DimGray} a}
    \psline[linecolor=DarkOrange3](-1.5,0)(0,1.5)(1.5,0)
    \psplot[linecolor=MidnightBlue]{-1.5}{1.5}{-2/3*x^2+1.5}
  \end{pspicture}
\end{figure}

Wir verwenden folgenden Variationsansatz:
\[
  \psi(\alpha_1,x) = |a|^{\alpha_1} - |x|^{\alpha_1}.
\]
Wir erwarten einen Wert zwischen $1$ und $2$ für $\alpha$, da $\cos(x) \approx 1- x^2/2 + \ldots$ ist. Durch Verwendung der obigen Formel erhalten wir für $\bar{H}$:
\[
  \bar{H}(\alpha_1) = \frac{\hbar^2}{8 m a^2} \frac{\left(2 \alpha_1 +2\right)\left(2 \alpha_1 + 1\right)}{\left(2 \alpha_1 -1\right)}.
\]
Für die Bestimmung der Extremums gilt:
\[
  \frac{\partial \bar{H}}{\partial \alpha_1} = 0 \quad  \implies \quad \alpha_1^\ast = \frac{1+\sqrt{6}}{2} \approx 1.72. 
\]
Somit erhalten wir für $E_0^\ast$:
\[
  E_0^\ast = \bar{H}(\alpha_1^\ast) = 1.00298 \cdot E_0 .  
\]
Es zeigt sich also, dass der verwendete Ansatz eine gute Wahl war, da nur eine geringe Abweichung von $E_0$ vorhanden ist.
\begin{notice*}
\[
  \Braket{\psi|H|\psi}  = \int\limits_{-a}^{a} \psi(x)^\ast \left(-\frac{\hbar^2}{2m}\partial_x^2\right) \psi(x) \;\mathrm{d}x
\]
\end{notice*}
In diesen speziell gewählten Beispiel, hätten wir auch direkt einen Kosinus als Ansatz wählen können. Die Lösung entspricht dann der analytischen von oben.
\end{example*}
%
\section{Zustandsabhängige Störungstheorie}
%
Für $H = H_0 + \lambda H_1$ mit $H_0$ exakt lösbar und $\lambda << 1$ gibt es perturbative Lösungen.
%
\subsection{Nicht-entarteter Fall}
Gegeben sei:
\[
  \left(H_0 - E_\alpha\right) \Ket{\alpha} = 0
\]
mit $E_\alpha$ nicht entartet. Wir suchen
\[
  \left(H-E_1\right) \Ket{a} = 0 \qquad \text{\color{Gray} gestörtes Eigenwertproblem}
\]
mit $\lim\limits_{\lambda \to 0} \Ket{a} = \Ket{\alpha}$. Wir suchen also die Eigenzustände und Eigenenergien. Dazu entwickeln wir zunächst $\Ket{a}$:
\[
  \Ket{a} = c_\alpha \Ket{\alpha} + \sum\limits_{\beta \neq \alpha} d_\beta \Ket{\beta}. 
\]
Hierbei hat $d_\beta$ die Ordnung von $\lambda$ $(d_\beta = \mathcal{O}(\lambda))$.

Für die Normierung ergibt sich:
%
\begin{align*}
  \Braket{a|a} &= 1 \\
  &\implies |c_\alpha|^2 + \sum\limits_{\beta \neq \alpha} |d_\beta|^2 = 1 \\
  &\implies c_\alpha = 1- \mathcal{O}(\lambda^2) 
\end{align*}
%
Somit gilt:
%
\begin{align*}
  \left(H - E_a\right) \Ket{a} &= \left(H_0 + \lambda H_1 - E_a\right)\left[ c_\alpha \Ket{\alpha} + \sum\limits_{\beta \neq \alpha} d_\beta \Ket{b} \right] & \bigg|\Bra{\gamma\neq \alpha} \\
  &= 0 + \lambda \Braket{\gamma|h_1|\alpha} + \sum\limits_{\beta \neq \alpha} d_\beta \left[E_\beta \delta{\gamma,\beta} + \lambda \Braket{\gamma|H_1|\beta} - E_a \delta_{\gamma,\beta}\right]
\end{align*}
%
In der Ordnung von $\lambda$ folgt:
%
\begin{align*}
  \lambda \Braket{\gamma|H_1|\alpha} + d_\gamma \left(E_\gamma -E_a \right) &= 0 \\
  \implies d_\gamma &= \frac{\Braket{\gamma|H_1|\alpha}}{E_a - E_\gamma} \lambda + \mathcal{O}(\lambda^2) \\
  &= \frac{\Braket{\gamma|H_1|\alpha}}{E_\alpha - E_\gamma} \lambda + \mathcal{O}(\lambda^2) 
\end{align*}
%
Hierbei wurde verwendet, dass $\lim\limits_{\lambda \to 0} E_a = E_\alpha$. Damit ergibt sich weiter:
\[
  \Ket{a} = \Ket{\alpha} + \lambda \sum\limits_{\beta \neq \alpha} \frac{\Braket{\beta|H_1|\alpha}}{E_\alpha - E_\beta} \Ket{\beta} + \mathcal{O}(\lambda^2).
\]

\begin{notice*}[Zur Näherung:]
\[
  \frac{1}{x+ c\lambda} = \frac{1}{x\left(1+ \frac{c \lambda}{x}\right)} \approx \frac{1}{x} \left(1- \frac{c\lambda}{x}\right) = \frac{1}{x} - \mathcal{O}(\lambda)
\]
\end{notice*}

\begin{theorem*}[Notwendige Bedingung]
\[
  \left| \lambda \frac{\Braket{\beta|H|\alpha}}{E_\alpha - E_\beta}\right| \ll 1 
\]
\end{theorem*}
%
Energie:
%
\begin{align*}
  0 &= \Braket{\alpha|H-E_a|a} \\
  &= \Braket{\alpha|H_0 + \lambda H_1 - E_a | \alpha + \lambda \sum\limits_{\beta \neq \alpha} \frac{\Braket{\beta|H_1|\alpha}}{E_\alpha - E_\beta} + \mathcal{O}(\lambda^2)}
\end{align*}
%
Dies führt dann zu:
\[
  \boxed{ E_a = E_\alpha + \lambda \Braket{\alpha|H_1|\alpha} + \lambda^2 \sum\limits_{\beta \neq \alpha}  \frac{\left|{\Braket{\beta|H_1|\alpha}}\right|^2}{E_\alpha - E_\beta} + \mathcal{O}(\lambda^3) }
\]
%
Als \acct{Matrixelement der Störung} wir der Term
\[
  \ldots + \lambda \Braket{\alpha|H_1|\alpha} + \ldots
\] 
bezeichnet.

\minisec{Fazit:}
\begin{enum-arab}
  \item Führende Ordnung ist das Matrixelement der Störung
  \item Falls das aus Symmetriegründen verschwindet, ist der $\lambda^2$-Term relevant
\end{enum-arab}
