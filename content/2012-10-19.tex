% Henri Menke, Michael Schmid, Marcel Klett, 2012 Universität Stuttgart.
%
% Dieses Werk ist unter einer Creative Commons Lizenz vom Typ
% Namensnennung - Nicht-kommerziell - Weitergabe unter gleichen Bedingungen 3.0 Deutschland
% zugänglich. Um eine Kopie dieser Lizenz einzusehen, konsultieren Sie
% http://creativecommons.org/licenses/by-nc-sa/3.0/de/ oder wenden Sie sich
% brieflich an Creative Commons, 444 Castro Street, Suite 900, Mountain View,
% California, 94041, USA.

%
% Vorlesung am 19.10.2012
%
\renewcommand{\printfile}{2012-10-19}

\section{Mathematische Bühne: Hilbert-Raum (endliche Dimension)}

\subsection{Definition}

$\mathcal{H}$ ist ein linearer Vektorraum mit Vektoren $\{ \ket{\phi}, \ket{\psi}, \ldots \}$ über den komplexen Zahlen.
%
\begin{align*}
  c_1 \ket{\phi} + c_2 \ket{\psi} \in \mathcal{H}
\end{align*}
%
Es existiert ein inneres (hermitsches, skalares) Produkt $\mathcal{H} \times \mathcal{H} \to \mathbb{C}$
%
\begin{align*}
  \Braket{\phi|\psi} = \Braket{\psi|\phi}^*
\end{align*}
% 
mit den Eigenschaften:
%
\begin{enum-arab}
  \item linear im 2. Argument 
  %
  \begin{align*}
    \Braket{\phi | c_1 \psi + c_2 \chi} = c_1 \Braket{\phi | \psi} + c_2 \Braket{\phi | \chi}
  \end{align*}
  %
  \item $\underbrace{\Braket{\phi | \phi}}_{\in \mathbb{R}} > 0 \quad \text{, falls} \quad \Ket{\phi} \neq \Ket{0}$
  %
  \item Daraus folgt Antilinearität im 1. Argument 
  %
  \begin{align*}
    \Braket{c\phi|\psi} =\Braket{\psi|c\phi}^* = \left( c\Braket{\psi|\phi}\right)^* = c^* \Braket{\psi|\phi}^* = c^* \Braket{\phi|\psi}
  \end{align*}
\end{enum-arab}
%
Zudem gibt es eine orthonormierte Basis $\{\Ket{n}\} = \{\Ket{1},\Ket{2},\ldots, \Ket{N} \}$ bei $\dim \mathcal{H} = N$.

Mit $\Braket{n|m} = \delta_{nm}$ kann jeder Vektor entwickelt werden.
%
\begin{align*}
  \Ket{\psi} &= \sum\limits_{n=1}^{N} c_n \Ket{n} \\
  \Braket{m|\psi} &= \sum\limits_{n=1}^{N} c_n \Braket{m|n} = \sum\limits_{n=1}^{N} c_n \delta_{mn} = c_m \\
  \Ket{\psi} &= \sum\limits_{n=1}^{N} \Ket{n} \underbrace{\Braket{n|\psi}}_{c_n}
\end{align*}
%
Daraus folgt
%
\begin{align*}
  \Ket{\psi} &= \sum\limits_{n} c_n \Ket{n} \\
  \Ket{\phi} &= \sum\limits_{m} d_m \Ket{m} \\
  \Braket{\phi|\psi} &= \Braket{\sum\limits_{m} d_m \, m | \sum\limits_{n} c_n \, n} \\
  &= \sum\limits_{n} \sum\limits_{m} c_n \, d_m^* \underbrace{\Braket{m|n}}_{\delta_{mn}} \\
  &= \sum\limits_{m} c_m \, d_m^* .
\end{align*}
%
Die Norm von $\|\Ket{\phi}\|$ ist definiert als
%
\begin{align*}
  \|\Ket{\phi}\| = \|\phi\| = \sqrt{\Braket{\phi|\phi}} = \sqrt{\sum\limits_{n} d_n \, d_n^*}
\end{align*}

\minisec{Schwarz'sche Ungleichung}
%
\begin{align*}
  |\Braket{\chi|\phi}|^2 \leq \Braket{\chi|\chi} \Braket{\phi|\phi} = \|x\|^2  \, \|\phi\|^2
\end{align*}
%
mit Gleicheit, falls $\Ket{\chi}$ und $\Ket{\phi}$ linear abhängig sind, also $\Ket{\chi} = c \Ket{\phi}$

\begin{proof}
  \begin{enum-arab}
    \item falls $\Braket{\chi|\phi} = 0$ ist die Ungleichung erfüllt.
      
    \item $\Braket{\chi|\phi} \neq 0 \iff \Ket{\chi} \neq 0 \text{ und } \Ket{\phi} \neq 0$
    %
    \begin{align*}
      0 &\leq \Braket{\phi - \lambda \chi | \phi - \lambda} \\
      &= \Braket{\phi|\phi} - \lambda \Braket{\phi|\chi} - \lambda^* \Braket{\chi|\phi} + \lambda \lambda^* \Braket{\chi|\chi}
    \end{align*}
    %
    Wähle nun
    %
    \begin{gather*}
      \lambda = \frac{\|\phi\|^2}{\Braket{\phi|\chi}} \qquad \text{und} \qquad \lambda^* = \frac{\|\phi\|^2}{\Braket{\chi|\phi}}
    \end{gather*}
    %
    Einsetzen ergibt
    %
    \begin{gather*}
      0 \leq \|\phi\|^2 - 2 \|\phi\|^2 + \frac{\|\phi\|^4 \, \|\chi\|^2}{|\Braket{\phi|\chi}|^2} \\
      \|\phi\|^2 |\Braket{\phi|\chi}|^2 \leq \|\phi\|^4 \, \|\chi\|^2
    \end{gather*}
  \end{enum-arab}
\end{proof}

\subsection{Lineare Operatoren}

Sei $A$ ein \acct{linearer Operator} mit den Eigenschaften:
%
\begin{gather*}
  A\Ket{\phi} = \Ket{A\phi} \in \mathcal{H}
\intertext{mit}
  A\Ket{c_1 \phi + c_2\chi} = c_1 \Ket{A\phi} + c_2 \Ket{A\chi} .
\end{gather*}
%
Darstellung des Operators in der Basis
%
\begin{align*}
  A \Ket{\phi} &= A \Ket{\sum\limits_{n} c_n \, n} = \sum\limits_{n} c_n \Ket{A \, n} \\
  \Braket{m|A|\phi} &= \sum\limits_{n} c_n \underbrace{\Braket{m|A|n}}_{A_{mn}} = \sum\limits_{n} c_n A_{mn} ,
\end{align*}
%
wobei die $A_{mn}$ \acct{Matrixelemente} genannt werden.

Ein \acct{adjungierter Operator} von $A$ ($A^\dagger$) ist definiert durch
%
\begin{align*}
  \Braket{\phi|A^\dagger \psi} = \Braket{A \phi | \psi} = \Braket{\psi | A \phi}^* .
\end{align*}
%
Die Matrixelemente des adjungierten Operators lauten
%
\begin{align*}
  A^\dagger_{mn} &= \Braket{m|A^\dagger|n} = \Braket{A \, m|n} = \Braket{n|A \, m}^* = A_{nm}^*\\
  \left( A^\dagger \right)^\dagger &= A
\end{align*}
%
Das adjungierte Produkt ($(AB)^\dagger$) lautet
%
\begin{align*}
  \Braket{\phi | (AB)^\dagger \psi} &= \Braket{(AB) \, \phi | \psi} \\
  &= \Braket{B \, \phi | A^\dagger \, \psi} \\
  &= \Braket{\phi | B^\dagger A^\dagger \, \psi}
\end{align*}
%
Daraus folgt $(AB)^\dagger = B^\dagger A^\dagger$

\begin{theorem*}[Definition]
  A hermitesch $\iff$ $A^\dagger = A$ $\iff A_{nm} = A_{mn}^*$
  
  Aus dieser Definition folgt, dass die Diagonale reell sein muss.
\end{theorem*}

\subsection{Dirac-Notation}

Zusammenfassung der \acct{Dirac-Notation}
%
\begin{align*}
  \Ket{A\phi} &\hateq A\Ket{\phi} \\
  \Braket{\psi|A \phi} &\hateq \Braket{\psi|A|\phi} \\
  \Braket{A^\dagger \psi|\phi} &\hateq \Braket{\psi|A|\phi}
\end{align*}
%
Das heißt, nach rechts wirkt ein Operator >>normal<<, nach links wirkt der Operator als adjungierter. Für hermitesche Operatoren ist das egal, weil $A = A^\dagger$.
%
\begin{notice*}[Merke:]
  Wird ein Ket zu einem Bra, so werden komplexe Zahlen konjugiert und Operatoren adjungiert.
  %
  \begin{table}[H]
    \centering
    \begin{tabular}{cc}
      ket & bra \\
      \hline
      $\ket{\phi}$ & $\bra{\phi}$ \\
      $\ket{c_1 \, \phi}$ & $\bra{c_1^* \phi}$ \\
      $\ket{A \, \phi}$ & $\bra{A^\dagger \, \phi}$ \\
    \end{tabular}
  \end{table}
\end{notice*}
%
\minisec{Darstellung als Spalten und Zeilenvektoren}
%
\begin{gather*}
  \left\{ \Ket{1},\ldots,\Ket{n}\right\} \hateq 
  \left\{  \left(
  \begin{matrix}
    1 \\
    0 \\
    \vdots \\
    0
  \end{matrix}
  \right),
  \left(
  \begin{matrix}
    0 \\
    1 \\
    \vdots \\
    0
  \end{matrix} \right), \ldots ,
  \left(
  \begin{matrix}
    0 \\
    0 \\
    \vdots \\
    1
  \end{matrix} \right)
  \right\}
\end{gather*}
%
Damit folgt für zwei Vektoren $\ket{\psi}$ und $\ket{\phi}$
%
\begin{align*}
  \Ket{\psi} &= \sum\limits_{n=1}^{N} c_n \Ket{n} \hateq
  \left(
  \begin{matrix}
    c_1 \\
    c_2 \\
    \vdots \\
    c_n
  \end{matrix}
  \right) \\
  \Bra{\psi} &= \sum\limits_{m} d_m^* \Bra{m} \hateq \left(d_1^*, d_2^*,...., d_n^* \right)
\end{align*}
%
Mit den Regeln der Matrixmultiplikation (Zeile $\times$ Spalte) ergibt sich
%
\begin{align*}
  \Braket{\phi|\psi} &= \sum\limits_{n=1}^{N} c_n d_n^* .
\end{align*}
%
Operator $A$:
%
\begin{align*}
  A \Ket{\psi} &= \sum\limits_{n} c_n A \ket{n} \\
  \Braket{\phi|\psi} &= \sum\limits_{m,n} d_m^* \Braket{m|c_n A n} = \sum\limits_{m,n} d_m^* A_{mn} c_n \\ 
  &= \left( d_1^* , \ldots , d_N^* \right) \cdot
  \left(
  \begin{matrix}
    A_{11} & \ldots & A_{1N} \\
    \vdots & \ddots & \vdots \\
    A_{N1} & \ldots & A_{NN}
  \end{matrix}
  \right)
  \cdot
  \begin{pmatrix}
    c_1 \\
    \vdots \\
    c_N
  \end{pmatrix}
\end{align*}
%
Operatorprodukt
%
\begin{align*}
  (AB)_{mn} = A_{mk} B_{kn}
\end{align*}
