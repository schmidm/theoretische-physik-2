% Henri Menke, Michael Schmid, Marcel Klett, 2012 Universität Stuttgart.
%
% Dieses Werk ist unter einer Creative Commons Lizenz vom Typ
% Namensnennung - Nicht-kommerziell - Weitergabe unter gleichen Bedingungen 3.0 Deutschland
% zugänglich. Um eine Kopie dieser Lizenz einzusehen, konsultieren Sie
% http://creativecommons.org/licenses/by-nc-sa/3.0/de/ oder wenden Sie sich
% brieflich an Creative Commons, 444 Castro Street, Suite 900, Mountain View,
% California, 94041, USA.

%
%    Vorlesung 09.01.2013
%
\renewcommand{\printfile}{2013-01-09}

\section{Rotationsinvariante Probleme in drei Dimensionen}

\subsection{Drehimpulsalgebra} 

Im folgendem Abschnitt wollen wir die Eigenschaften des Drehimpuls untersuchen, was uns auf die \acct{Drehimpulsalgebra} führen wird. Als eine fundamentale Eigenschaft, aus der wir die meisten Resultate in diesem Abschnitt erzielen werden ist, dass der Drehimpuls (angular momentum) der infinitesimale Erzeuger der Rotation ist.

Allgemein ist die Drehung entlang einer Achse $\bm{n}$ um den Winkel $\phi$ definiert als:
\[
  \Ket{\widetilde{\psi}}=D\left(\phi,\bm{n}\right) \Ket{\psi} .
\]
Für kleine Winkel $\phi$ folgt die Näherung
%
\begin{gather*}
D\left(\phi,\bm{n}\right) \approx 1 - \frac{\mathrm{i}}{\hbar} \phi J_{\bm{n}} .
\end{gather*}
%
Zudem ist die Vertauschungsrelation $[J_x,J_y]=\hbar \mathrm{i} J_z$ gegeben (Zur Vereinfachung wird im folgenden $\hbar=1$ gesetzt). Diese Vertauschungsrelation bestimmt das Spektrum der $J$-Operatoren (und damit des Drehimpulses) vollständig.
%
\begin{notice*}
Es zeigt sich, dass die verschiedenen Drehimpulskomponenten $J_x$, $J_y$ und $J_z$ nicht miteinander kommutieren. Dies bedeutet aber, dass sie sich nicht gleichzeitig diagonalisieren lassen und somit kein gemeinsames Basissystem existiert. Um dies zu realisieren werden zunächst verschiedene Operatoren definiert.
\end{notice*}
%
\begin{theorem*}[Definition:]
%
Wir benutzen nun folgende Definitionen und Relationen:
%
\begin{align*}
  \bm{J}^2 &= J_x^2 + J_y^2 + J_z^2 \\
  \left[\bm{J}^2,J_{\alpha}\right] &= 0 \quad \text{für} \quad \alpha=x,y,z .
\end{align*}
%
Das heißt $\bm{J}^2=\bm{J}^2$ und $J_z$ haben eine gemeinsame Eigenbasis. Aus historischen Gründen wird immer $J_z$ gewählt.  
Für den weiteren Verlauf definieren wir einen Eigenzustand von $\bm{J}^2$ und $J_z$, so dass folgendes gilt:
%
\begin{align*}
  \bm{J}^2 \Ket{\alpha,\beta} &= \alpha \Ket{\alpha,\beta} \\
  J_z \Ket{\alpha,\beta} &= \beta  \Ket{\alpha,\beta} .
\end{align*}
%
Zudem führen wir zwei Leiteroperatoren $J_+$ und $J_-$ ein.
%
\begin{align*}
  J_+ &\coloneq J_x + \mathrm{i} J_y \\
  J_- &\coloneq J_x - \mathrm{i} J_y .
\end{align*}

\end{theorem*}

Anhand dieser Definitionen ergeben sich folgende Kommutatorrelationen:
%
\begin{align*}
  \left[\bm{J}^2,J_{\pm}\right] = \left[\bm{J}^2,J_x\right] \pm \mathrm{i} \left[\bm{J}^2,J_y\right] = 0
\end{align*}
%
Weiter gilt:
%
\begin{align*}
  \left[J_z,J_+\right] &= \left[J_z,J_x\right] + \mathrm{i} \left[J_z,J_y\right] \\
  &= \mathrm{i} J_y - \mathrm{i i} J_x \\
  &= J_x + \mathrm{i} J_y \\
  &= J_+ \\
  \left[J_z,J_-\right] &= \left[J_z,J_x\right] - \mathrm{i} \left[J_z,J_y\right] \\
  &= \mathrm{i} J_y + \mathrm{i i} J_x \\
  &= -J_x + \mathrm{i} J_y \\
  &= -J_- .
\end{align*}
%
$J_+$ und $J_-$ heißen Leiter- bzw. Erzeuger und Vernichter Operatoren, weil
%
\begin{align*}
  J_z \left(J_+ \Ket{\alpha,\beta} \right) &= J_+ J_z \Ket{\alpha,\beta}+ \left[J_z,J_+\right]\Ket{\alpha,\beta} \\
  &=  J_+ J_z \Ket{\alpha,\beta}+ J_+ \\
  \Ket{\alpha,\beta} &= (\beta+1) J_+ \Ket{\alpha,\beta}
\end{align*}
%
\begin{align*}
  \bm{J}^2  \left(J_+ \Ket{\alpha,\beta} \right) &= \alpha J_+ \Ket{\alpha,\beta} \\
  \bm{J}^2  \left(J_- \Ket{\alpha,\beta} \right) &= \alpha J_- \Ket{\alpha,\beta}
\end{align*}
%
Es wird also klar, dass $J_{\pm}\Ket{\alpha,\beta}$ ebenfalls Eigenfunktionen von $\bm{J}^2$ und $J_z$ sind. Da der Eigenwert von $J_z \left(J_+ \Ket{\alpha,\beta} \right) = (\beta+1)$ ist, lässt sich schließen, dass $J_+\Ket{\alpha,\beta} = c_+\left(\alpha,\beta\right) \Ket{\alpha,\beta+1}$ sein muss. Durch analoge Rechnung und Überlegung erhält man $J_-\Ket{\alpha,\beta} = c_-\left(\alpha,\beta\right) \Ket{\alpha,\beta-1}$. Dabei >>verschiebt<< $J_+$ den Drehimpuls Richtung z-Achse im Uhrzeigersinn und $J_-$ gegen den Uhrzeigersinn.
%
\begin{equation*}
  \Braket{\psi|J_x^2|\psi}= \left|J_x\Ket{\psi}\right|^2 \geq 0 \quad \text{für alle} \quad \Ket{\psi} .
\end{equation*}
%
Weiter zeigt sich:
%
\begin{align*}
  \Braket{\alpha,\beta|\bm{J}^2|\alpha,\beta} &= \alpha = \underbrace{\Braket{\alpha,\beta|J_x^2|\alpha,\beta}}_{a} +   \underbrace{\Braket{\alpha,\beta|J_y^2|\alpha,\beta}}_{b} + \underbrace{\Braket{\alpha,\beta|J_z^2|\alpha,\beta}}_{\beta^2} \\
  \alpha &= \underbrace{a + b}_{\geq 0 } + \beta^2 \quad \implies \alpha \geq \beta^2 .
\end{align*}
%
Es muss also ein $\beta_{\mathrm{max}}$ geben, so dass $J_+\Ket{\alpha, \beta_{\mathrm{max}}} = 0$. Dies ist genau dann der Fall, wenn $c_+(\alpha,\beta)=0$ ist. Daraus folgt (mit $J_+^\dagger=J_-$):
%
\begin{align*}
  J_+\Ket{\alpha, \beta_{\mathrm{max}}} &= 0 = \left|J_+\Ket{\alpha, \beta_{\mathrm{max}}}\right|^2 \\
  &= \Braket{\alpha, \beta_{\mathrm{max}}|J_- J_+|\alpha, \beta_{\mathrm{max}}} \\
  &= \Braket{\alpha, \beta_{\mathrm{max}}|\left(J_x-iJ_y\right)\left(J_x + iJ_y\right)|\alpha, \beta_{\mathrm{max}}} \\
  &= \Braket{\alpha, \beta_{\mathrm{max}}|J_x^2 + J_y^2 + i\left[J_x,J_y\right]|\alpha, \beta_{\mathrm{max}}} \\
  &= \Braket{\alpha, \beta_{\mathrm{max}}|\bm{J}^2-J_z^2-J_z|\alpha, \beta_{\mathrm{max}}} \\
  &= \alpha - \beta_{\mathrm{max}}^2 - \beta_{\mathrm{max}} \\
  \implies \alpha &= \beta_{\mathrm{max}} \left(\beta_{\mathrm{max}}+1\right)
\end{align*}
%
Mit gleicher Rechnung erhält man
%
\begin{equation*}
J_-\Ket{\alpha,\beta_{\mathrm{min}}} = 0 \ldots \implies \alpha = \beta_{\mathrm{min}} \left(\beta_{\mathrm{min}}-1\right)
\end{equation*}
%
Setzt man nun $\alpha=\alpha$, so erhält man
%
\begin{align*}
  \beta_{\mathrm{min}}^2 - \beta_{\mathrm{min}} = \beta_{\mathrm{max}}^2 + \beta_{\mathrm{max}} .
\end{align*}
%
Dies kann als eine quadratische Gleichung für $\beta_{\mathrm{max}}$ aufgefasst werden. Die Lösungen sind dabei $\beta_{\mathrm{max}} = \beta_{\mathrm{min}}-1$ und $\beta_{\mathrm{max}} = -\beta_{\mathrm{min}}$. Dabei macht die erste Lösung keinen Sinn, denn $\beta_{\mathrm{max}}$ wäre in dem Fall kleiner als $\beta_{\mathrm{min}}$! Zudem wissen wir, dass $\beta_{\mathrm{max}} - \beta_{\mathrm{min}} = k$ sein muss, wobei $k \in Z$. 
%
\begin{align*}
  \beta_{\mathrm{max}} + \beta_{\mathrm{max}} = \frac{k}{2}.
\end{align*}
%
Aus den vorigen Rechnungen wissen wir zudem, dass $\alpha= \beta_{\mathrm{max}} \left(\beta_{max} +1\right)$ gilt und damit auch $\alpha = \frac{k}{2} \left(\frac{k}{2}+1\right)$.
%
\begin{table}[H]
\centering
\begin{tabular}{c|ccc}
k & $\beta_{\mathrm{max}}$ & $\alpha$ & $\Ket{\alpha, \beta_{\mathrm{max}}}$ \\
\hline
$0$ & $0$            & $0$            & $\Ket{0,0}$ \\ 
$1$ & ${1}/{2}$  & ${3}/{4}$  & $\Ket{{3}/{4},{1}/{2}}$ \\ 
$2$ & $1$            & $2$            & $\Ket{2,1}$ \\ 
$3$ & ${3}/{2}$  & ${15}/{4}$ & $\Ket{{15}/{4},{3}/{2}}$ \\ 
\end{tabular}
\end{table}
%
\minisec{Fazit}
%
Für $J_z$ sind ${\hbar}/{2}$ und seine vielfachen Eigenwerte. Die allgemeine Notation sieht hierbei wie folgt aus:
%
\begin{align*}
  \alpha &= j(j+1)  \\
  \implies  \bm{J}^2\Ket{j,m}  &= \hbar^2 j(j+1) \Ket{j,m} \quad \left(j=0,\frac{1}{2},1,\ldots\right) \\
  \beta &= m \\
  \implies  J_z\Ket{j,m} &= \hbar m \Ket{j,m} \quad (|m|\leq j) \quad m=-j,\ldots, +j
\end{align*}
%
Betrachten wir nun die Matrixelemente
%
\begin{align*}
  J_+ \Ket{j,m} &= c_+ (j,m) \Ket{j,m+1} \\
  \left|c_+ (j,m) \Ket{j,m+1}\right|^2 &= \Braket{j,m|J_-J_+|j,m} \\
  \left|c_+ (j,m)\right|^2 &= \Braket{j,m|\bm{J}^2-J_z^2-\hbar J_z|j,m} \\
  \left|c_+ (j,m)\right|^2 &= \hbar^2\left(j(j+1) - m^2 -m\right) \\
  c_+ &= \hbar \sqrt{j(j+1)-m(m+1)} = \hbar \sqrt{(j+m+1)(j-m)}
\end{align*}
%
Analog erhält man $c_- =  \hbar \sqrt{(j-m+1)(j+m)}$
\[
  \boxed{J_+ \Ket{j,m} = \hbar \sqrt{(j - m)(j + m + 1)} \Ket{j,m + 1}}
\]
\[
  \boxed{J_- \Ket{j,m} = \hbar \sqrt{(j + m)(j - m + 1)} \Ket{j,m - 1}}
\]
Für die Matrixelemente von $J_x$ erhalten wir:
%
%                      Hier schön machen !!!!!!!!!!!!!!!1
%
\begin{align*}
  \Braket{j',m'|J_x|j,m} &= \Braket{j',m'|\frac{J_+ + J_-}{2}|j,m} \\
  &= \frac{\hbar}{2} \Big\{ \sqrt{(j-m) (j+m+1)} \delta_{m',m+1} \\
  &\qquad + \sqrt{(j+m)(j-m+1)} \delta_{m',m-1} \Big\} \delta_{j',j}.
\end{align*}
%
Hierbei haben wir die oben eingeführten Leiteroperatoren verwendet um folgende Relationen zu erhalten:
%
\begin{align*}
  J_x &= \frac{1}{2} \left(J_+ + J_-\right) \\
  J_y &= -\frac{\mathrm{i}}{2} \left(J_+ - J_-\right). 
\end{align*}
%
Analog zur obigen Formel zur Bestimmung der Matrixelemente für $J_x$ kann auch die Formel für die Bestimmung der Matrixelemente von $J_y$ hergeleitet werden. Für $J_y$ gilt also:
%
\begin{align*}
  \Braket{j',m'|J_y|j,m} &= \Braket{j',m'|\frac{J_+ - J_-}{2\mathrm{i}}|j,m} \\
  &= \frac{\hbar}{2\mathrm{i}} \Big\{ \sqrt{(j-m) (j+m+1)} \delta_{m',m+1} \\
  &\qquad - \sqrt{(j+m)(j-m+1)} \delta_{m',m-1} \Big\} \delta_{j',j}.
\end{align*}
%
\begin{theorem*}
$j$ kann nur ganzzahlige oder halbzahlige Werte annehmen:
\[
  j = 0, \frac{1}{2}, 1, \frac{3}{2}, \ldots
\]
Ist also $\Ket{jm}$ ein gemeinsamer Eigenvektor zu $\bm{J}^2$ und $J_z$, dann muss $m$ zwangsläufig einen von $(2j+1)$ Werten annehmen:
\[
  m = -j, -j+1, \ldots, j-1, j .
\] 
\end{theorem*}
%
Nimmt $j$ die Werte $1,2,3,\ldots$ an, so spricht man von einem ganzzahligen Drehimpuls, für die Werte $1/2, 3/2, \ldots$ dagegen von einem halbzahligen Drehimpuls.
%
\begin{table}[H]
  \centering
  \begin{tabular}{cc|c|cc|ccc}
    \toprule
    & & \multicolumn{6}{c}{$j,m$} \\
    & & $0,0$ & $1/2,1/2$ & $1/2,-1/2$ & $1,1$ & $1,0$ & $1,-1$ \\
    \midrule
    \multirow{6}{*}{$j',m'$}
    & $0,0$ & {\color{green} $0$},{\color{red} $0$} & \multicolumn{2}{|c}{{\color{green} $0$},{\color{red} $0$}} & \multicolumn{3}{|c}{{\color{green} $0$},{\color{red} $0$}} \\
    \cmidrule(r){2-8}
    & $1/2,1/2$ & \multirow{2}{*}{{\color{green} $0$},{\color{red} $0$}} & {\color{green} $3/4$},{\color{red} $1/2$} & {\color{green} $0$} & \multicolumn{3}{|c}{\multirow{2}{*}{{\color{green} $0$},{\color{red} $0$}}} \\
    & $1/2,-1/2$ & & {\color{green} $0$} & {\color{green} $3/4$},{\color{red} $-1/2$} & & & \\
    \cmidrule(r){2-8}
    & $1,1$ & \multirow{3}{*}{{\color{green} $0$},{\color{red} $0$}} & \multicolumn{2}{c|}{\multirow{3}{*}{{\color{green} $0$},{\color{red} $0$}}} & {\color{green} $2$},{\color{red} $1$} & {\color{green} $0$},{\color{red} $0$} & {\color{green} $0$},{\color{red} $0$} \\
    & $1,0$ & & & & {\color{green} $0$},{\color{red} $0$} & {\color{green} $2$},{\color{red} $0$} & {\color{green} $0$},{\color{red} $0$} \\
    & $1,-1$ & & & & {\color{green} $0$},{\color{red} $0$} & {\color{green} $0$},{\color{red} $0$} & {\color{green} $2$},{\color{red} $-1$} \\
    \bottomrule
  \end{tabular}
  \caption*{Matrixelemente von {\color{green} $\bm{J}/\hbar$} und {\color{red} $J_z/\hbar$}}
\end{table}

\begin{table}[H]
  \centering
  \begin{tabular}{c|cc}
    \toprule
    & $(1/2,1/2)$ & $(1/2,-1/2)$ \\
    \midrule
    $(1/2,1/2)$ & {\color{Purple} $0$},{\color{orange} $0$} & {\color{Purple} $1/2$},{\color{orange} $-\mathrm{i}/2$} \\
    $(1/2,-1/2)$ & {\color{Purple} $1/2$},{\color{orange} $\mathrm{i}/2$} & {\color{Purple} $0$},{\color{orange} $0$} \\
    \bottomrule
  \end{tabular}
  \caption*{Matrixelemente von {\color{Purple} $J_x/\hbar$} und {\color{orange} $J_y/\hbar$}}
\end{table}

Durch Verwendung der hergeleiteten Formeln und dem obigen Beispiel können auch die Spin-$1$ Matrizen hergeleitet werden ($j=1$). Diese lauten dann wie folgt:
\[
J_x = \frac{1}{\sqrt{2}}\begin{pmatrix} 0 & 1 & 0 \\ 1 & 0 & 1  \\ 0 & 1 & 0  \end{pmatrix}, \quad J_y = \frac{1}{\sqrt{2}}\begin{pmatrix} 0 & -\mathrm{i} & 0 \\ \mathrm{i} & 0 & -\mathrm{i}  \\ 0 & \mathrm{i} & 0  \end{pmatrix}, \quad J_z = \begin{pmatrix} 1 & 0 & 0 \\ 0 & 0 & 0  \\ 0 & 0 & -1  \end{pmatrix}.
\]


\begin{notice*}
Wir bemerken einen scheinbaren Widerspruch zu Kapitel $3$ Abschnitt 3, in dem wir $J$ wie folgt eingeführt haben:
\[
  J = XP_y - YP_x = \hbar \left(x p_y -y p_x\right) = L_z \stackrel{!}{=} \mathrm{i} \hbar \partial_\phi .
\]
$J$ hatte hierbei die Eigenwerte $m \hbar$ mit $m\in \mathbb{Z}$. Der Widerspruch wird dadurch aufgelöst, dass wir hier den \acct{Bahndrehimpuls} (orbital angular momentum) betrachten haben, aber in diesen Kapitel den >>totalen<< Drehimpuls (totally angular momentum) eingeführt haben. Diese sind aber nicht ein und das selbe !
\end{notice*}
