% Henri Menke, Michael Schmid, Marcel Klett 2013 Universität Stuttgart.
%
% Dieses Werk ist unter einer Creative Commons Lizenz vom Typ
% Namensnennung - Nicht-kommerziell - Weitergabe unter gleichen Bedingungen 3.0 Deutschland
% zugänglich. Um eine Kopie dieser Lizenz einzusehen, konsultieren Sie
% http://creativecommons.org/licenses/by-nc-sa/3.0/de/ oder wenden Sie sich
% brieflich an Creative Commons, 444 Castro Street, Suite 900, Mountain View,
% California, 94041, USA.

%
% Vorlesung am 11.01.2013
%
\renewcommand{\printfile}{2013-01-11}

\subsection{Vollständiger Satz kommutierender Observablen}

Betrachtet wird ein rotationsinvariantes (symmetrisches) Potential, dass wie folgt aussieht:
\[
  V(\bm{r})=V(|\bm{r}|) .
\] 
Weiter betrachten wir den Bahndrehimpuls $\bm{L}$ in Ortsdarstellung ($\bm{L}$ bezeichnet im Gegensatz zu $J$ den Bahndrehimpuls, $J$ den Drehimpulsoperator, den wir im vorherigen Kapitel eingeführt haben), für den gilt:
%
\begin{align*}
  \bm{L} &= \bm{r} \times \bm{p} \\
  &= \bm{r} \times \left(-\mathrm{i} \hbar \nabla_r\right) .
\end{align*}
%
Im letzten Umformungsschritt wurde der Impulsoperator $\bm{p}$ in der Ortsdarstellung:
\[
   p_i = -\mathrm{i}\hbar \partial_i \quad, \quad i\in\{x,y,z\} ,
\]
ausgeführt und leicht umgeschrieben. Für $L_z$ bedeutet dies:
\[
  L_z = \mathrm{i} \hbar \partial_\phi .
\]
Es gelten folgende Kommutatorrelationen:
%
\begin{align*}
  [L_z,\bm{p}^2] &= 0 \\
  [L_z,V(r)] &= 0 \\
  [\bm{L}^2, V(r)] &= 0
\end{align*}
Somit auch:
\[
  \left[ \frac{\bm{p}^2}{2 \mu } + V(\bm{r}) , \bm{L}^2 \right] = 0 .
\]
Es zeigt sich also, dass $H$, $\bm{L}^2$ und $L_z$ vertauschen. Somit existiert ein vollständiger Satz kommutierender Observablen. Folglich gibt es gemeinsame Eigenfunktionen.

Bezüglich der Basis:
\[
  \left\{\Ket{E\ell m} \right\} \quad \text{mit} \quad (\ell m\hateq jm)
\] 
gelten somit folgende Eigenwertgleichungen:
%
\begin{align}
  H \Ket{E\ell m} &= E \Ket{E\ell m} \label{Eq: Ham_5_2} \\
  L^2 \Ket{E\ell m} &= \hbar^2 \ell(\ell+1) \Ket{E\ell m}  \nonumber\\
  L_z \Ket{E\ell m} &= \hbar m \Ket{E\ell m} \nonumber.
\end{align}
%

\subsection{Radialgleichung}

Im folgenden Abschnitt soll gezeigt werden, dass aus der dreidimensionalen zeitunabhängigen Schrödingergleichung in Kugelkoordinaten durch Verwendung der Symmetrieeigenschaften eines Systems mit rotationsinvarianten Potential eine eindimensionale Radialgleichung hergeleitet werden kann, die einer eindimensionalen Schrödingergleichung mit effektiven Potential ähnelt.

Die zeitunabhängige Schrödingergleichung in Ortsdarstellung lautet:
\[
  H \psi(\bm{r}) = E \psi(\bm{r}) , 
\]
wobei für den Hamiltonoperator gilt:
\[
  H = \frac{1}{2 \mu }\bm{p}^2 + V(\bm{r}). 
\]
Für unserer weitere Betrachtung, wollen wir diese Gleichung in Kugelkoordinaten transformieren. Für den ersten Teil des Hamiltonoperator bedeutet dies:
% 
\begin{align*}
  \frac{1}{2 \mu }\bm{p}^2 &= \frac{p_x^2}{2 \mu } +\frac{p_y^2}{2 \mu } +\frac{p_z^2}{2 \mu } \\
  &\hateq - \frac{\hbar^2}{2 \mu} \left(\partial_r^2 + \frac{2}{r} \partial_r\right) + \frac{\bm{L}^2}{2 \mu r^2} .
\end{align*}
%
Hierfür wurde $\bm{L}^2$ ausgeführt, in Kugelkoordinaten transformiert und anschließend $p^2$ ermittelt.

\begin{proof}
  $\bm{L}^2$ lässt sich unter Berücksichtigung der Nichtvertauschbarkeit von Orts- und Impulsoperator wie folgt darstellen:
  %
  \begin{align*}
    \bm{L}^2 &= \left( \bm{r} \times \bm{p}\right)^2 \\
    &= \varepsilon_{\alpha \beta \gamma} x_\beta p_\gamma \varepsilon_{a \mu \nu } x_\mu p_\nu \\
    &= (\delta_{\beta \gamma} \delta_{\gamma \nu} - \delta_{\beta \nu} \delta_{\gamma \mu}) x_\beta p_\gamma x_\mu p_\nu \\
    &= x_\beta p_\gamma x_\beta p_\gamma - x_\beta p_\gamma x_\gamma p_\beta 
  \intertext{Mit $xp-px=\mathrm{i}\hbar$ folgt:}
    &= x_\beta (x_\beta p_\gamma - \mathrm{i}\hbar \delta_{\gamma \beta}) - x_\beta p_\gamma (\mathrm{i} \hbar \delta_{\beta \gamma} + p_\beta x_\gamma) \\
    &= \bm{x}^2 \bm{p}^2 - 2 \mathrm{i} \hbar \bm{x} \bm{p} - (\bm{x}\cdot\bm{p})(\bm{x}\cdot\bm{p})\\
    &= \bm{x}^2 \bm{p}^2 + \mathrm{i} \hbar \bm{x} \bm{p} - (\bm{x}\cdot\bm{p})^2
  \end{align*}
  % 
  Betrachtet man nun die einzelnen Komponenten, so gilt für die erste Komponente:
  %
  \begin{align*}
    \Braket{r \theta \phi |\bm{r}^2 \bm{p}^2| \psi} &= r^2 \Braket{r \theta \phi |\bm{r}^2 \bm{p}^2| \psi} .
  \end{align*}
  Für die zweite Komponente gilt mit $\bm{r} = r \bm{e}_r$ und $\bm{p} = -\mathrm{i} \hbar \nabla$ folgendes:
  %
  \begin{align*}
    \Braket{r \theta \phi |\bm{r} \bm{p}| \psi} &= - \mathrm{i} \hbar \Braket{r \theta \phi |\bm{r} \nabla| \psi}
  \intertext{Da für die Projektion des Impulsoperator auf den Ortsoperator $\bm{r} \cdot \bm{p} = -\mathrm{i}\hbar \bm{r} \cdot \nabla = -\mathrm{i}\hbar r \partial_r$ gilt, folgt somit:}
    &= - \mathrm{i} \hbar r \partial_r \psi(r,\theta,\phi) .
  \end{align*}
  %
  Für die dritte Komponente bedeutet dies:
  %
  \begin{align*}
      \Braket{r \theta \phi |(\bm{r} \bm{p})^2| \psi} &=   \Braket{r \theta \phi | (-\mathrm{i} \hbar r \partial_r)^2| \psi} \\ 
      &= -\hbar^2 r \partial_r r \partial_r \psi(r,\theta,\phi) .
  \end{align*}
  %
  Somit ergibt sich für:
  %
  \begin{align*}
      \Braket{r \theta \phi |\bm{L}^2| \psi} &= r^2 2 \mu \Braket{r \theta \phi |H_{\text{Kin}}| \psi} + \hbar^2 r \partial_r r \partial_r \psi(r,\theta,\phi) + \hbar^2 r \partial_r \psi(r,\theta,\phi) .
  \end{align*}
  % 
  Es zeigt sich also, das wir einen kinetischen Anteil des Hamiltonoperators erhalten. Somit ergibt sich letztendlich für $\bm{L}^2$:
  \[
    \bm{L}^2 = 2 \mu r^2 H_{\text{Kin}} + \hbar^2 \left(r^2 \partial_r^2 + 2 r \partial_r \right) .
  \]
\end{proof}

Aus obigen Beweis erhalten wir nun auch $H_{\text{Kin}}$:
\[
  H_{\text{Kin}} = \frac{\bm{L^2}}{2 \mu r^2} - \frac{\hbar^2}{2 \mu} \left( \partial_r^2 + \frac{2}{r} \partial_r\right) .
\]
Wenden wir die \eqref{Eq: Ham_5_2} an, so ergibt sich:
\[
  \Braket{r \theta \phi |H_{\text{Kin}} + V(r)| \psi} = E \Braket{r \theta \phi | \psi} .
\]
Setzt man nun das oben ermittelte $H_{\text{Kin}}$ in, so ergibt sich als Eigenwertgleichung die \acct{zeitunabhängige Schrödingergleichung in Kugelkoordinaten mit rotationsinvarianten Potantial}:
\[
  \left\{ -\frac{\hbar^2}{2 \mu} \left(\partial_r^2 + \frac{2}{r} \partial_r\right) + \frac{\bm{L}^2}{2 \mu r^2} + V(r) \right\} \psi(r,\theta,\phi) = E \psi(r,\theta,\phi) .
\]
Mit $\Ket{\psi} = \Ket{E\ell m}$ und den bekannten Eigenfunktionen von $\bm{L}^2$ machen wir den Separationsansatz
\[
  \psi(r,\theta,\phi) = R_E(r) Y_\ell^m(\theta,\phi) .
\]
Eingesetzt in die Differentialgleichung folgt unter Verwendung der im vorherigen Kapitel eingeführten Leiteroperatoren für den radialen Anteil 
\[
  \left\{ -\frac{\hbar^2}{2 \mu} \left(\partial_r^2 + \frac{2}{r} \partial_r\right) + \frac{\hbar^2 \ell(\ell+1)}{2 \mu r^2} + V(r) \right\} R_E(r) = E R_E(r) .
\]
Es zeigt sich also, dass diese Gleichung unabhängig von $m$ ist. Im Separationsansatz liegt ein allgemeines Prinzip vor. Da $H$ rotationsinvariant ist, gilt
\[
  [H,\bm{L}] = [H,\bm{L}^2] = 0 .
\]
Dies besagt, dass $H$, $L_z$ und $\bm{L}^2$ gemeinsam diagonalisierbar sind. Aus einer kontinuierlichen Symmetrie folgt somit eine Erhaltungsgröße.

Die obige Differentialgleichung hängt nur noch von einer Koordinate ab, hat aber noch nicht die Form einer eindimensionalen Schrödingergleichung. Um dies zu erreichen, substituieren wir mit dem Ansatz:
\[
  R_E(r) = \frac{u(r)}{r} .
\]  
Somit geht unsere Differentialgleichung über in:
\[
  \left\{ -\frac{\hbar^2}{2 \mu} \partial_r^2  + \frac{\hbar^2 \ell(\ell+1)}{2 \mu r^2} + V(r) \right\} u(r) = E u(r) .
\]
Wir haben somit das Problem auf ein eindimensionales Problem zurückgeführt, wobei die eindimensionale Schrödingergleichung nun ein effektives Potential besitzt.

\begin{notice*}
  Der Vergleich mit
  \[
  \left(-\frac{\hbar^2}{2m} \partial_x^2 + V(x)\right) \psi(x) = E \psi(x),
  \]
  zeigt, dass das neue Potential $\frac{\hbar^2\ell(\ell+1)}{2 \mu r^2}$ mit einschließt. In der klassischen Mechanik ist das Keplerproblem ein Analogon.
\end{notice*}

\subsection{Eigenfunktionen zu $L_z$ , $L^2$ in Kugelkoordinaten für den Winkelanteil}

Um die Eigenwerte und Eigenfunktionen des Bahndrehimpulses zu bestimmen, ist es sinnvoll in Kugelkoordinaten überzugehen. Dadurch wird sich herausstellen, dass die Eigenfunktionen nicht vom Radius aber von den Winkeln $\theta$ und $\phi$ abhängen.

Für die $z$-Komponente des Bahndrehimpulses gilt:
\[
  L_z = x p_y - y p_x.
\]
In der Ortsdarstellung entspricht dies:
\[
  L_z = -\mathrm{i}\hbar (x \partial_y - y \partial_x) .
\]

\begin{notice*}
  Für den Übergang in Kugelkoordinaten ist es sinnvoll zunächst zu betrachten, was $\partial/\partial \phi$ und $\partial/\partial \theta$ ist.Betrachten wir zuerst die Darstellung von $\partial/\partial \phi$ mit Hilfe der Kettenregel:
  \[
    \frac{\partial}{\partial \phi} = \frac{\partial x}{\partial \phi} \frac{\partial}{\partial x} +\frac{\partial y}{\partial \phi} \frac{\partial}{\partial y} + \frac{\partial z}{\partial \phi} \frac{\partial}{\partial z}
  \]
  Mit den >>nicht<< üblichen Kugelkoordinaten folgt:
  %
  \begin{align*}
    x &= r \sin(\theta) \cos(\phi) &&\implies \frac{\partial x}{\partial \phi} = -y && \\
    y &= r \sin(\theta) \sin(\phi) &&\implies \frac{\partial y}{\partial \phi} = x && \\
    z &= r \cos(\theta) &&\implies \frac{\partial z}{\partial \phi} = 0 && \\
  \end{align*}
  %
  Eingesetze in die obige Gleichung bedeutet dies für die partielle Ableitung nach $\phi$:
  \[
    \frac{\partial}{\partial \phi} = -y \partial_x + x \partial_y .
  \]
  Analog erhält man für $\partial/\partial \theta$:
  \[
    \frac{\partial}{\partial \theta} = r \cos(\theta)\left(\cos(\phi) \partial_x + \sin(\phi) \partial_y \right) - r \cos(\theta) \partial_z.
  \]
  Diese Relationen sind für die Transformation in Kugelkoordinaten wichtig.
\end{notice*}

Mit den in der Bemerkung eingeführten Tricks ergibt sich somit für $L_z$:
\[
  \boxed{L_z = -\mathrm{i}\hbar \partial_\phi}
\]
Um dies für $\bm{L}^2$ zu gewährleisten, ist es sinnvoll zunächst die Leiteroperatoren $L_\pm$ zu transformieren. Für $L_+$ bedeutet dies: 
%
\begin{align*}
  L_+ &= L_x + \mathrm{i}L_y = (y p_z - z p_y + \mathrm{i} (zp_x - xp_z)) \\
  &= z\hbar(\partial_x + \mathrm{i}\partial_y) - \hbar (x+\mathrm{i} y) \partial_z
\end{align*}
%
\begin{notice*}
  Auch hier ist es sinnvoll sich zunächst folgenden Trick anzueignen:
  %
  \begin{gather*}
    \cot(\theta)\partial_\phi = r \cos(\theta) \left(\cos(\phi) \partial_y - \sin(\phi) \partial_x\right) .
  \end{gather*}
  %
  Wir betrachten auch:
  %
  \begin{align*}
    \left(\partial_\theta + \mathrm{i} \cot(\theta)\partial_\phi\right) &= r \cos(\theta)\left(\mathrm{e}^{-\mathrm{i}\phi} \partial_x + \mathrm{i} \mathrm{e}^{-\mathrm{i}\phi} \partial-y\right) - r \sin(\theta)\partial_z &\bigg|\cdot \mathrm{e}^{\mathrm{i}\phi} \\
    \mathrm{e}^{\mathrm{i}\phi} \left(\partial_\theta + \mathrm{i} \cot(\theta)\partial_\phi\right) &= r \cos(\theta)\left(\partial_x + \mathrm{i} \partial_y \right) - r \sin(\theta)\left(\cos(\phi) + \mathrm{i}\sin(\phi\right)\partial_z \\
    &= z (\partial_x + \mathrm{i}\partial_y)-(x+\mathrm{i}y)\partial_z
  \end{align*}
  %
  Hier mit lässt sich $L_+$ in Kugelkoordinaten Transformieren.
\end{notice*}

Mit der obigen Bemerkung lautet somit $L_+$ in Kugelkoordinaten wie folgt:
\[
  \boxed{L_+ = \hbar \mathrm{e}^{\mathrm{i} \phi} \left(\partial_\theta + \mathrm{i} \cot(\theta)\partial_\phi\right)}
\]
Durch analoges Vorgehen erhält man auch $L_-$:
\[
  \boxed{L_- = -\hbar \mathrm{e}^{-\mathrm{i} \phi} \left(\partial_\theta - \mathrm{i} \cot(\theta)\partial_\phi\right)}
\]
Mit Hilfe der obigen gefundenen Darstellungen von $L_\pm$ und $L_z$ lässt sich somit auch $\bm{L}^2$ bestimmen, dazu betrachten wir die Identität:
%
\begin{align*}
  \bm{L^2} &= L_+ L_+ +L_z^2 - L_z 
\end{align*}
%
Durch stures Einsetzen und umformen so durch Verwenden der Identität:
\[
  \cot^2(\alpha) - \frac{1}{\sin^2(\alpha)} = -1, 
\]
folgt für $\bm{L}^2$:
\[
  \boxed{\bm{L}^2 = -\hbar^2 \left[  \frac{1}{\sin(\theta)} \partial_\theta \left(\sin(\theta)\partial_\theta\right) + \frac{1}{\sin^2(\theta)} \partial_\phi^2\right]}
\]
Wir betrachten nun die Gleichung:
%
\begin{align}
  L_+ \Ket{\ell\ell} &= \Ket{0} . \label{eq: Lpls} 
\end{align}
%
Dies ist insofern wahr, da $L_+$ der Operator ist, der $m$ erhöht, da aber $m$ bereits seinen höchsten Wert $m=\ell$ angenommen hat, es also nicht möglich ist, dass $m$ nochmals erhöht wird, verschwindet die Lösung. Weiter folgt somit:
\[
  \Braket{\theta\phi|\ell\ell} = \mathrm{e}^{\mathrm{i}\ell\phi}u_{\ell\ell}(\theta).
\]
Setzen wir dies in \eqref{eq: Lpls} ein so folgt:
% 
\begin{align*}
  \hbar^2 \mathrm{e}^{\mathrm{i} \phi} \left(\partial_\theta + \mathrm{i} \cot(\theta) \partial_\phi\right) \underbrace{\mathrm{e}^{\mathrm{i}\ell\phi}u_{\ell\ell}(\theta)}_{=Y_{\ell\ell}(\theta,\phi)} &\stackrel{!}{=} 0 
\end{align*}
%
Dies ist eine lineare Differentialgleichung die sich vereinfachen lässt:
\[
  \partial_\theta u_{\ell\ell}(\theta) - \ell \cot(\theta) u_{\ell\ell}(\theta) = 0 .
\]
Als Lösung (durch Separation) ergibt sich für $u_{\ell\ell}(\theta)$:
\[
  u_{\ell\ell}(\theta) = \sin^\ell(\theta) .
\]
Somit ergibt sich für $Y_{\ell\ell}(\theta,\phi)$:
\[
  \boxed{Y_{\ell\ell}(\theta,\phi) = c_\ell \mathrm{e}^{\mathrm{i}\ell \phi} \sin^\ell(\theta)}
\]
Der Koeffizient $c_\ell$ kann hier bei eine beliebig von $r$ abhängige Funktion sein. Mit Normierung gilt:
\[
  Y_{\ell\ell}(\theta,\phi) = (-1)^\ell \left(\frac{2\ell+1}{4 \pi}\right)^{1/2} \frac{\sin^\ell(\theta)}{2^\ell \ell!} \mathrm{e}^{\mathrm{i}\ell\phi} .
\]
Das dies richtig ist, lässt sich wie folgt überprüfen:
\[
  \int \left|Y_{\ell\ell}\right|^2 \, \mathrm{d}\Omega = 1 .
\]
Die Anderen Funktionen erhalten wir durch Anwendung von $L_-$:
%
\begin{align*}
  L_- \Ket{\ell\ell} &= \hbar (2 \ell)^{1/2} \Ket{\ell,(\ell-1)} &\bigg| \Bra{\theta \phi} \\
  Y_{\ell,\ell-1} &\stackrel{!}{=} \Braket{\theta \phi | \ell,(\ell-1)} = L_- \mathrm{e}^{\mathrm{i}\ell \phi} \sin^\ell(\theta) .
\end{align*}
Hierbei ist $L_-$ ein Differentialoperator.

\minisec{Zusammenfassung:}
Für ein allgemeines $Y_{\ell m}(\theta,\phi)$ gilt somit:
\[
Y_{\ell m}(\theta,\phi) = \Braket{\theta \phi | \ell m} = \sqrt{ \frac{(2\ell+1)(\ell-m)!}{4 \pi (\ell+m)!}} (-1)^m \mathrm{e}^{\mathrm{i}m\phi} \mathcal{P}_\ell^m(\cos(\theta)).
\]
Wir stellen also fest, dass diese Gleichung nur für ganzzahlige $m$ erfüllt ist. $\mathcal{P}_\ell^m$ entspricht hierbei den assoziierten Legendre Polynomen.

\begin{theorem*}[Definition:] 
  Die assoziierten \acct{Legendre Polynome} sind wie folgt definiert:
  \[
    \mathcal{P}_\ell^m(x) = \frac{(-1)^\ell}{2^\ell \ell!} \left(1-x^2\right)^{m/2} \frac{\mathrm{d}^{\ell+m}}{\mathrm{d}x^{\ell+m}} (x^2-1)^\ell
  \]
\end{theorem*}
Es zeigt sich, dass die Kugelflächenfunktionen $Y_{\ell m}$ vollständig auf der Kugel sind (also folgende Orthogonalitätsrelation erfüllen): 
\[
  \int Y_{\ell'm'}^\ast(\theta,\phi)Y_{\ell m}(\theta,\phi) \; \mathrm{d}\Omega = \delta_{\ell\ell'}\delta_{mm'} .
\]
Die Vollständigkeit bedeutet, dass jede Funktion $f(\theta,\phi)$ nach den Kugelflächenfunktionen entwickeln werden kann:
\[
  f(\theta,\phi) = \sum\limits_{\ell=0}^{\infty} \sum\limits_{m=-\ell}^{\ell} c_{\ell m} Y_{\ell m}(\theta,\phi) .
\]
Nimmt $\ell$ die Zahlen $0,1,2,3,\ldots$ an, so entspricht dies den Orbitalen, wie wir sie aus dem Chemieunterricht kennen. Also für $\ell=0$ entspricht dies dem s-Orbital, $\ell=1$ dem p-Orbital und für $\ell=2$ dem d-Orbital. 

\begin{notice*}
  Die Funktion $Y_{\ell m}(\theta,\phi)$ stellt eine \acct{Kugelflächenfunktion} dar, die die assoziierten Legender Polynome enthält. Die niedrigsten Kugelflächenfunktionen lauten explizit:
  %
  \begin{align*}
    Y_{00} &= \frac{1}{\sqrt{4 \pi}}, && Y_{10} = \sqrt{\frac{3}{4 \pi}}\cos(\theta), \\
    Y_{11} &= -\sqrt{\frac{3}{8 \pi}} \sin(\theta)\mathrm{e}^{\mathrm{i}\phi}, && Y_{20} = \sqrt{\frac{5}{16 \pi}} \left(3 \cos^2(\theta)-1\right), \\
    Y_{21} &= -\sqrt{\frac{15}{8 \pi}} \cos(\theta)\sin(\theta)\mathrm{e}^{\mathrm{i}\phi}, &&
    Y_{22} = \sqrt{\frac{15}{32 \pi}} \sin^2(\theta) \mathrm{e}^{2\mathrm{i}\phi}
  \end{align*}
  %
  Geplottet ergeben sich die bekannten Orbitaltypen.
\end{notice*}

\begin{table}[H]
  \begin{tabular}{ccc}
    \includegraphics{harmonics/l0m0} & & \\
    $\ell = 0, m = 0$ & & \\
    \includegraphics{harmonics/l1m0} & \includegraphics{harmonics/l1m1} & \\
    $\ell = 1, m = 0$ & $\ell = 1, m = \pm 1$ & \\
    \includegraphics{harmonics/l2m0} & \includegraphics{harmonics/l2m1} & \includegraphics{harmonics/l2m2} \\
    $\ell = 2, m = 0$ & $\ell = 2, m = \pm 1$ & $\ell = 2, m = \pm 2$ \\
  \end{tabular}
\end{table}
