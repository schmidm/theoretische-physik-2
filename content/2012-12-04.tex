% Henri Menke, Michael Schmid, Marcel Klett, 2012 Universität Stuttgart.
%
% Dieses Werk ist unter einer Creative Commons Lizenz vom Typ
% Namensnennung - Nicht-kommerziell - Weitergabe unter gleichen Bedingungen 3.0 Deutschland
% zugänglich. Um eine Kopie dieser Lizenz einzusehen, konsultieren Sie
% http://creativecommons.org/licenses/by-nc-sa/3.0/de/ oder wenden Sie sich
% brieflich an Creative Commons, 444 Castro Street, Suite 900, Mountain View,
% California, 94041, USA.

%
%    Vorlesung 4.12.2012
%
\renewcommand{\printfile}{2012-12-04}

\section{Harmonischer Oszillator}

\subsection{Spektrum algebraisch}

Der Hamilton Operator für einen harmonischen Oszillator lautet
%
\begin{align*}
  H &= \frac{\hat{P}^2}{2m} + \frac{m}{2} \omega^2 \hat{X}^2 \\
  &= \frac{\hbar \omega}{2} \left( \frac{\hat{P}^2}{m \hbar \omega} + \frac{m \omega}{\hbar} \hat{X}^2 \right) .
\end{align*}
%
Der Term in Klammern ist jetzt dimensionslos. Wähle außerdem
%
\begin{align*}
  \hat{p} = \frac{\hat{P}}{P_0} \; , \quad \hat{x} = \frac{\hat{X}}{X_0}
\end{align*}
%
dabei sind
%
\begin{align*}
  P_0 = \sqrt{m \hbar \omega} \quad \text{und} \quad X_0 = \sqrt{\frac{\hbar}{m \omega}} .
\end{align*}
%
Damit transformiert sich unser Hamiltonoperator zu
%
\begin{align*}
  H = \frac{\hbar \omega}{2} (\hat{p}^2 + \hat{x}^2) .
\end{align*}
%
Aus der Definition von $\hat{p}$ und $\hat{x}$ folgt
%
\begin{align*}
  [\hat{x},\hat{p}] = \mathrm{i} .
\end{align*}

\begin{theorem*}[Definition]
  Vernichtungsoperator und Erzeugungsoperator
  %
  \begin{align*}
    a \coloneq \frac{1}{\sqrt{2}} (\hat{x} + \mathrm{i} \hat{p}) && \text{\color{DimGray} Vernichtungsoperator} \\
    a^\dagger \coloneq \frac{1}{\sqrt{2}} (\hat{x} - \mathrm{i} \hat{p}) && \text{\color{DimGray} Erzeugungsoperator}
  \end{align*}
\end{theorem*}

Mit dieser Definition lassen sich $\hat{x}$ und $\hat{p}$ ausdrücken:
%
\begin{align*}
  \hat{x} = \frac{1}{\sqrt{2}} (a + a^\dagger) \\
  \hat{p} = \frac{\mathrm{i}}{\sqrt{2}} (a^\dagger - a)
\end{align*}
%
Außerdem folgt aus der Definition
%
\begin{align*}
  [a,a^\dagger]
  &= \frac{1}{2} [\hat{x} + \mathrm{i} \hat{p},\hat{x} - \mathrm{i} \hat{p}] \\
  &= \frac{1}{2} (-\mathrm{i}) \, 2 \, [\hat{x},\hat{p}] = 1 .
\end{align*}
%
Mit der Definition und der neuen Schreibweise von $\hat{x}$ und $\hat{p}$ folgt für den Hamiltonoperator
%
\begin{align*}
  H
  &= \frac{\hbar \omega}{2} \left[ -\frac{1}{2}(a^\dagger a^\dagger - a^\dagger a - a a^\dagger + a a) + \frac{1}{2} (a a + a a^\dagger + a^\dagger a + a^\dagger a^\dagger) \right] \\
  &= \frac{\hbar \omega}{2} (a a^\dagger + a^\dagger a) \\
  &= \hbar \omega \left( a^\dagger a + \frac{1}{2} \right) .
\end{align*}

\begin{theorem*}[Definition]
  Anzahloperator
  %
  \begin{align*}
    \hat{n} \coloneq a^\dagger a
  \end{align*}
\end{theorem*}

Damit vereinfacht sich der Hamiltonoperator weiter zu
%
\begin{align*}
  H = \hbar \omega \left( \hat{n} + \frac{1}{2} \right) .
\end{align*}
%
Die Kommutatoren von $\hat{n}$ mit $a$ und $a^\dagger$ sind ebenfalls interessant:
%
\begin{align*}
  [\hat{n},a] &= [a^\dagger a, a] = - a \\
  [\hat{n},a^\dagger] &= [a^\dagger a, a^\dagger] = a^\dagger
\end{align*}

\begin{enum-arab}
  \item Betrachten wir das Spektrum von $\hat{n}$. Dabei sind $\Ket{\nu}$ die Eigenvektoren von $\hat{n}$.
  %
  \begin{align*}
    \hat{n} \Ket{\nu} &= \nu \Ket{\nu} \\
    a^\dagger a \Ket{\nu} &= \nu \Ket{\nu}
  \end{align*}
  
  \item Verwende die Kommutatorrelation
  %
  \begin{align*}
    \hat{n} (a \Ket{\nu}) &= a^\dagger a a \Ket{\nu} = (a a^\dagger - 1) a \Ket{\nu} = a (\nu -1) \Ket{\nu} .
  \end{align*}
  %
  Daraus folgt, dass $a \Ket{\nu}$ Eigenvektor zum Eigenwert $(\nu - 1)$ ist, oder $a \Ket{\nu} = \Ket{0}$ (Nullvektor).
  
  \item Weil $\hat{n}$ hermitesch ist gilt
  %
  \begin{align*}
    0 \leq | a \Ket{\nu} |^2 &= \Braket{\nu | a^\dagger a | \nu} \\
    &= \nu \Braket{\nu | \nu} .
  \end{align*}
  %
  Weil $\Braket{\nu | \nu} > 0$ ist muss auch gelten $\nu \geq 0$. Es folgt also, dass es einen kleinsten Eigenwert $\nu = 0$ gibt.
  %
  \begin{align*}
    a \Ket{0} = \Ket{0}_{\mathcal{H}}
  \end{align*}
  %
  wobei $\Ket{0}_{\mathcal{H}}$ der Nullvektor in $\mathcal{H}$ ist, während $\Ket{0}$ Eigenvektor zu $a$ ist.
  
  \item Betrachte
  %
  \begin{align*}
    \hat{n} (a^\dagger \Ket{\nu})
    &= a^\dagger a a^\dagger \Ket{\nu} \\
    &= (a^\dagger a^\dagger a + a^\dagger) \Ket{\nu} \\
    &= a^\dagger (\nu + 1) \Ket{\nu} , \\
    0
    &\leq \Braket{\nu | a a^\dagger | \nu} \\
    &= \Braket{\nu | (a^\dagger a + 1) | \nu} \\
    &= (\nu + 1) \Braket{\nu | \nu} .
  \end{align*}
\end{enum-arab}

Das Spektrum von $H$ ist also
%
\begin{align*}
  \left\{
    \frac{\hbar \omega}{2},
    \frac{3 \hbar \omega}{2},
    \frac{5 \hbar \omega}{2},
    \frac{7 \hbar \omega}{2},
    \ldots
  \right\}
\end{align*}
%
\begin{figure}[H]
  \centering
  \begin{pspicture}(-1,-0.3)(1,3)
    \psaxes[labels=none,ticks=none]{->}(0,0)(-1,-0.3)(1,3)
    \psplot{-1}{1}{3*x^2}
    \psyTick[linecolor=DarkOrange3](0.5){}
    \psyTick[linecolor=DimGray](1){\color{DimGray} \hbar \omega}
    \psyTick[linecolor=DarkOrange3](1.5){}
    \psyTick[linecolor=DarkOrange3](2.5){}
    \psline[linecolor=DarkOrange3]{->}(0.2,1.6)(0.2,2.4)
    \uput[0](0.2,2){\color{DarkOrange3} $a^\dagger$}
    \psline[linecolor=DarkOrange3]{->}(-0.2,2.4)(-0.2,1.6)
    \uput[180](-0.2,2){\color{DarkOrange3} $a$}
  \end{pspicture}
\end{figure}

$\nu$ kann die Werte $0,1,2,\ldots$ annehmen. Erlaubte werte sind
%
\begin{align*}
  \hat{n} \Ket{n} = n \Ket{n}
\end{align*}
%
also $n \in \mathbb{N}_0$.

Die Nullpunktsenergie ist \[ \frac{\hbar \omega}{2} . \qquad \text{\color{DimGray} (Grundzustandsenergie)} \]

\begin{example*}
  Ein makroskopischer Oszillator mit der Masse $m = 1 \; \mathrm{kg}$ und der Periodendauer $T = 1 \; \mathrm{s}$ hat die Energie $E = 1 \; \mathrm{J} \approx 10^{34} \; {\hbar}/{\mathrm{s}}$
\end{example*}

\minisec{Matrixelemente des Erzeugungs- und Vernichtungsoperators}
%
\begin{align*}
  a^\dagger \Ket{n} &= c_n \Ket{n+1} \\
  \Braket{n | a a^\dagger | n} &= |c_n|^2 = (n+1) \Braket{n|n} \\
  c_n &= \sqrt{n+1} 
\end{align*}
%
Somit ergibt sich also für $a^\dagger$:
\[
  \boxed{a^\dagger \Ket{n} = \sqrt{n+1} \Ket{n+1}}
\]
Analog folgt für den Vernichtungsoperator $a$:
%
\begin{align*}
  a \Ket{n} &= c_n \Ket{n-1} \\
  \Braket{n | a^\dagger a | n} &= |c_n|^2 = n \Braket{n|n} \\
  c_n &= \sqrt{n} 
\end{align*}
%
Somit ergibt sich also für $a$:
\[
  \boxed{a \Ket{n} = \sqrt{n} \Ket{n-1}}
\]
%
Sequenz:
%
\begin{align*}
  a^\dagger \Ket{0} &= \Ket{1} \\
  a^\dagger \Ket{1} &= \sqrt{2} \Ket{2} \\
  a^\dagger \Ket{2} &= \sqrt{3} \Ket{3}
\end{align*}
%
Damit lässt sich schreiben:
%
\begin{align*}
  \Ket{2} &= \frac{1}{\sqrt{2}} a^\dagger \Ket{1} = \frac{1}{\sqrt{2}} \frac{1}{\sqrt{1}} \left( a^\dagger \right)^2 \Ket{0} \\
  \Ket{n} &= \frac{1}{\sqrt{n!}} \left( a^\dagger \right)^n \Ket{0}
\end{align*}
%
Die Matrixelemente lauten also
%
\begin{align*}
  \Braket{n' | a^\dagger | n}
  &= \sqrt{n+1} \Braket{n' | n+1} \\
  &= \sqrt{n+1} \delta_{n',n+1} \\
  \Braket{n' | a | n}
  &= \sqrt{n'+1} \Braket{n'+1 | n} \\
  &= \sqrt{n'+1} \delta_{n'+1,n}
\end{align*}
%
Als Matrix geschrieben
%
\begin{align*}
  a =
  \begin{pmatrix}
    0 & \sqrt{1} & 0 & 0 & \ldots \\
    0 & 0 & \sqrt{2} & 0 & \ldots \\
    0 & 0 & 0 & \sqrt{3} & \ldots \\
    \vdots & \vdots & \vdots & \vdots & \ddots \\
  \end{pmatrix}
  \; , \quad
  a^\dagger =
  \begin{pmatrix}
    0 & 0 & 0 & \ldots \\
    \sqrt{1} & 0 & 0 & \ldots \\
    0 & \sqrt{2} & 0 & \ldots \\
    0 & 0 & \sqrt{3} & \ldots \\
    \vdots & \vdots & \vdots & \ddots \\
  \end{pmatrix} .
\end{align*}
%
Es gilt die Identität
%
\begin{align*}
  a a^\dagger - a^\dagger a = \mathds{1} .
\end{align*}
%
Mit der Matrixschreibweise lassen sich $\hat{x}$ und $\hat{p}$ ausdrücken
%
\begin{align*}
  \hat{x} =
  \frac{1}{\sqrt{2}}
  \begin{pmatrix}
    0 & \sqrt{1} & 0 & \ldots \\
    \sqrt{1} & 0 & \sqrt{2} & \ldots \\
    0 & \sqrt{2} & 0 & \ldots \\
    \vdots & \vdots & \vdots & \ddots \\
  \end{pmatrix}
  \; , \quad
  \hat{p} =
  \frac{\mathrm{i}}{\sqrt{2}}
  \begin{pmatrix}
    0 & -\sqrt{1} & 0 & \ldots \\
    \sqrt{1} & 0 & -\sqrt{2} & \ldots \\
    0 & \sqrt{2} & 0 & \ldots \\
    \vdots & \vdots & \vdots & \ddots \\
  \end{pmatrix} .
\end{align*}
%
Damit folgt für den Kommutator
%
\begin{align*}
  [\hat{x},\hat{p}] = \mathrm{i} \mathds{1} .
\end{align*}

\subsection{Wellenfunktion im Ortsraum}

Wir wissen
\begin{align*}
  \psi_n(x) &= \Braket{x|n} .
\end{align*}
%
Der Grundzustand des harmonischen Oszillators:
%
\begin{align*}
  a \Ket{0} &= \Ket{0} \qquad \left| \Bra{x} \right. \\
  \frac{1}{\sqrt{2}} \Braket{x | \hat{x} + \mathrm{i} \hat{p} | 0} &= 0 \\
  \frac{1}{\sqrt{2}} \left( x + \mathrm{i} (- \mathrm{i} \partial_x) \right) \psi_0(x) &= 0 \\
  \left( x + \partial_x \right) \psi_0(x) &= 0 \\
  \implies \quad \psi_0(x) &= c_0 \exp\left( -\frac{x^2}{2} \right)
\end{align*}
%
Normierung der Lösung:
%
\begin{align*}
  1 &\overset{!}{=} \int \mathrm{d}x \; |\psi_0(x)|^2 = c_0^2 \int \mathrm{d}x \; \exp\left( - x^2 \right) = c_0^2  \sqrt{\pi} \\
  \implies \quad \psi_0(x) &= \frac{1}{\sqrt[4]{\pi}} \exp\left( -\frac{x^2}{2} \right)
\end{align*}
%
\begin{figure}[H]
  \centering
  \psset{xunit=0.5cm}
  \begin{pspicture}(-4,-1.3)(4,1.3)
    \psaxes[labels=none,ticks=none]{->}(0,0)(-4,-1.3)(4,1.3)
    \psplot[plotpoints=200,linecolor=DimGray]{-4}{4}{1/16*x^2}
    \psplot[plotpoints=200,linecolor=MidnightBlue]{-4}{4}{1/(2^(0/2)*(0!)*\psPi^(1/4)) * 2.7182818284590451^(-(x/2)^2)}
    \psplot[plotpoints=200,linecolor=DarkOrange3]{-4}{4}{1/(2^(1/2)*(1!)*\psPi^(1/4)) * 2.7182818284590451^(-(x/2)^2) * 2*x}
    \psplot[plotpoints=200,linecolor=DarkRed]{-4}{4}{1/(2^(2/2)*(2!)*\psPi^(1/4)) * 2.7182818284590451^(-(x/2)^2) * (4*x^2 - 2)}
  \end{pspicture}
  \psset{xunit=1cm}
\end{figure}
%
\begin{align*}
  \Ket{1}
  &= a^\dagger \Ket{0} \\
  &= \frac{1}{\sqrt{2}} (\hat{x} - \mathrm{i} \hat{p}) \Ket{0} \qquad \Big| \Bra{x} \\
  \psi_1(x)
  &= \frac{1}{\sqrt{2}} (x - \partial_x) \frac{1}{\sqrt[4]{\pi}} \exp\left( -\frac{x^2}{2} \right) \\
  &= \frac{1}{\sqrt{2} \sqrt[4]{\pi}} 2 x \exp\left( -\frac{x^2}{2} \right) \\
  %
  \Ket{2}
  &= \frac{1}{\sqrt{2}} a^\dagger \Ket{1} \\
  &= \frac{1}{\sqrt{2} \sqrt[4]{\pi}} (x - \partial_x) x \exp\left( -\frac{x^2}{2} \right) \\
  \psi_2(x)
  &= \frac{1}{\sqrt{2} \sqrt[4]{\pi}} (2 x^2 - 1) \exp\left( -\frac{x^2}{2} \right)
\end{align*}
%
Allgemein:
%
\begin{align*}
  \Ket{n} &= \frac{1}{\sqrt{n!}} \left( a^\dagger \right)^n \Ket{0} \\
  \psi_n(x) &= \frac{1}{\sqrt{2^n n!}} \frac{1}{\sqrt[4]{\pi}} (x - \partial_x)^n \exp\left( -\frac{x^2}{2} \right)
\end{align*}
