% Henri Menke, Michael Schmid, Marcel Klett, 2012 Universität Stuttgart.
%
% Dieses Werk ist unter einer Creative Commons Lizenz vom Typ
% Namensnennung - Nicht-kommerziell - Weitergabe unter gleichen Bedingungen 3.0 Deutschland
% zugänglich. Um eine Kopie dieser Lizenz einzusehen, konsultieren Sie
% http://creativecommons.org/licenses/by-nc-sa/3.0/de/ oder wenden Sie sich
% brieflich an Creative Commons, 444 Castro Street, Suite 900, Mountain View,
% California, 94041, USA.

%
%    Vorlesung 7.12.2012
%
\renewcommand{\printfile}{2012-12-07}

\minisec{Repetition}
%
Für den harmonischen Oszillator in der Quantenmechanik ergeben sich folgende Resultate:
%
\begin{align*}
  H &= \hbar \omega \left(a^\dagger a + \frac{1}{2}\right),
\end{align*}
% 
mit $\hat{n} = a^\dagger a$ und $\hat{n} \Ket{n}=n=\Ket{n}$ ergibt sich
%
\begin{align*}
  \Ket{n} &= \frac{1}{\sqrt{n!}} \left(a^\dagger\right)^n \frac{1}{\sqrt[4]{\pi}} \Ket{0}.
\end{align*}
% 
Es ergibt sich also
%
\begin{align*}
  \psi_n (x) &= \frac{1}{\sqrt{2^n n!}} \frac{1}{\sqrt[4]{\pi}} (x - \partial_x)^n \exp\left( -\frac{x^2}{2} \right) 
  = \begin{cases} \text{gerade} &, \text{für } n=0,2,4,\ldots \\ \text{ungerade} &, \text{für } n=1,3,5,\ldots  \end{cases}
\end{align*}
%

\minisec{Erwartungswerte}
%
\begin{align*}
  \Braket{x}_{\Ket{n}} &= \Braket{n|\hat{x}|n} = 0 \\
  \Braket{p}_{\Ket{n}} &= \Braket{n|\hat{p}|n} = 0
\end{align*}
%

\minisec{Heisenbergsche Unschärferelation im GZ}
%
\begin{align*}
  \left(\Delta x\right)_{\ket{0}}^2 &= \Braket{x^2}_{\Ket{0}} \\
  &=\frac{1}{2} \Braket{0|\left(a + a^\dagger\right)^2|0} \\
  &=\frac{1}{2} \Braket{0|a^2 + a a^\dagger + a^\dagger a + \left(a^\dagger\right)^2|0} \\
  &= \frac{1}{2} \\
  \left(\Delta p\right)^2_{\Ket{0}} &= \ldots = \frac{1}{2}
\end{align*}
%
Für die Heisenbergsche Unschärferelation ergibt sich somit
\[
  \left(\Delta x\right)_{\Ket{0}} \left(\Delta p\right)_{\Ket{0}} \geq \frac{1}{2} \left|\Braket{\left[x,p\right]}\right| = \frac{1}{2}
\]
Es zeigt sich also, dass der Grundzustand >>optimal scharf<< ist.

\subsection{Darstellung durch Hermite-Polynome}

Die Wellenfunktion $\psi_n(x)$ lässt sich auch durch \acct{Hermite-Polynome} ausdrücken, sie lautet dementsprechend
\[
  \psi_n(x) = \frac{1}{\sqrt{2^n n!}} \frac{1}{\sqrt[4]{\pi}} \exp\left(- \frac{x^2}{2}\right) H_n(x).
\]
Mit
%
\begin{align*}
  H_n(x) &\coloneqq \exp\left(\frac{x^2}{2}\right) \left(\sqrt{2} a^\dagger\right)^n \exp\left(-\frac{x^2}{2}\right) \\
  &= \exp(x^2) \underbrace{\exp\left(\frac{x^2}{2}\right) \left(x-\partial_x\right)^n \exp\left(-\frac{x^2}{2}\right)}_{(-1)^n {\partial_x}^n \quad (\text{Beweis durch Induktion})} \exp(-x^2) \\
  &=(-1)^n \exp(x^2) \left(\partial_x\right)^n \exp(-x^2) \qquad \text{übliche Definition}.
\end{align*}
%
Die Hermite-Polynome sehen somit wie folgt aus:
%
\begin{align*}
  H_0(x) &= 1 \\
  H_1(x) &= 2 x \\
  H_2(x) &= 4 x^2 -2 \\
  &\vdots
\end{align*}
%
Für die Hermite-Polynome ergeben sich folgende Eigenschaften:
\begin{enum-roman}
  \item Normierung:
  %
    \begin{align*}
      \int\limits_{-\infty}^{\infty} \mathrm{d}x \; H_n(x)H_m(x) \exp(-x^2) = \sqrt{\pi} 2^n n \delta_{n,m}
    \end{align*}
  %
  \item Vollständigkeit:
  %
    \begin{align*}
      \sum\limits_{n} \psi_n(x) \psi_n(x') = \delta(x-x')
    \end{align*}
  %
  \item Differentialgleichung:
  %
    \begin{align*}
      \left[{\partial_x}^2 - 2 x \partial_x + 2 n\right] H_n(x)= 0
    \end{align*}
  %
  \item Erzeugende Funktion:
  %
    \begin{align*}
      G(x,t) &= \sum\limits_{n=0}^{\infty} \frac{t^n}{n!} H_n(x) &\bigg|{\partial_x}^2 \\
      {\partial_x}^2 G(x,t) &= \sum\limits_{n=0}^{\infty} \frac{t^n}{n!} \left(2 x H_n' - 2 n H_n \right) \\
      &= 2 x {\partial_x} G - 2 t \partial_t G \\
      {\partial_x}^2 G(x,t) &= 2 x {\partial_x} G - 2 t \partial_t G
    \end{align*}
  %
  Diese Differentialgleichung lässt sich mit dem Ansatz $G(x,t) = \exp\left(f(x,t)\right)$ vereinfachen.
  \[
    {\partial_x}^2 f + (\partial_x f)^2 = 2 x \partial_x f - 2 t \partial_t f
  \] 
  Auch diese Differentialgleichung lässt sich mit dem Ansatz $f = \alpha(t) x + \beta(t)$ vereinfachen.
  %
    \begin{align*}
      \alpha(t)^2 = \underbrace{2 x \alpha(t) - 2 t (\partial_t \alpha) x}_{\text{soll $= 0$ sein!}} - 2 t \partial_t \beta
    \end{align*}
  %
  Da $2 x \alpha(t) - 2 t (\partial_t \alpha) x = 0$ sein soll, muss ein geeignetes $\alpha$ gewählt werden. Für 
  \[
    \alpha = t \partial_t \alpha \implies \alpha = c t
  \]
  folgt somit:
  %
    \begin{align*}
      c^2 t^2 &\stackrel{!}{=} -2 t \partial_t \beta \\
      \implies \beta &= - \frac{c^2}{4} t^2 + d \\
      \implies G(x,t) &= \exp\left(c t x + \frac{c^2}{4} t^2 + d\right) 
    \end{align*}
  %
  Die einzelnen Konstanten $c,d$ sind wie folgt zu bestimmen:
  %
    \begin{align*}
       G(x,t=0) &= \exp(d) \stackrel{!}{=} H_0(x) = 1 &\implies d=0 \\
       \partial_t G(x,t=0) &=cx \stackrel{!}{=} H_1(x) = 2 x &\implies c = 2
    \end{align*}
  %
  Somit ergibt sich letztendlich
  \[
    G(x,t) = \exp\left(2 t x - t^2\right) 
  \]
  als erzeugende Funktion.
  %
\end{enum-roman}

\subsection{Direkte Lösung im Ortsraum} 

Die skalierte, stationäre Schrödingergleichung lautet im Falle des harmonischen Oszillator wie folgt:
\[
  \left(-\frac{1}{2} {\partial_x}^2 + \frac{1}{2} x^2 \right) \psi(x) = \varepsilon \psi(x).
\]
Hierbei ist:
%
\begin{align*}
  x &= \frac{X}{X_0} \\
  \varepsilon &= \frac{E}{\hbar \omega }
\end{align*}
%
\begin{notice*}
Um auf den geeigneten Lösungsansatz zu kommen, wird zunächst ein kleine Vorüberlegung  mit folgenden Ansatz getätigt:
%
\begin{align*}
  \psi(x) &\sim \exp\left(-\alpha x^m\right) \\ 
  \psi'(x) &\sim -\alpha x^{m-1} \exp\left(-\alpha x^m\right) \\
  \psi''(x) &\sim \left( \alpha m (m-1) + \alpha^2 m^2 x^{2m-2}\right) \exp\left(-\alpha x^m\right)
\end{align*}
% 
Hieraus folgt:
\[
  \alpha^2 m^2 x^{2m-2} \approx x^2 \implies m=2 \quad \text{und} \quad \alpha = \pm \frac{1}{2} 
\]
Damit ist die Asymptotik also unter Kontrolle.
\end{notice*}
Es bietet sich somit folgender Ansatz zur Lösung an.
\[
  \psi(x) = u(x) \exp(-\frac{x^2}{2})
\]
Durch einsetzten in die obige Schrödingergleichung ergibt sich:
%
\begin{align*}
  -\frac{1}{2} u'' + x u' + \frac{1}{2} u  &= \varepsilon u \\
  u'' - 2 x u' + (2\varepsilon - 1) u  &= 0 \qquad \text{exakt!} 
\end{align*}
%
Für die Lösung dieser Differentialgleichung bedient man sich eines Potenzreihenansatzes:
\[
  u(x) = \sum\limits_{n=0}^{\infty} b_n x^n.
\]
Durch einsetzen in die Differentialgleichung ergibt sich:
%
\begin{align*}
  \sum\limits_{n=2} n(n-1) b_n x^{n-2} -2 \sum\limits_{n=1} n b_n x^n + (2\varepsilon - 1) \sum\limits_{n=0} b_n x^n &= 0 .
\end{align*}
%
Betrachtet man nun den ersten Summenterm $\sum\limits_{n=2} n(n-1) b_n x^{n-2}$ und führt dort zuerst die Substitution $n=n'+2$ durch und anschließend eine weitere Substitution mit $u'= u$, so reduziert sich die Gleichung auf
\[
  \sum\limits_{n=0}^{\infty} \left\{ (n+2)(n+1) b_{n+2} + b_n (2\varepsilon -1 -2n)  \right\} x^n = 0.
\]
Die Rekursion für die Koeffizienten ist somit
\[
  b_{n+2} = -\frac{2\varepsilon -1 -2 n}{(n+2)(n+1)} b_n.
\]
Durch Vorgeben von $b_0$,$b_1$  kann der Rest ausgerechnet werden. 

Eine hinreichende Bedingung um eine Lösung zu erhalten ist, dass die Funktion Normierbar (quadratintegrabel) sein muss. Dies ist nur dann der Fall, wenn wir fordern, dass die Rekursion abbricht.
\[
  2n = 2 \varepsilon -1 \implies \varepsilon = n + \frac{1}{2}
\]
Für die Notwendigkeit gilt:
\[
  \lim\limits_{n\to \infty}\frac{b_{n+2}}{b_n} \to \frac{2}{n}
\]
Vergleicht man dazu die Funktion $\exp(x^2)$ folgt:
%
\begin{align*}
  \exp\left(x^2\right) &= \sum\limits_{k=0}^{\infty} \frac{1}{k!} \left(x^2\right)^k \\
  &= \sum\limits_{l=0}^{\infty} \frac{l}{\left(l/2\right)!} x^l
\end{align*}
%
Hierbei ist $l$ nur gerade. Mit 
\[
  \frac{c_{l+2}}{c_l} = \frac{(l/2)!}{(l/2+1)!} \sim \frac{1}{l/2+1} \sim \frac{2}{l}
\]
folgt genau die obere Lösung.

Ohne die Abbruchforderung gelte also $u(x) \sim \exp\left(x^2\right)$. Somit wäre aber $u(x)$ nicht mehr normierbar!

\begin{example*}
Beim abgeschnittenen Potential des harmonischen Oszillator existiert mindestens eine Lösung, bzw. gebundener Zustand.
%
\begin{figure}[H]
  \centering
  \begin{pspicture}(-2,-0.3)(2,2)
    \psaxes[labels=none,ticks=none]{->}(0,0)(-2,-0.3)(2,2)
    \psplot[plotpoints=200,linecolor=MidnightBlue,linestyle=dotted,dotsep=1pt]{-1.414}{1.414}{x^2}
    \psplot[plotpoints=200,linecolor=MidnightBlue]{-1}{1}{x^2}
    \psline[linecolor=MidnightBlue](-2,1)(-1,1)
    \psline[linecolor=MidnightBlue](1,1)(2,1)
    \psplot[plotpoints=200,linecolor=DarkOrange3]{-0.3}{0.3}{x^2}
    \psline[linecolor=DarkOrange3](-2,0.09)(-0.3,0.09)
    \psline[linecolor=DarkOrange3](0.3,0.09)(2,0.09)
    \psline[linestyle=dotted,dotsep=1pt](-1,1)(-1,0)
    \psline[linestyle=dotted,dotsep=1pt](1,1)(1,0)
    \psxTick[labelFontSize=\color{DimGray}](-1){-a}
    \psxTick[labelFontSize=\color{DimGray}](1){a}
    \psyTick[linecolor=Purple,labelFontSize=\scriptstyle\color{Purple}](0.333){1/2}
    \psyTick[linecolor=Purple,labelFontSize=\scriptstyle\color{Purple}](1){3/2}
    \psyTick[linecolor=Purple,labelFontSize=\scriptstyle\color{Purple}](1.666){5/2}
  \end{pspicture}
\end{figure}
%
Zweite Lösung mit Asymptotik $\psi \sim \exp(x^2/2)$ relevant für gewisse Probleme.
\end{example*}
