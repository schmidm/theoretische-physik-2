% Henri Menke, Michael Schmid, Marcel Klett, 2012 Universität Stuttgart.
%
% Dieses Werk ist unter einer Creative Commons Lizenz vom Typ
% Namensnennung - Nicht-kommerziell - Weitergabe unter gleichen Bedingungen 3.0 Deutschland
% zugänglich. Um eine Kopie dieser Lizenz einzusehen, konsultieren Sie
% http://creativecommons.org/licenses/by-nc-sa/3.0/de/ oder wenden Sie sich
% brieflich an Creative Commons, 444 Castro Street, Suite 900, Mountain View,
% California, 94041, USA.

%
%    Vorlesung 27.11.2012
%
\renewcommand{\printfile}{2012-11-27}

\subsection{Translationsoperator, periodische Potentiale und Bloch Theorem}


\begin{theorem*}[Definition:] Der \acct{Translationsoperator} ist wie folgt definiert:

\begin{figure}[H]
  \centering
  \begin{pspicture}(-0.3,-0.3)(3,1.2)
    \psaxes[labels=none,ticks=none]{->}(0,0)(-0.3,-0.3)(3,1.2)
    \psplot[plotpoints=200,linecolor=MidnightBlue]{0}{2.7}{2.7182818284590451^(-((x-0.5)/0.2)^2)}
    \psplot[plotpoints=200,linecolor=MidnightBlue]{0}{2.7}{2.7182818284590451^(-((x-2)/0.2)^2)}
    \psline[linestyle=dotted,dotsep=1pt](0.5,1)(0.5,-0.1)
    \psline[linestyle=dotted,dotsep=1pt](2,1)(2,-0.1)
    \psline[linecolor=DarkOrange3](0.5,0)(2,0)
    \psline{->}(1,0.75)(1.5,0.75)
    \uput[-90](1.25,0){\color{DarkOrange3} $a$}
    \uput[90](0.5,1){\color{MidnightBlue} $\Ket{\psi}$}
    \uput[90](2,1){\color{MidnightBlue} $T\Ket{\psi}$}
  \end{pspicture}
  \vspace*{-4em}
\end{figure}
%
\begin{align*} 
  \Braket{x|T_a|\psi} &\coloneq \psi(x-a) \\
  &\stackrel{\mathrm{Taylor}}{=} \sum\limits_{n=0}^{\infty} \frac{(-a)^n}{n!} \partial_x^n \psi  \bigg|_{x}\\
  &= \exp\left(-a \partial_x\right) \psi(x) \\
  &= \Braket{x|\exp\left(-\frac{\mathrm{i}}{\hbar} a \hat{P}\right)|\psi} \\
  \implies T_a &= \exp\left(-\frac{\mathrm{i}}{\hbar} a \hat{P}\right)
\end{align*}
%
Betrachte die Ableitung vom Translationsoperator
%
\begin{align*}
  \frac{\mathrm{d}}{\mathrm{d}a}T_a \bigg|_{a=0} = -\frac{\mathrm{i}}{\hbar} \hat{P}
\end{align*}
%
Das heißt der Impulsoperator erzeugt die Translation. Analog dazu kann der Operator $\mathscr{J}$ betrachtet werden. Dieser erzeugt die Drehung.
%
\begin{notice*}
Vergleich mit den infinitesimalen Drehungen:
%
\begin{align*}
  \mathscr{D}_{\alpha} = \mathds{1} - \mathrm{i} \frac{\varepsilon}{\hbar} \mathscr{J}_\alpha \qquad,\qquad \alpha \in \{x,y,z\}
\end{align*}
%
\end{notice*}
%
Der Translationsoperator $T$ ist unitär und besitzt die Eigenwerte $\exp(-\mathrm{i}Ka)$ mit $K \in \mathbb{R}$.
%
\begin{align*}
  T_a\Ket{\varPhi_a} &= \lambda_a \Ket{\varPhi_a} \\
  \Braket{x|T_a|\varPhi_a} &= \lambda_a \Braket{x|\varPhi_a} \\
  \varPhi_a(x-a) &= e^{-\mathrm{i}Ka} \varPhi_a(x)
\end{align*}
%
Alle Funktionen $\phi_a(x)$, welche die oben genannte Bedingung erfüllen, sind Eigenfunktionen vom Translationsoperator $T_a$.
\end{theorem*}
%
\begin{notice*}[Eigenwerte des Translationoperator:]
Allgemein gilt für einen Operator $\hat{A}$ die Eigenwertgleichung:
\[
  \hat{A} \Ket{a} = a \Ket{a}
\]
wobei $a$ dem Eigenwert entspricht. Analog folgt für Operatorfunktionen die Eigenwertgleichung:
%
\begin{align*}
    f(\hat{A}) \Ket{a} 
    &\stackrel{\text{Taylor}}{=} \sum_{n=0}^{\infty} \frac{1}{n!} f^{(n)}(0) \hat{A}^n \Ket{a} \\
    &= \sum_{n=0}^{\infty} \frac{1}{n!} f^{(n)}(0) a^n \Ket{a} \\
    &=  f(a) \Ket{A} .
\end{align*}
%
Für den Impulsoperator bedeutet dies:
%
\begin{align*}
  p \psi(x) &= -\mathrm{i} \hbar \partial_x \psi(x) \\
  \partial_x \psi(x) &= \frac{\mathrm{i}}{\hbar} p \psi(x) \\
  \implies \psi(x) &= \exp\left(\frac{\mathrm{i}}{\hbar} px\right) 
\intertext{Mit $K = p/\hbar$ folgt:}
  \psi(x) = \exp\left(\mathrm{i} K x\right) .
\end{align*}
%
Konsequenterweise folgt somit für unseren Translationsoperator $T_a$:
%
\begin{align*}
  T_a \Ket{\phi} &= \exp\left(-\frac{\mathrm{i}}{\hbar} a \hat{p}\right) \Ket{\phi} && \bigg| \Bra{x} \\
  T_a \phi(x) &= \phi(x-a) = \lambda_a \phi(x) \\
  &= \sum_{n=0}^{\infty} \frac{\left(-\mathrm{i}/\hbar\right)^n}{n!} a^n p^n \phi(x) \\
  &= \sum_{n=0}^{\infty} \frac{\left(-\mathrm{i} K a \right)^n}{n!} \phi(x) \\
  &= \exp\left(-\mathrm{i} K a\right) \phi(x)
\end{align*}
%
Die Eigenwerte des Translationsoperator sind also durch $\exp(-\mathrm{i}Ka)$ gegeben, wobei $K \in \mathbb{R}$.
\end{notice*}
%
\minisec{periodisches Potential}

\begin{figure}[H]
  \centering
  \begin{pspicture}(-0.3,-0.6)(3,0.6)
    \psaxes[labels=none,ticks=none]{->}(0,0)(-0.3,-0.6)(3,0.6)
    \psplot[plotpoints=200,linecolor=MidnightBlue]{-0.3}{3}{-0.5*cos(\psPi*x)}
    \psline[linecolor=DarkOrange3]{|<->|}(0,-0.6)(2,-0.6)
    \rput*(1,-0.6){\color{DarkOrange3} $a$}
  \end{pspicture}
\end{figure}

\begin{theorem}
Gilt für ein Potential $V$:
\[
  V(x) = V(x+a)
\]
Dann gilt: 
\[
  \left[H,T_a\right] = 0
\] 
\begin{proof}

\begin{enum-roman}
  \item 
    %
    \begin{align*}
      \left[\frac{p^2}{2m}, \exp\left(-\frac{\mathrm{i}}{\hbar} a p\right)\right] &= 0 
    \end{align*}
    %
  \item
    %
    \begin{align*}
      \left[V(x),T_a\right] &= \Braket{x'|(V(\hat{x})T_a - T_a V(\hat{x}))|x} \\
      &= \left(V(x')-V(x)\right) \underbrace{\Braket{x'|T_a|x}}_{\Braket{x'|x+a}} \\
      &= \left(V(x')-V(x)\right) \delta\left(x'-(x+a)\right) \\
      &= 0 \qquad \text{laut Vorraussetzung $V(x) = V(x+a)$}
    \end{align*}
    %
\end{enum-roman}
%
\end{proof}
\end{theorem}
%
Als Konsequenz aus dem gezeigten, ergibt sich das so genannte \acct{Bloch-Theorem}
%
\begin{theorem*}[Bloch-Theorem]
  Liegt ein periodische Potential $V(x) = V(x+a)$ vor und besitzen der Hamiltonoperator $H$ und der Translationsoperator $T_a$ gemeinsame Eigenfunktionen:
  %
  \begin{align*}
    H\Ket{\phi_K} = E \Ket{\phi_K} 
  \end{align*}
  %
  und die stationären Zustände haben die Form:
  %
  \begin{align*}
     \phi_K(x) &= \exp\left(\mathrm{i}Ka\right) \phi_K(x-a)
  \end{align*}
  %
  ($\phi_K(x)$ ist eine periodische Funktion) lässt sich die Schrödingergleichung im Intervall $0 \leq x \leq a$ wie folgt schreiben:
  %
  \begin{align*}
    \left( \frac{-\hbar^2}{2m} \partial_x + V(x) \right) \phi_K(x) = E_K \phi_K(x),
  \end{align*}
  %
  so haben die Lösungen dann die Form:
  \[
    \phi_K(a) = \exp\left(\mathrm{i}Ka\right) \phi_K(0).
  \]
\end{theorem*}

\subsection{Bänderstruktur im Beispiel des Dirac-Kamms}

\begin{figure}[H]
  \centering
  \begin{pspicture}(-1.4,-0.3)(1.4,1)
    \psaxes[labels=none,dx=0.5]{->}(0,0)(-1.4,-0.3)(1.4,1)[\color{DimGray} $x$,0][\color{DimGray} $V(x)$,0]
    \psxTick(0.5){\color{DimGray} a}
    \psxTick(1){\color{DimGray} 2 a}
    \psline[linecolor=DarkOrange3](-1,0)(-1,0.8)
    \psline[linecolor=DarkOrange3](-0.5,0)(-0.5,0.8)
    \psline[linecolor=DarkOrange3](0,0)(0,0.8)
    \psline[linecolor=DarkOrange3](0.5,0)(0.5,0.8)
    \psline[linecolor=DarkOrange3](1,0)(1,0.8)
  \end{pspicture}
\end{figure}

Das Rechnerisch einfachste, periodische Potential stellt der \acct{Dirac-Kamm} dar.
\[
  V(x) = \alpha \sum\limits_{j=-\infty}^{\infty} \delta(x-ja)
\]
Werden nun die verschiedenen Bereiche untersucht, so muss einmal $0 \leq x \leq a$ und $-a \leq x \leq 0$ betrachtet werde. Es gilt:
%
\begin{enum-roman}
  \item $0 \leq x \leq a$:
  %
  \begin{align*}
    \phi(x) &= A \sin(kx) + B \cos(kx) \qquad \text{mit} \qquad k= \sqrt{\frac{2mE}{\hbar^2}}
  \end{align*}
  %
  \item  $-a \leq x \leq 0$ (Verwendung des Bloch-Theorems):
  %
  \begin{align*}
    \phi(x) &= \exp\left(-\mathrm{i}Ka\right) \phi(x+a) \\
    &=  \exp\left(-\mathrm{i}Ka\right) \left[A \sin(k(x+a)) + B \cos(k(x+a))\right]
  \end{align*}
  %
\end{enum-roman}
%
Für die Anschlussbedingung bei $x=0$ gilt:
%
\begin{enum-roman}
  \item 
  \begin{align*}
    \phi(-0) &= \phi(+0) \\
    B &= \exp\left(-\mathrm{i}Ka\right) \left[A \sin(k(x+a)) + B \cos(k(x+a))\right] 
  \end{align*}
  \item
  \begin{align*}
    \phi'(+\varepsilon) - \phi'(-\varepsilon) &= \frac{2m \alpha}{\hbar^2} \phi(0)\\
    kA - \exp\left(-\mathrm{i}Ka\right) \left(kA\cos(ka) - kB \sin(ka)\right) &= \frac{2m \alpha}{\hbar^2} B
  \end{align*}
\end{enum-roman}
%
Somit ergibt sich also:
%
\begin{align*}
\cos(Ka) = \cos(ka) + \underbrace{\frac{m \alpha a}{\hbar^2}}_{= \beta } \frac{1}{ka} \text{sin}(ka) \qquad \text{mit} \qquad ka = z .
\end{align*}

\begin{figure}[H]
  \centering
  \begin{pspicture}(-0.5,-1.9)(6.5,1.9)
    \psaxes[labelFontSize=\scriptstyle\color{DimGray},trigLabelBase=2,dx=1,trigLabels]{->}(0,0)(-0.5,-1.9)(6.5,1.9)[\color{DimGray} $z=ka$,0][\color{DimGray},0]
    \psyTick(1.58){\color{DarkOrange3} \frac{m \alpha}{\hbar^2}a}
    \psplot[plotpoints=200,linecolor=MidnightBlue]{0}{6.5}{cos(\psPiH*x)}
    \psplot[plotpoints=200,linecolor=DarkOrange3]{0.01}{6.5}{sin(\psPiH*x)/x}
    \psline[linecolor=Purple]{|-|}(1.414,0)(2,0)
    \psline[linecolor=Purple]{|-|}(2.645,0)(4,0)
    \psline[linecolor=Purple]{|-|}(4.876,0)(6,0)
  \end{pspicture}
\end{figure}

Falls $ka=z$ im lila Bereich liegt, gibt es ein $Ka$. Für $z_n(\beta)\leq z\leq n \pi$ gibt es Lösungen, d.h. 
\[
  \frac{\hbar^2}{2m } z_n^2(\beta) \leq E  \leq \frac{\hbar^2}{2m} (\pi n)^2
\] 
