% Henri Menke, Michael Schmid, Marcel Klett, 2012 Universität Stuttgart.
%
% Dieses Werk ist unter einer Creative Commons Lizenz vom Typ
% Namensnennung - Nicht-kommerziell - Weitergabe unter gleichen Bedingungen 3.0 Deutschland
% zugänglich. Um eine Kopie dieser Lizenz einzusehen, konsultieren Sie
% http://creativecommons.org/licenses/by-nc-sa/3.0/de/ oder wenden Sie sich
% brieflich an Creative Commons, 444 Castro Street, Suite 900, Mountain View,
% California, 94041, USA.

%
% Vorlesung vom 30.10.2012
%
\renewcommand{\printfile}{2012-10-30}

\section{Quantenmechanik des Spin \texorpdfstring{$1/2$}{1/2} Systems}

\subsection{Hilbertraum \texorpdfstring{$\mathcal{H}$}{H} und \texorpdfstring{$\sigma_z$}{Sigma z}-Darstellung}

Die Stern-Gerlach-Experimente zeigen, dass es für den Spin offensichtlich zwei linear unabhängige Zustände gibt. Der Hilbertraum hat in diesem Falle die Dimension
\[
  \dim(\mathcal{H}) = 2 .
\] 
Als Basis werden folgende Vektoren gewählt:
%
\begin{align*}
  \{ \Ket{1}, \Ket{2}\} &\hateq \left\{ \begin{pmatrix} 1 \\ 0 \end{pmatrix} , \begin{pmatrix} 0 \\ 1 \end{pmatrix} \right\} \hateq  \{ \Ket{z+}, \Ket{z-}\} 
\end{align*}
%
Dies sind die Eigenvektoren des zu $\sigma_z$ gehörenden Operators. Dessen Eigenwerte sind $\pm 1$:
%
\begin{align*}
  \sigma_z \Ket{z+} &= 1 \cdot \Ket{z+} \\
  \sigma_z \Ket{z-} &= -1 \cdot \Ket{z-}
\end{align*}
%
Für den $\sigma_z$ Operator lässt sich also folgende Matrixdarstellung finden:
%
\begin{align*}
  \sigma_z &= \begin{pmatrix} 1 & 0 \\ 0 & -1 \end{pmatrix}
\end{align*}
%
Für einen beliebigen Zustand $\Ket{\psi}$ gilt unteranderem:
%
\begin{align*}
  \Ket{\psi} &= c_1 \Ket{z+} + c_2 \Ket{z-}
\end{align*}
%
Für die \acct{Normierung} gilt folgende Definition: 
%
\begin{align*}
  1 &\stackrel{!}{=} \Braket{\psi|\psi} \\
  &= c_1 c_1^\ast + c_2 c_2^\ast
\end{align*}
%
\minisec{Experiment 1}

\begin{figure}[H]
  \centering
  \vspace*{-2em}
  \begin{pspicture}(0,-1)(7,1)
  \rput(0,0){\psframe(-0.25,-0.25)(0.25,0.25)\rput(0,0){\color{DimGray} O}}
  \rput(2,0){\psframe(-0.5,-0.5)(0.5,0.5)\rput(0,0){\color{DimGray} SG,$z$}}
  \psline{->}(0.25,0)(1.5,0)
  \psline{->}(2.5,0.3)(4.5,0.3)
  \psline{-|}(2.5,-0.3)(3.5,-0.3)
  \uput[90](3.5,0.3){\color{DimGray} $\Ket{z+}$}
  \rput(5,0){\psframe(-0.5,-0.5)(0.5,0.5)\rput(0,0){\color{DimGray} SG,$z$}}
  \psline{->}(5.5,0.3)(6.5,0.3)
  \psline{->}(5.5,-0.3)(6.5,-0.3)
  \uput[0](6.5,0.3){\color{DimGray} $1$}
  \uput[0](6.5,-0.3){\color{DimGray} $0$}
  \end{pspicture}
  \vspace*{-4em}
\end{figure}

\begin{align*}
  \text{prob}[\sigma_z \hateq +1 | \Ket{z+}] &=  \Braket{\psi |P_{\Ket{z+}}|\psi} \\
  &= \Braket{z+|z+}\Braket{z+|z+} = 1 \\
  \text{prob}[\sigma_z \hateq -1 | \Ket{z+}] &= |\Braket{z-|z+}|^2 = 0
\end{align*}
%

\subsection{Spin-Operator}
%
\begin{notice*}[Frage:] 
Welcher Operator $\sigma_n$ gehört zur physikalischen Größe >>Spin in $n$-Richtung<<?
\end{notice*}
%
Wir wissen bereits:
%
\begin{item-triangle}
  \item $\sigma_n$ entspricht einer hermiteschen $2 \times 2$-Matrix, der Form: 
  %
  \begin{align*}
    \sigma_n &= \begin{pmatrix} a & b \\ b^\ast & d \end{pmatrix} 
  \end{align*}
  %
  mit den Eigenwerten $\pm 1$.
  \item Somit ist die Spur der Matrix, gleich der Summe der Eigenwerte:
  %
  \begin{align*}
    \text{tr}(\sigma_n) &= \sum \, \text{Eigenwerte} = 0 \\
    \det(\sigma_n) &= 1 \\
    \implies a &\stackrel{!}{=} -d
  \end{align*}
  %
  \item Somit gilt also für die Determinante:
  %
  \begin{align*}
    \det&= ad-bb^\ast = -a^2-bb^\ast = -1
  \end{align*}
  %
\end{item-triangle}
%
Somit lässt sich der \acct{Spin-Operator $\sigma_n$} wie folgt parametriesieren:
%
\begin{align*}
  \sigma_n &= \begin{pmatrix} \cos(\beta) & \exp(-\mathrm{i}\alpha) \sin(\beta) \\  \exp(\mathrm{i}\alpha) \sin(\beta) & -\cos(\beta) \end{pmatrix}
\end{align*}
%
mit  $\alpha = \alpha(\bm{n})$ und $\beta = \beta(\bm{n})$.

Für die Eigenvektoren gilt somit:
%
\begin{align*}
  \sigma_n \cdot \begin{pmatrix} \cos\left(\frac{\beta}{2}\right)  \\ \exp(\mathrm{i} \alpha )  \sin\left(\frac{\beta}{2}\right) \end{pmatrix} &= +1  \begin{pmatrix} \cos\left(\frac{\beta}{2}\right)  \\ \exp(\mathrm{i} \alpha )  \sin\left(\frac{\beta}{2}\right) \end{pmatrix} && {\color{DarkOrange3} \Ket{n+} }\\
  \sigma_n \cdot \begin{pmatrix} \sin\left(\frac{\beta}{2}\right)  \\ -\exp(\mathrm{i} \alpha )  \cos\left(\frac{\beta}{2}\right) \end{pmatrix} &= +1  \begin{pmatrix} \sin\left(\frac{\beta}{2}\right)  \\ -\exp(\mathrm{i} \alpha )  \cos\left(\frac{\beta}{2}\right) \end{pmatrix} && {\color{DarkOrange3} \Ket{n-} }\\
\end{align*}
%
\minisec{Bestimmung von $\sigma_x$}

Gesucht sind also folgende Funktionen für $\sigma_n$, um $\sigma_x$ zu erhalten:
\[
  \alpha(x) \qquad \text{und} \qquad \beta(x)
\]
sodass für den Eigenvektor $\Ket{x+}$ gilt:
\[
  \Ket{x+} = \begin{pmatrix} \cos\left(\frac{\beta_x}{2}\right)  \\ \exp(\mathrm{i} \alpha_x )  \sin\left(\frac{\beta_x}{2}\right) \end{pmatrix}
\]
Dazu wird vom Stern-Gerlach Experiment 2a ausgegangen:

\begin{figure}[H]
  \centering
  \vspace*{-2em}
  \begin{pspicture}(0,-1)(7,1)
  \rput(0,0){\psframe(-0.25,-0.25)(0.25,0.25)\rput(0,0){\color{DimGray} O}}
  \rput(2,0){\psframe(-0.5,-0.5)(0.5,0.5)\rput(0,0){\color{DimGray} SG,$z$}}
  \psline{->}(0.25,0)(1.5,0)
  \psline{->}(2.5,0.3)(4.5,0.3)
  \psline{-|}(2.5,-0.3)(3.5,-0.3)
  \uput[90](3.5,0.3){\color{DimGray} $\Ket{\psi} = \Ket{z+}$}
  \rput(5,0){\psframe(-0.5,-0.5)(0.5,0.5)\rput(0,0){\color{DimGray} SG,$x$}}
  \psline{->}(5.5,0.3)(6.5,0.3)
  \psline{->}(5.5,-0.3)(6.5,-0.3)
  \uput[0](6.5,0.3){\color{DimGray} $1/2$}
  \uput[0](6.5,-0.3){\color{DimGray} $1/2$}
  \end{pspicture}
  \vspace*{-4em}
\end{figure}

Für die Wahrscheinlichkeit gilt also:
%
\begin{align*}
  \text{prob}[\sigma_x \hateq +1 | \Ket{z+}] &\stackrel{!}{=} \frac{1}{2} \\
  &\stackrel{\text{P2}}{=} |\Braket{x+|z+}|^2 \\
  &= \left| \begin{pmatrix} \cos\left(\frac{\beta_x}{2}\right)  & \exp(-\mathrm{i} \alpha_x )  \sin\left(\frac{\beta_x}{2}\right) \end{pmatrix} \begin{pmatrix}  1 \\ 0 \end{pmatrix}  \right|^2 \\
  &= \cos^2\left(\frac{\beta_x}{2}\right)
\end{align*}
%
Ebenso folgt für:
%
\begin{align*}
  \text{prob}[\sigma_x \hateq -1 | \Ket{z+}] &\stackrel{!}{=} \frac{1}{2} \\
  &= \sin^2\left(\frac{\beta_x}{2}\right)
\end{align*}
%
Beide ermittelten Wahrscheinlichkeiten entsprechen dem experimentell ermittelten Ergebnis von $1/2$ genau dann, wenn:
%
\begin{align*}
  \beta_x = \frac{\pi}{2} \\
  \implies \sigma_x &=  \begin{pmatrix} 0 & \exp(-\mathrm{i}\alpha_x)  \\ \exp(\mathrm{i} \alpha_x) & 0 \end{pmatrix} 
\end{align*}
%
Für die Bestimmung von $\alpha_x$ wird zunächst analog zur Bestimmung der $\sigma_y$-Matrix vorgegangen.

\minisec{Bestimmung von $\sigma_y$}

Analog zur Bestimmung von $\sigma_x$ folgt für $\sigma_y$:
%
\begin{align*}
  \beta_y &= \frac{\pi}{2} \\
  \implies \sigma_y &=  \begin{pmatrix} 0 & \exp(-\mathrm{i}\alpha_y)   \\ \exp(\mathrm{i} \alpha_y) & 0 \end{pmatrix} 
\end{align*}
%
Um nun $\alpha_x$ und $\alpha_y$ zu bestimmen, wird das Stern-Gerlach-Experiment 2c betrachtet:

\begin{figure}[H]
  \centering
  \vspace*{-2em}
  \begin{pspicture}(0,-1)(7,1)
  \rput(0,0){\psframe(-0.25,-0.25)(0.25,0.25)\rput(0,0){\color{DimGray} O}}
  \rput(2,0){\psframe(-0.5,-0.5)(0.5,0.5)\rput(0,0){\color{DimGray} SG,$x$}}
  \psline{->}(0.25,0)(1.5,0)
  \psline{->}(2.5,0.3)(4.5,0.3)
  \psline{-|}(2.5,-0.3)(3.5,-0.3)
  \uput[90](3.5,0.3){\color{DimGray} $\Ket{x+}$}
  \rput(5,0){\psframe(-0.5,-0.5)(0.5,0.5)\rput(0,0){\color{DimGray} SG,$y$}}
  \psline{->}(5.5,0.3)(6.5,0.3)
  \psline{->}(5.5,-0.3)(6.5,-0.3)
  \uput[0](6.5,0.3){\color{DimGray} $1/2$}
  \uput[0](6.5,-0.3){\color{DimGray} $1/2$}
  \end{pspicture}
  \vspace*{-4em}
\end{figure}

Für die Wahrscheinlichkeit folgt nach diesem Experiment:
%
\begin{align*}
  \text{prob}[\sigma_y \hateq +1 | \Ket{x+}] &\stackrel{!}{=} \frac{1}{2} \\
  &= |\Braket{y+|x+}|^2 \\
  &= \left| \begin{pmatrix} \cos\left(\frac{\beta}{2}\right)  &  \exp(-\mathrm{i} \alpha_y)  \sin\left(\frac{\beta}{2}\right) \end{pmatrix} \begin{pmatrix} \cos\left(\frac{\beta}{2}\right)  \\ \exp(\mathrm{i} \alpha_x )  \sin\left(\frac{\beta}{2}\right) \end{pmatrix}  \right|^2 \\
  &= \left|\begin{pmatrix} \frac{1}{\sqrt{2}}  & \frac{1}{\sqrt{2}} \exp(-\mathrm{i} \alpha_y) \end{pmatrix}  \begin{pmatrix} \frac{1}{\sqrt{2}}  \\ \frac{1}{\sqrt{2}} \exp(\mathrm{i} \alpha_x) \end{pmatrix}  \right|^2 \\
  &= \left| \frac{1}{2} + \frac{1}{2} \exp(\mathrm{i}(\alpha_x - \alpha_y)) \right|^2 \\
  &= \frac{1}{2} \bigg| \underbrace{1+ \exp(\mathrm{i}(\alpha_x - \alpha_y))}_{\stackrel{!}{=} \sqrt{2} } \bigg|^2 \\
  \implies \alpha_x - \alpha_y &= \pm \frac{\pi}{2}
\end{align*}
%
Nach Konvention sind die beiden Winkel wie folgt festgelegt:
\[
  \alpha_x = 0 \qquad \text{und} \qquad \alpha_y = \frac{\pi}{2}
\]
Damit lauten die \acct{Pauli-Matrizen}  und ihre Eigenvektoren in endgültiger Form:
%
\begin{align*}
  \sigma_z  &= \begin{pmatrix} 1 & 0 \\ 0 & -1 \end{pmatrix} && \Ket{z+} = \begin{pmatrix} 1  \\ 0  \end{pmatrix}  && \Ket{z-} = \begin{pmatrix}  0 \\  1 \end{pmatrix} \\
  \sigma_x  &= \begin{pmatrix} 0 & 1 \\ 1 & 0 \end{pmatrix} && \Ket{x+} = \frac{1}{\sqrt{2}} \begin{pmatrix} 1  \\ 1  \end{pmatrix}  && \Ket{x-} = \frac{1}{\sqrt{2}} \begin{pmatrix}  1 \\  -1 \end{pmatrix} \\ 
  \sigma_y  &= \begin{pmatrix} 0 & -\mathrm{i} \\ \mathrm{i} & 0 \end{pmatrix} && \Ket{y+} = \frac{1}{\sqrt{2}} \begin{pmatrix} 1  \\ \mathrm{i}  \end{pmatrix}  && \Ket{y-} = \frac{1}{\sqrt{2}} \begin{pmatrix}  1 \\  -\mathrm{i} \end{pmatrix} 
\end{align*}
%
Die Pauli-Matrizen besitzen folgende Eigenschaften:
\begin{enum-roman}
  \item $\sigma_i\sigma_j = \delta_{ij} + \mathrm{i} \varepsilon_{ijk}\sigma_k$
  \item $\sigma_i^2 = \mathds{1}$ \\
  \item $[\sigma_i,\sigma_j]= 2 \mathrm{i} \varepsilon_{ijk} \sigma_k$
  \item $\bm{\sigma}= \begin{pmatrix} \sigma_x \\ \sigma_y \\ \sigma_z \end{pmatrix} $
  \item $(\bm{\sigma} \cdot \bm{a}) (\bm{\sigma} \cdot \bm{b}) = \bm{a}\bm{b} \mathds{1} + \mathrm{i} \bm{\sigma} (\bm{a}\times\bm{b}) $ 
\end{enum-roman}

\subsection{Allgemeines $\sigma_n$}

\begin{figure}[H]
  \centering
  \vspace*{-2em}
  \begin{pspicture}(-1,-0.5)(1,2)
    \newpsstyle{showCoorStyle}{linestyle=dotted,dotsep=1pt}
    \pstThreeDCoor[linecolor=DimGray,linewidth=1pt,xMax=1,yMax=1,zMax=2,xMin=0,yMin=0,zMin=0,nameX=\color{DimGray}$x$,nameY=\color{DimGray}$y$,nameZ=\color{DimGray}$z$]
    \pstThreeDLine[SphericalCoor=true,arrows=->](0,0,0)(2,80,60)
    \pstThreeDNode(0,0,0){O}
    \pstThreeDNode[SphericalCoor=true](1.5,80,60){A}
    \pstThreeDNode(0,0,1.5){B1}
    \pstThreeDNode(0.6,0,0){B2}
    \pstThreeDNode[SphericalCoor=true](0.6,80,0){C}
    \pstThreeDDot[dotscale=0.1,SphericalCoor=true,drawCoor=true](1.5,80,60)
    \ncarc[arrows=<-,arcangle=-10,linecolor=DarkOrange3]{A}{B1}
    \ncarc[arrows=<-,arcangle=10,linecolor=DarkOrange3]{C}{B2}
    \pstThreeDPut[SphericalCoor=true](1.1,80,75){\color{DarkOrange3} $\theta$}
    \pstThreeDPut[SphericalCoor=true](0.9,45,0){\color{DarkOrange3} $\phi$}
  \end{pspicture}
  
  \begin{pspicture}(0,-1)(10,1)
  \rput(0,0){\psframe(-0.25,-0.25)(0.25,0.25)\rput(0,0){\color{DimGray} O}}
  \rput(2,0){\psframe(-0.5,-0.5)(0.5,0.5)\rput(0,0){\color{DimGray} SG,$\bm{n}$}}
  \psline{->}(0.25,0)(1.5,0)
  \psline{->}(2.5,0.3)(4.5,0.3)
  \psline{-|}(2.5,-0.3)(3.5,-0.3)
  \rput(5,0){\psframe(-0.5,-0.5)(0.5,0.5)\rput(0,0){\color{DimGray} SG,$z$}}
  \psline{->}(5.5,0.3)(6.5,0.3)
  \psline{->}(5.5,-0.3)(6.5,-0.3)
  \uput[0](6.5,0.3){\color{DimGray} $p_{z+}(\bm{n}) = p\left(z=+1 \big| \ket{n+}\right)$}
  \uput[0](6.5,-0.3){\color{DimGray} $p_{z-}(\bm{n}) = p\left(z=-1 \big| \ket{n-}\right)$}
  \end{pspicture}
  \vspace*{-2em}
\end{figure}

Für die Bildung des Erwartungswertes folgt:
%
\begin{align*}
  \Braket{\sigma_x}_{\Ket{n+}} &= \Braket{n+|\sigma_x|n+}
\end{align*}
%
Wir wissen:
%
\begin{align*}
  \Braket{\sigma_{x/y/z}} &= \begin{cases} +1 & \text{falls} \; \bm{n}= +\hat{\bm{x}} \\ -1 & \text{falls} \; \bm{n}= -\hat{\bm{x}} \end{cases}
\end{align*}
%
$\Braket{\sigma_{x/y/z}}$ kann zu einen dreidimensionalen Vektor zusammengefasst werden:
\[
  \Braket{\bm{\sigma}_{\Ket{n+}}}
\]
Aus der Rotationssymmetrie folgt somit:
\[
  \Braket{\bm{\sigma}}_{\Ket{n+}} = \bm{n} = \begin{pmatrix} \sin\theta \cos\phi \\ \sin\theta \sin\phi \\ \cos\theta \end{pmatrix}
\]
Für die Erwartungswerte der einzelnen Pauli-Matrizen gilt somit:
%
\begin{align*}
  \Braket{n+|\sigma_x|n+} &= \begin{pmatrix} \cos\left(\beta/2\right) & \exp(-\mathrm{i} \alpha) \sin(\beta/2) \end{pmatrix} \begin{pmatrix} 0 & 1 \\ 1 & 0 \end{pmatrix} \begin{pmatrix} \cos\left(\beta/2\right) \\ \exp(\mathrm{i} \alpha) \sin(\beta/2) \end{pmatrix} \\
  &= \cos\alpha \sin\beta \stackrel{!}{=} \sin\theta \cos\phi \\
  \Braket{n+|\sigma_y|n+} &= \sin\alpha \sin\beta \\
  &\stackrel{!}{=} \sin\theta \sin\phi \\
  \Braket{n+|\sigma_z|n+} &= \cos\beta \\
  &\stackrel{!}{=} \cos\theta \\
  \implies \alpha &= \phi \qquad \text{und} \qquad \beta = \theta \\
  \implies \sigma_n &= \begin{pmatrix} \cos\theta & \exp(-\mathrm{i} \phi) \sin(\theta) \\  \exp(\mathrm{i} \phi) \sin(\theta) & -\cos\theta \end{pmatrix}
\end{align*}
%

\minisec{Experiment 4}

\begin{figure}[H]
  \centering
  \vspace*{-2em}
  \begin{pspicture}(0,-1)(9,1)
  \rput(0,0){\psframe(-0.25,-0.25)(0.25,0.25)\rput(0,0){\color{DimGray} O}}
  \rput(2,0){\psframe(-0.5,-0.5)(0.5,0.5)\rput(0,0){\color{DimGray} SG,$z$}}
  \psline{->}(0.25,0)(1.5,0)
  \psline{->}(2.5,0.3)(4.5,0.3)
  \psline{-|}(2.5,-0.3)(3.5,-0.3)
  \rput(5,0){\psframe(-0.5,-0.5)(0.5,0.5)\rput(0,0){\color{DimGray} SG,$x$}}
  \pnode(5.5,0.3){A1}
  \pnode(5.5,-0.3){A2}
  \pnode(6.5,0){B}
  \ncdiag[angleA=0,angleB=180,linearc=0.2]{A1}{B}
  \ncdiag[angleA=0,angleB=180,linearc=0.2]{A2}{B}
  \rput(7,0){\psframe(-0.5,-0.5)(0.5,0.5)\rput(0,0){\color{DimGray} SG,$z$}}
  \psline{->}(7.5,0.3)(8.5,0.3)
  \psline{->}(7.5,-0.3)(8.5,-0.3)
  \uput[0](8.5,0.3){\color{DimGray} $1$}
  \uput[0](8.5,-0.3){\color{DimGray} $0$}
  \end{pspicture}
  \vspace*{-2em}
\end{figure}

In den zweiten SG,$z$ geht der Zustand $\Ket{\psi}$ rein:
%
\begin{align*}
  \Ket{\psi} &= \Ket{x+}\Braket{x+|z+} + \Ket{x-}\Braket{x-|z+}
\end{align*}
%
Es handelt sich hier um eine >>echte<< Superposition. Der linke Summand beschreibt hierbei den oberen Laufweg und der rechte Summand den unteren Laufweg. $\Braket{x+|z+}$ und $\Braket{x-|z+}$ stellen jeweils Amplituden dar. Es folgt also weiter:
%
\begin{align*}
  &= \frac{1}{\sqrt{2}} \begin{pmatrix} 1 \\ 1 \end{pmatrix} \frac{1}{\sqrt{2}} + \frac{1}{\sqrt{2}} \begin{pmatrix} 1 \\ -1 \end{pmatrix} \frac{1}{\sqrt{2}} \\
  &= \begin{pmatrix} 1 \\ 0 \end{pmatrix} \\
  &= \Ket{z+}
\end{align*}
%
Für die Wahrscheinlichkeit gilt dann:
\[
  \text{prob}[\sigma_z \hateq +1 | \Ket{\psi}] = 1
\]
Damit zeigt sich, dass die mathematische Formulierung die Experimente beschreibt.

Wenn wir beim ersten SG,z den unteren Kanal durchgelassen hätten und den oberen blockiert, so gilt:
%
\begin{align*}
  \Ket{\psi} &= \Ket{x+}\Braket{x+|z-} + \Ket{x-}\Braket{x-|z-} \\
  &= \frac{1}{{2}} \begin{pmatrix} 1 \\ 1 \end{pmatrix} - \frac{1}{{2}} \begin{pmatrix} 1 \\ -1 \end{pmatrix} \\ 
  &=  \begin{pmatrix} 0 \\ 1 \end{pmatrix} \\
  &= \Ket{z-} 
\end{align*}
%
Es zeigt sich also, dass die relativen Phasen entscheidend sind.
