% Michael Schmid, 2012 Universität Stuttgart.
%
% Dieses Werk ist unter einer Creative Commons Lizenz vom Typ
% Namensnennung - Nicht-kommerziell - Weitergabe unter gleichen Bedingungen 3.0 Deutschland
% zugänglich. Um eine Kopie dieser Lizenz einzusehen, konsultieren Sie
% http://creativecommons.org/licenses/by-nc-sa/3.0/de/ oder wenden Sie sich
% brieflich an Creative Commons, 444 Castro Street, Suite 900, Mountain View,
% California, 94041, USA.

\renewcommand{\printfile}{appendix3}

\section{Statistischer Operator, bzw. Dichtematrix}

Wenn ein quantales System von einen einzigen Vektor in einem Hilbertraum beschrieben werden kann, so wird dies als \acct{reiner Zustand} bezeichnet. Somit liegt vollständige Kenntnis über das System vor. Ist dagegen nur eine Wahrscheinlichkeit $p_n$ gegeben für die das quantale System im Zustand $\Ket{\psi_n}$ ist, dann liegt keine vollständige Kenntnis vor, so wird dies als \acct{gemischter Zustand} bezeichnet, der mathematisch einenm \acct{statistischen Operator} entspricht.

\minisec{Im Fall des reinen Zustands}

Wenn wir von einem reinen Zustand ausgehen, kann der Erwartungswert eines Operators $\hat{A}$ wie folgt berechnet werden:
\[
  \Braket{\hat{A}} = \Braket{\phi|\hat{A}|\phi}.
\]
Dieser kann durch jede orthonomalbasis $\ket{n}$ von $\mathcal{H}$ dargestellt werden: 
%
\begin{align*}
  \Braket{\phi|\hat{A}|\phi} &= \sum_{n,m} \Braket{\phi|n}\Braket{n|\hat{A}|m}\Braket{m|\phi} \\
  &= \sum_{n,m} \Braket{m|\phi}\Braket{\phi|n}\Braket{n|\hat{A}|m} \\
  &= \sum_{n,m} \Braket{m|\mathcal{P}_\phi|n}\Braket{n|\hat{A}|m}\\
  &= \sum_{m} \Braket{m|\mathcal{P}_\phi \hat{A}|m} \\
  &= \mathrm{tr}\left(\mathcal{P}_\phi \hat{A}\right) = \mathrm{tr}\left( \hat{A}\mathcal{P}_\phi\right)
\end{align*}
%
$\mathcal{P}_\phi$ entspricht dem Projektionsoperator des Zustandes $\Ket{\phi}$. Es gelten folgende weitere Eigenschaften:
\begin{enum-roman}
  \item $\mathcal{P}_\phi^2 = \mathcal{P}_\phi$
  \item $\mathrm{tr}\left(\mathcal{P}_\phi\right) = 1$
\end{enum-roman}

\minisec{Im Fall des gemischten Zustands}

Für ein quantales System, das nicht in einem reinen Zustand ist sondern mit der Wahrscheinlichkeit $p_\alpha$ ($0\leq p_\alpha\leq 1$, $\sum_{\alpha} p_\alpha = 1$) im Zustand $\Ket{\phi_\alpha}$ und $\Ket{\phi_\alpha}$ normiert, aber nicht notwendigerweise orthogonal ist, definieren wir den statistischen Operator, bzw. die \acct{Dichtematrix/-operator} $\varrho$ wie folgt:
\[
   \boxed{\varrho = \sum_{\alpha} p_\alpha \Ket{\phi_\alpha}\Bra{\phi_\alpha} = \sum_\alpha p_\alpha \mathcal{P}_{\phi_\alpha}}
\]
Für den Erwartungswert eines Operators $\hat{A}$ im Zustand $\ket{\phi_\alpha}$ gilt somit:
\[
  \boxed{\Braket{\hat{A}} = \sum_{\alpha} p_\alpha \Braket{\phi_\alpha|\hat{A}|\phi_\alpha} = \mathrm{tr}\left(\hat{A} \varrho\right) = \mathrm{tr}\left(\varrho \hat{A}\right) }
\]
Es zeigt sich, dass ein reiner Zustand nur ein Spezialfall eines statistischen Zustandes ist, wenn also eines des $p_\alpha$'s genau Eins ist, und alle anderen gleich Null.
%
Es gelten folgende Eigenschaften:
\begin{enum-roman}
  \item $\varrho$ ist hermitesch: $\varrho = \varrho^\dagger$
  \item $\varrho$ hat die Spur 1: $\mathrm{tr}(\varrho) = 1$
  \item $\varrho$ ist ein positiver Operator: $\Braket{\phi|\varrho|\phi} \geq 0 \quad \forall \Ket{\phi}$
  \item $\varrho \neq \varrho^2$ (Gleichheit nur bei reinen Zuständen)
  \item $\mathrm{tr}(\varrho^2) \leq 1$ (Gleichheit nur bei reinen Zuständen)
\end{enum-roman}

\begin{notice*}
Die Dichtematrix stellt eine Wahrscheinlichkeitsverteilung über dem Hilbertraum dar.
\end{notice*}

\subsection{Zeitentwicklung eines statistischen Operators}

\minisec{Im Falle eines reinen Zustands}

Im Falle eines reinen Zustands nehmen wir eine zeitliche Abhängigkeit des Projektionsoperators $\mathcal{P}_\phi(t)$ an:
\[
  \mathcal{P}_\phi(t) = \Ket{\phi(t)}\Bra{\phi(t)}.
\]
Für die zeitliche Ableitung und unter Zuhilfenahme der Schrödingergleichung gilt:
%
\begin{align*}
  \partial_t \mathcal{P}_\phi(t) &= \partial_t \left(\Ket{\phi(t)}\Bra{\phi(t)}\right) \\
  &= \Ket{\dot{\phi}(t)}\Bra{\phi(t)} + \Ket{\phi(t)}\Bra{\dot{\phi}(t)} \\
  &\stackrel{\text{SG}}{=} -\frac{\mathrm{i}}{\hbar} \left( H(t) \mathcal{P}_\phi(t) - \mathcal{P}_\phi(t) H(t) \right) \\
  &= -\frac{\mathrm{i}}{\hbar} \left[H(t),\mathcal{P}_\phi(t)\right].
\end{align*}
%
\minisec{Im Falle eines gemischten Zustands}
Im Falle eines gemischten, zeitabhängigen Zustand gilt für die Dichtematrix:
\[
  \varrho(t) = \sum_{\alpha} p_\alpha \Ket{\phi_\alpha(t)}\Bra{\phi_\alpha(t)} .
\]
Somit folgt analog zu oben:
%
\begin{align*}
  \partial_t \varrho(t) &= \left\{ \sum_{\alpha} p_\alpha \Ket{\dot{\phi}_\alpha(t)}\Bra{\phi_\alpha(t)} + \sum_{\alpha} p_\alpha \Ket{\phi_\alpha(t)}\Bra{\dot{\phi}_\alpha(t)} \right\} \\
  &\stackrel{\text{SG}}{=} -\frac{\mathrm{i}}{\hbar} \left\{ \sum_{\alpha} p_\alpha H(t) \Ket{\phi_\alpha(t)}\Bra{\phi_\alpha(t)} - \sum_{\alpha} p_\alpha H(t) \Ket{\phi_\alpha(t)}\Bra{\phi_\alpha(t)} \right\} \\
  &= -\frac{\mathrm{i}}{\hbar} \left\{ H(t) \varrho(t) - \varrho(t) H(t) \right\} 
\end{align*}
%
Mit Hilfe des Kommutators erhalten wir so die \acct{Schrödingergleichung für Dichtematrizen}, welche durch 
\[
  \boxed{\partial_t \varrho(t) = -\frac{\mathrm{i}}{\hbar} \left[H(t),\varrho(t)\right]}
\]
gegeben ist. Als Lösung ergibt sich:
\[
  \varrho_t = \exp\left(-\frac{\mathrm{i}}{\hbar} H \, (t-t_0)\right) \varrho_0 \exp\left(\frac{\mathrm{i}}{\hbar} H \, (t-t_0)\right).
\]
Für den Anfangszustand $\varrho_0$ gilt:
\[
  \varrho_0 = \sum_{\alpha} p_\alpha \Ket{\phi_0}\Bra{\phi_0}.
\]
\subsection{Allgemeine Formulierung der quantenmechanischen Postulate}

\begin{theorem*}[P1:]
  Der Zustand eines quantalen Systems wird durch einen Dichteoperator $\varrho$ auf dem Hilbertraum $\mathcal{H}$ repräsentiert. $\varrho$ ist ein positiver Operator und hat die Spur 1.
\end{theorem*}

\begin{theorem*}[P2b:]
  Die Wahrscheinlichkeit $p_\alpha$ ein quantales System im Zustand $\Ket{\alpha}$ zu finden ist durch
  \[
    p_\alpha = \mathrm{tr}\left(\varrho \Ket{\alpha}\Bra{\alpha}\right) = \mathrm{tr}\left(\varrho \mathcal{P}_\alpha\right)
  \]
  gegeben.
\end{theorem*}

\begin{theorem*}[P2c:]
  Nach einer Messung befindet sich das quantale System im Zustand:
  \[
    \varrho' = \frac{\mathcal{P}_n \varrho \mathcal{P}_n}{\mathrm{tr}\left(\varrho \mathcal{P}_n\right)}.
  \] 
\end{theorem*}

\begin{theorem*}[P4:]
  Die zeitliche Entwicklung eines statistischen Operators wird durch 
  \[
    \partial_t \varrho(t) = -\frac{\mathrm{i}}{\hbar} \left[H(t),\varrho(t)\right]
  \]
  beschrieben.
\end{theorem*}

