% Henri Menke, Michael Schmid, Marcel Klett, 2012 Universität Stuttgart.
%
% Dieses Werk ist unter einer Creative Commons Lizenz vom Typ
% Namensnennung - Nicht-kommerziell - Weitergabe unter gleichen Bedingungen 3.0 Deutschland
% zugänglich. Um eine Kopie dieser Lizenz einzusehen, konsultieren Sie
% http://creativecommons.org/licenses/by-nc-sa/3.0/de/ oder wenden Sie sich
% brieflich an Creative Commons, 444 Castro Street, Suite 900, Mountain View,
% California, 94041, USA.

%
% Vorlesung vom 6.11.2012
%
\renewcommand{\printfile}{2012-11-06}

\subsection{Zusammenhang mit der Rotation}
\minisec{Klassisch:}
In klassischen Mechanik wird der Vektor $\bm{a}$ mit der Matrix $\mathscr{R}_z(\varepsilon)$ um den Winkel $\varepsilon$ um die positive $z$-Achse gedreht:

\begin{figure}[H]
  \centering
  \begin{pspicture}(-0.2,-0.2)(1,1)
    \psaxes[ticks=none,labels=none]{->}(0,0)(-0.2,-0.2)(1,1)
    \psline{->}(-0.1,-0.1)(0.8,0.8)
    \psline[linecolor=DarkOrange3]{->}(0,0)(0.8,0.5)
    \uput[-90](0.8,0.5){\color{DarkOrange3} $\bm{a}$}
  \end{pspicture}
  \vspace*{-2em}
\end{figure}

\begin{align*}
  \mathscr{R}_z(\varepsilon) &= \begin{pmatrix} \cos\varepsilon & - \sin\varepsilon &  0\\\sin\varepsilon & \cos\varepsilon & 0 \\0 & 0 & 1 \end{pmatrix} \stackrel{\text{entwickeln}}{=} \begin{pmatrix} 1 -\frac{\varepsilon^2}{2} & - \varepsilon &  0 \\ \varepsilon & 1 -\frac{\varepsilon^2}{2} & 0 \\ 0 & 0 & 1 \end{pmatrix} + \mathcal{O}(\varepsilon^3)
\end{align*}
%
Entsprechend erhält man für die Matrizen $\mathscr{R}_x(\varepsilon)$ und $\mathscr{R}_y(\varepsilon)$:
%
\begin{align*}
  \mathscr{R}_x(\varepsilon) &= \begin{pmatrix} 1& 0 &  0 \\ 0 & 1 -\frac{\varepsilon^2}{2} & -\varepsilon \\0 & \varepsilon & 1 -\frac{\varepsilon^2}{2} \end{pmatrix} + \mathcal{O}(\varepsilon^3)  &&  \mathscr{R}_y(\varepsilon) = \begin{pmatrix} 1 -\frac{\varepsilon^2}{2}& 0 &  \varepsilon \\ 0 & 1  & 0 \\-\varepsilon & 0 & 1 -\frac{\varepsilon^2}{2} \end{pmatrix} + \mathcal{O}(\varepsilon^3) 
\end{align*}
%
Das \acct{Lie'sche Produkt}, bzw. der Kommutator je zwei dieser Matrizen liefert: 
%
\begin{align*}
  \leadsto  \mathscr{R}_x(\varepsilon)\mathscr{R}_y(\varepsilon)-\mathscr{R}_y(\varepsilon)\mathscr{R}_x(\varepsilon) 
  &= \begin{pmatrix} 0& -\varepsilon^2 &  0 \\ \varepsilon^2 & 0 & 0 \\ 0 & 0 & 0 \end{pmatrix} + \mathcal{O}(\varepsilon^3)  \\
  &\approx \mathscr{R}_z(\varepsilon^2) - \mathds{1}  + \mathcal{O}(\varepsilon^3)
\end{align*}
%
In Verweis auf die klassische Mechanik ist dies keine Überraschung da die Matrizen $\mathscr{R}(\varepsilon)$ eine Lie'sche Gruppe bilden, deren Erzeugende $\mathscr{J}$ kommutieren (siehe klassische Mechanik >>infinitesimale Drehung<<).

\minisec{Quantal:}
Wollen wir den Spin $\ket{\psi}$ drehen, so dass gilt
\[
  \ket{\psi} \iff \Ket{\widetilde{\psi}}
\] 
wird ein Drehoperator $D_i$ benötigt für den gilt
\[
  \ket{\widetilde{\psi}} = D_{x/y/z} \Ket{{\psi}} .
\]
Es ist klar, dass der \acct{Drehoperator} $D_i$  nicht gleich $\mathscr{R}$ sein kann, da $\dim\mathcal{H}=2$ und $\dim\mathscr{R}=3$ gilt. Der Ansatz ist also folgender:
\[
  D_{x/y/z} \approx \mathds{1} - \frac{\mathrm{i}}{\hbar} \varepsilon \mathscr{J}_{x/y/z} .
\]
Wir verlangen, dass $D$ den selben Vertauschungsrelationen genügt wie in der obigen Betrachtung im klassischen Fall:
% 
\begin{align*}
  D_x(\varepsilon) D_y(\varepsilon)-& D_y(\varepsilon) D_x(\varepsilon) \\
  &=  \left(\mathds{1} - \frac{\mathrm{i}}{\hbar} \varepsilon \mathscr{J}_{x}\right) \left(\mathds{1} - \frac{\mathrm{i}}{\hbar} \varepsilon \mathscr{J}_{y}\right)  - \left(\mathds{1} - \frac{\mathrm{i}}{\hbar} \varepsilon \mathscr{J}_{y}\right) \left(\mathds{1} - \frac{\mathrm{i}}{\hbar} \varepsilon \mathscr{J}_{x}\right) \\
  &= -\frac{\varepsilon^2}{\hbar^2} \left( \mathscr{J}_{x} \mathscr{J}_{y} -\mathscr{J}_{y} \mathscr{J}_{x} \right) \\
  &= -\frac{\varepsilon^2}{\hbar^2} \left[\mathscr{J}_{x} , \mathscr{J}_{y} \right]
\end{align*}
%
Analog zur obigen Betrachtung muss die rechte Seite gleich $D_z(\varepsilon^2) -\mathds{1}$ sein, so dass gilt:
%
\begin{align*}
     D_x(\varepsilon)D_y(\varepsilon)- D_y(\varepsilon) D_x(\varepsilon) &\stackrel{!}{=}  D_z(\varepsilon^2) -\mathds{1} \\
     &= - \mathrm{i} \hbar \varepsilon^2 \mathscr{J}_{z}
\end{align*}
%
Durch Vergleich beider Seiten folgt:
%
\begin{align*}
  - \frac{\mathrm{i}}{\hbar} \varepsilon^2 \mathscr{J}_{z} &= -\frac{\varepsilon^2}{\hbar^2} \left[ \mathscr{J}_{x},\mathscr{J}_{y} \right] \\
  \implies \left[\mathscr{J}_{x} , \mathscr{J}_{y} \right] &= \mathrm{i} \hbar \mathscr{J}_{z}
\end{align*}
%
Durch Vergleich mit den $\sigma$-Matrizen (Pauli Matrizen) erhält man:
%
\begin{align*}
  \mathscr{J}_{i} &= \frac{\hbar}{2} \sigma_i \qquad i \in \{x,y,z\} \\
  &\stackrel{!}{=} S_i
\end{align*}
%
\minisec{Allgemeine Drehung um eine $n$-Achse}
\[
  D_n(\phi) = \exp\left(-\mathrm{i}\frac{\phi}{2} \bm{n} \cdot \bm{\sigma}\right) 
\]
%
\begin{example*} Es soll $\Ket{x+}$ um $\phi$ entlang der $z$-Achse gedreht werden:
%
\begin{align*}
  D_z(\phi) \Ket{x+} &=  \exp\left(-\mathrm{i} \frac{\phi}{2} \sigma_z \right) \Ket{x+} \\
  &= \exp\left(-\mathrm{i} \frac{\phi}{2} \sigma_z \right) \left( \frac{1}{\sqrt{2}} ( \Ket{z+} + \Ket{z-} ) \right) \\
  &= \frac{1}{\sqrt{2}} \left( \exp\left(-\frac{\mathrm{i}}{2} \phi \right) \Ket{z+} + \exp\left(\frac{\mathrm{i}}{2} \phi \right)\Ket{z-}\right)  \\
  &= \frac{1}{\sqrt{2}} \exp\left(-\frac{\mathrm{i}}{2} \phi \right) \left(\Ket{z} + \exp\left(\mathrm{i} \phi \right) \Ket{z-} \right)
\end{align*}
%
Für der Winkel $\pi/2$ und $2\pi$ folgt somit:
%
\begin{align*}
  D_z\left(\frac{\pi}{2}\right) &= \frac{1}{\sqrt{2}} \exp\left(-\mathrm{i}\frac{\phi}{4}\right) \left(\ket{z+} + \mathrm{i} \Ket{z-}\right) = \exp\left(-\mathrm{i}\frac{\phi}{4}\right) \Ket{y+} \\
  D_z(2\pi) &= \frac{1}{\sqrt{2}} (-1) \left(\ket{z+} + \Ket{z-}\right) = - \Ket{x+}
\end{align*}
%
Im zweiten Fall zeigt sich also, dass es eine zusätzliche Phase gibt. Diese Phase hat bestimmte Konsequenzen.
\end{example*}
%

\minisec{Konsequenzen der Phase}

\begin{figure}[H]
  \centering
  \vspace*{-2em}
  \begin{pspicture}(0,-1)(9,1)
  \uput[90](0.75,0){\color{DimGray} $\Ket{z+}$}
  \rput(2,0){\psframe(-0.5,-0.5)(0.5,0.5)\rput(0,0){\color{DimGray} SG,$x$}}
  \psline{->}(0,0)(1.5,0)
  \psline{-}(2.5,0.3)(4.9,0.3)
  \psline{-}(2.5,-0.3)(5.5,-0.3)
  \uput[90](3,0.3){\color{DimGray} $\Ket{x+}$}
  \uput[-90](3,-0.3){\color{DimGray} $\Ket{x-}$}
  \pscircle[linecolor=DarkOrange3,fillstyle=hlines,hatchcolor=DarkOrange3,hatchsep=2pt](5.2,0.3){0.3}
  \uput[45](5.2,0.5){\color{DarkOrange3} $\bm{B} = B_z \bm{e}_z = \omega \bm{e}_z = g \mu_B \frac{B_z}{\hbar} \bm{e}_z$}
  \pnode(5.5,0.3){A1}
  \pnode(5.5,-0.3){A2}
  \pnode(6.5,0){B}
  \ncdiag[angleA=0,angleB=180,linearc=0.2]{A1}{B}
  \ncdiag[angleA=0,angleB=180,linearc=0.2]{A2}{B}
  \rput(7,0){\psframe(-0.5,-0.5)(0.5,0.5)\rput(0,0){\color{DimGray} SG,$z$}}
  \psline{->}(7.5,0.3)(8.5,0.3)
  \psline{-|}(7.5,-0.3)(8,-0.3)
  \uput[0](8.5,0.3){\color{DimGray} $p_{+}(t)$}
  \psline{->}(6.3,-0.5)(6.3,-0.1)
  \uput[-60](6,-0.5){\color{DimGray} $\Ket{\psi(t)} = \dfrac{1}{\sqrt{2}} \left\{ \Ket{x-} + D_z(\phi=\omega t) \Ket{x+} \right\}$}
  \end{pspicture}
\end{figure}

\begin{figure}[H]
  \centering
  \begin{pspicture}(-0.2,-0.2)(3,1.5)
    \psaxes[ticks=none,labels=none]{->}(0,0)(-0.2,-0.2)(3,1.5)[\color{DimGray} $t$,0][\color{DimGray} $p_{+}(t)$,0]
    \psplot[linecolor=DarkOrange3]{0}{2}{cos(1.57079632679*x)^2}
    \uput[-90](1,0){\color{DimGray} $2 \pi / \omega$}
    \uput[-60](2,0){\color{DimGray} $4 \pi / \omega$}
    \uput[180](0,1){\color{DimGray} $1$}
    \psline(-0.1,1)(0.1,1)
    \psline(1,-0.1)(1,0.1)
    \psline(2,-0.1)(2,0.1)
    \psline[linestyle=dotted,dotsep=1pt](2,0)(2,1.5)
  \end{pspicture}
\end{figure}

\section[Allgemeine Konsequenzen aus der Schrödinger Gleichung für \texorpdfstring{$n$}{n}-Niveau Systeme]{Allgemeine Konsequenzen aus der Schrödinger Gleichung für \texorpdfstring{$n$}{n}-Niveau (\texorpdfstring{$\hateq\dim\mathcal{H}$}{=dim H}) Systeme}

\subsection{Der Zeitentwicklungsoperator}

Eine formale Lösung der Schrödinger Gleichung
\[
  \mathrm{i} \hbar \partial_t \Ket{\psi} = H \Ket{\psi}
\]
ist durch einen \acct{Zeitentwicklungsoperator} zu finden.
%
\begin{align*}
  \Ket{\psi(t)} &= U(t,t_0) \Ket{\psi(t_0)} \qquad \bigg|\cdot \mathrm{i} \hbar \partial_t \\
  H \Ket{\psi(t)} &= \mathrm{i} \hbar \partial_t U(t,t_0) \Ket{\psi(t_0)} \\
  H U(t,t_0) \Ket{\psi(t)} &= \mathrm{i} \hbar \partial_t U(t,t_0) \Ket{\psi(t_0)}\\
  \implies \mathrm{i} \hbar \partial_t U(t,t_0) &= H U(t,t_0)\\
\end{align*}
%
Mit $U(t_0,t_0)=\mathds{1}$ folgt:
%
\begin{align*}
  \left(-\mathrm{i} \hbar \partial_t U^\dagger (t,t_0)\right) &= U^\dagger \underbrace{H^\dagger}_{H} = U^\dagger H \\
  \implies \partial (U^\dagger U) &= (\partial_t U^\dagger) U + U^\dagger (\partial_t U) \\
  &= \frac{\mathrm{i}}{\hbar} \left(U^\dagger H U - U^\dagger H U \right) \\
  &= 0 
\end{align*}
%
Dies gilt aber nur, da $H$ hermitesch ist. Somit folgt also:
%
\begin{align*}
  U U^\dagger &= \mathds{1}
\end{align*}
%
$U$ ist somit unitär.

Nun kann in drei Fälle unterschieden werden:
\begin{enum-roman}
  \item $H$ ist zeitunabhängig:
  %
  \begin{align*}
    U(t) &= \exp\left(-\frac{i}{\hbar} H \cdot(t-t_0)\right) \\
    \implies \Ket{\psi(t)} &= \exp\left(-\frac{i}{\hbar} H \cdot(t-t_0)\right)  \Ket{\psi(t_0})
  \end{align*}
  %
  \item $H$ ist zeitabhängig, aber $[H(t_2),H(t_1)]= 0$ $\forall t_2,t_1$ (z.B.: $\bm{B}(t) = \bm{B}_0 f(t)$)
  \[
    \text{Lösung:} \qquad U(t,t_0) = \exp\left(-\frac{\mathrm{i}}{\hbar} \int\limits_{t_0}^{t}H(t') \; \mathrm{d}t' \right)
  \]
  \item $H(t)$ mit  $[H(t_2),H(t_1)] \neq 0$:
  \begin{example*} $\bm{B}(t) = B_0 \bm{e}_z + B_1 (\cos (\omega t) \bm{e}_x + \sin(\omega t) \bm{e}_y)$
  
  \begin{figure}[H]
    \centering
    \begin{pspicture}(-0.2,-0.2)(1,1)
      \psaxes[ticks=none,labels=none]{->}(0,0)(-0.2,-0.2)(1,1)
      \psline[linecolor=DarkOrange3]{->}(0,0)(0.8,0)
      \psline[linecolor=DarkOrange3]{->}(0,0)(0,0.8)
      \uput[180](0,0.6){\color{DarkOrange3} $B_0$}
      \uput[-90](0.6,0){\color{DarkOrange3} $B_1$}
      \psline(0,0)(0.8,0.8)
      \psarc[linecolor=DarkOrange3]{->}(0,0){0.5}{20}{70}
    \end{pspicture}
    \vspace*{-2em}
  \end{figure}
  
  \begin{align*}
    \implies H(t) &\propto \bm{\sigma} \cdot \bm{B}(t) \qquad \text{(Rabi !)} \\
    U(t) &= \text{formale Lösung (zeitgeordnete Operatoren)}
  \end{align*}
  %
  \end{example*}
\end{enum-roman}
%

\subsection{Stationäre Zustände}
Sei der Hamiltonoperator $H$ unabhängig von der Zeit $t$. So gilt für die Eigenzustände:
\[
  H \Ket{n} = E_n \Ket{n} \qquad n=\{0, \ldots,N-1 \}
\]
O.B.d.A sei $n=0$ der Grundzustand $E_0 = 0$.

\begin{enum-arab}
  \item Sei $\Ket{\psi(t)}= \Ket{n}$:
  %
  \begin{align*}
    \Ket{\psi(t)} &= U(t,t_0) \Ket{n} \\
    &= \exp\left(-\frac{\mathrm{i}}{\hbar} H(t-t_0)\right) \Ket{n} \\
    &= \exp\left(-\frac{\mathrm{i}}{\hbar} E_n(t-t_0)\right) \Ket{n} 
    \intertext{Mit $\omega_n = E_n/\hbar$ folgt:}
    &= \exp\left(-\mathrm{i} \omega_n (t-t_0)\right) \Ket{n} . \\
  \end{align*}
  %
  Eigenzustände nehmen ausschließlich eine zeitabhängige Phase an. Für die Wahrscheinlichkeit den Zustandes $\Ket{\chi}$ im Zustand
  \[
    \Ket{\psi(t)} = \exp\left(-\mathrm{i}\omega_n (t-t_0)\right) \Ket{n}
  \]
  zu finden ist:
  %
  \begin{align*}
   \mathrm{prob}[P_{\chi}=|\Ket{\psi(t)}] &= |\Braket{\chi|\psi(t)}|^2 \\
   &=  |\Braket{\chi|\psi(t_0)}|^2 \\
   &= |\Braket{\chi|n}|^2 \qquad \text{zeitunabhängig}
  \end{align*}
  %
  \item Für beliebige Anfangszustände gilt für die allgemeine Zeitentwicklung:
  %
  \begin{align*}
    \Ket{\psi(t_0)} &= \sum\limits_{n=0}^{N-1} c_n \Ket{n}\\
    \implies \Ket{\psi(t)} &= \sum\limits_{n=0}^{N-1} c_n \exp(-\mathrm{i} \omega_n(t-t_0)) \Ket{n}
  \end{align*}
  Letzteres folgt per Superposition.
  %
\end{enum-arab} 
