% Henri Menke, Michael Schmid, Marcel Klett, 2012 Universität Stuttgart.
%
% Dieses Werk ist unter einer Creative Commons Lizenz vom Typ
% Namensnennung - Nicht-kommerziell - Weitergabe unter gleichen Bedingungen 3.0 Deutschland
% zugänglich. Um eine Kopie dieser Lizenz einzusehen, konsultieren Sie
% http://creativecommons.org/licenses/by-nc-sa/3.0/de/ oder wenden Sie sich
% brieflich an Creative Commons, 444 Castro Street, Suite 900, Mountain View,
% California, 94041, USA.

%
%    Vorlesung 9.11.2012 Teil 1
%
\renewcommand{\printfile}{2012-11-9}

\subsection{Zeitentwicklung eines Erwartungswert}

Sei $A$ eine physikalische Größe die eventuell zeitabhängig ist ($A(t)$)
%
\begin{align*}
  \braket{A(t)}_{\Ket{\psi}} &= \Braket{\psi(t)|A(t)|\psi(t)} \\
  \frac{\mathrm{d}}{\mathrm{d}t} \braket{A(t)}_{\Ket{\psi}} &= \frac{\mathrm{d}}{\mathrm{d}t} \Braket{\psi(t)|A(t)|\psi(t)} 
\end{align*}
%
Mit $\mathrm{i} \hbar \mathrm{d}_t \Ket{\psi} = H\Ket{\psi}$ folgt:
%
\begin{gather*}
  \begin{aligned}
    &= \frac{\mathrm{i}}{\hbar} \Braket{\psi(t) H(t)|A(t)|\psi(t)} - \frac{\mathrm{i}}{\hbar} \Braket{\psi(t) |A(t)H(t)|\psi(t)} + \Braket{\psi(t)|(\partial_t A(t))|\psi(t)} \\
    &= \frac{\mathrm{i}}{\hbar} \Braket{\psi(t)|[H(t),A(t)]|\psi(t)} + \Braket{\psi(t)|(\partial t A(t))|\psi(t)}
  \end{aligned} \\
  \boxed{\frac{\mathrm{d}}{\mathrm{d}t} \braket{A(t)}_{\Ket{\psi}} = \frac{\mathrm{i}}{\hbar} \Braket{[H(t),A(t)]}_{\Ket{\psi(t)}} + \Braket{\partial_t A(t)}_{\Ket{\psi(t)}}}
\end{gather*}
%
Dies ist das \acct{Ehrenfest Theorem}. Zumeist verschwindet hierbei der hintere  $\Braket{\partial_t A(t)}_{\Ket{\psi(t)}}$, da eine physikalische Größe (Observable) selten explizit zeitabhängig ist.

Aus dem Ehrenfesttheorem folgt sofort:
\begin{enumerate}
  \item Für $A=H$:
  
   $[H,H] = 0$ und H zeitunabhängig ($\partial_t H = 0$) ist der Erwartungswert erhalten.
  \item falls $[A,H] = 0$ und $\partial_t A = 0$ Erwartungswert von A bleibt erhalten. Darüber hinaus gilt dann auch:
  %
  \begin{align*}
    \frac{\mathrm{d}}{\mathrm{d}t} \Ket{f(A)} &= 0 \\
    \implies \frac{\mathrm{d}}{\mathrm{d}t} \mathrm{prob}[A\hateq a_n | \psi(t)] &= 0
  \end{align*}
  %
\end{enumerate}

Ein Analogon zur klassischen Mechanik ist hierbei die >>Poisson-Klammer<<, die wie folgt funktioniert:
%
\begin{align*}
  \frac{\mathrm{d}}{\mathrm{d}t} f(q_i,p_i,t) &= \{ f,H \} + \partial_t f
\end{align*}
%
\subsection{Energie-Zeit Unschärfe}

Die Zeit ist kein Operator, d.h. die Heisenbergsche-Unschärferelation ist nicht naiv anwendbar.
Mögliche Form:
\begin{enumerate}
  \item Ehrenfest für $A$ zeitunabhängig:
  %
  \begin{align*}
    \frac{\mathrm{d}}{\mathrm{d}t} \Braket{A}_{\psi(t)} &= - \frac{\mathrm{i}}{\hbar} \Braket{[A,H]}_{\psi(t)}
  \end{align*}
  %
  \item Heisenberg:
  %
  \begin{align*}
    (\Delta A)_{\psi(t)} \cdot (\Delta B)_{\psi(t)} &\geq \frac{1}{2} \Braket{[A,B]}_{\psi(t)} \\ 
    B&=H \\
    (\Delta A)_{\psi(t)} \cdot (\Delta B)_{\psi(t)} &\geq \frac{1}{2} \hbar \left| \frac{\mathrm{d}}{\mathrm{d}t} \Braket{A}_{\psi(t)} \right| 
  \end{align*}
  %
  \item Definition: charakteristische Zeit für Veränderung von $\Braket{A}$
  %
  \begin{align*}
    \frac{1}{\tau_{\psi} (A)} & = \left| \frac{\mathrm{d}}{\mathrm{d}t} \Braket{A} \right| \cdot \frac{1}{(\Delta A)}
  \end{align*} 
  %
  Durch Einsetzen der obigen Gleichungen ineinander folgt:
  %
  \begin{align*}
    (\Delta H)_{\psi} \cdot \tau_{\psi} (A) &\geq  \frac{\hbar}{2}
  \end{align*}
  %
  Hierbei gilt für die einzelnen Glieder:
  %
  \begin{align*}
    (\Delta H)_{\psi} &&\text{Energieunschärfe} \\
    \tau_{\psi} (A) &&\text{typische Zeit für A-Änderungen}
  \end{align*}
  %
\end{enumerate}

\subsection{Schrödinger vs. Heisenberg-Bild}

Bisher waren Zustände zeitabhängig und Operatoren im allgemeinen zeitunabhängig. Diese Darstellung der Quanten-Mechanik nennt man \acct{Schrödinger-Bild}.

Alternativ kann man zur Berechnung von Erwartungswerten auch in das \acct{Heisenberg-Bild} übergehen. 

Sei der Zustand $\Ket{\psi}$, als auch der Operator $A$ gegeben:
%
\begin{align*}
  \Braket{A}_{\psi} &= \Braket{\psi(t)|A|\psi(t)} \\
  &= \Bigg\langle \psi(t_0) \Bigg| \underbrace{\exp\left(\frac{\mathrm{i}}{\hbar}H(t-t_0)\right) A \exp\left(-\frac{\mathrm{i}}{\hbar}H(t-t_0)\right)}_{\coloneq A_H(t)} \Bigg| \psi(t_0)) \Bigg\rangle \\
  &= \Braket{\psi_H | A_H | \psi_H} \qquad
\end{align*}
%
Mit 
%
\begin{align*}
  \psi_H &= \Ket{\psi(t_0)} \\
  A_H &= \exp\left(\frac{\mathrm{i}}{\hbar}H(t-t_0)\right) \, A \, \exp\left(-\frac{\mathrm{i}}{\hbar}H(t-t_0)\right)
\end{align*}
%
Dabei sind hier nun die Operatoren zeitabhängig und die Zustände zeitunabhängig. Der Übergang vom Schrödinger-Bild in das Heisenberg-Bild erfolgt durch eine unitäre Transformation.
%
\begin{align*}
  \Ket{\psi} &= \underbrace{\exp\left(\frac{\mathrm{i}}{\hbar} H (t-t_0)\right)}_{U^\dagger(t,t_0)} \Ket{\psi(t)} = \Ket{\psi(0)} \\
  A_H &= U^\dagger (t,t_0) A U (t,t_0) \\
  \implies \mathrm{i} \hbar \frac{\mathrm{d}}{\mathrm{d}t} A_H &= [A_H, H_H] + \mathrm{i} \hbar \underbrace{\left(\frac{\partial A}{\partial_t} \right)_H}_{\text{falls $A_s(t)$}}
\end{align*}
%
Dabei steht $A_s$ für den Operator $A$ im Schrödinger-Bild, der explizit zeitabhängig ist.

