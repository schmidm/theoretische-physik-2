% Henri Menke, Michael Schmid, Marcel Klett 2013 Universität Stuttgart.
%
% Dieses Werk ist unter einer Creative Commons Lizenz vom Typ
% Namensnennung - Nicht-kommerziell - Weitergabe unter gleichen Bedingungen 3.0 Deutschland
% zugänglich. Um eine Kopie dieser Lizenz einzusehen, konsultieren Sie
% http://creativecommons.org/licenses/by-nc-sa/3.0/de/ oder wenden Sie sich
% brieflich an Creative Commons, 444 Castro Street, Suite 900, Mountain View,
% California, 94041, USA.

%
% Vorlesung am 25.01.2013
%
\renewcommand{\printfile}{2013-01-25}

\begin{notice*}[Repetition:]
\begin{align*}
  H_0 &\to H \equiv H_0 + \lambda H_1 \\
  E_\alpha &\to E_a = E_\alpha + \lambda \Braket{\alpha|H_1|\alpha} + \lambda^2 \sum\limits_{\beta \neq \alpha} \frac{\left|\Braket{\beta|H_1|\alpha}\right|^2}{E_\alpha - E_\beta} + \mathcal{O}(\lambda^3)
\end{align*}

\begin{figure}[H]
  \centering
  \begin{pspicture}(-0.3,-0.3)(1.5,1.5)
    \psaxes[labels=none,ticks=none]{->}(0,0)(-0.5,-0.5)(1.5,1.5)[\color{DimGray} $\lambda$, 0][\color{DimGray} $E_a$, 0]
    \psyTick(0.5){}
    \psyTick(0.75){}
    \psyTick(1.0){}
    \pscurve[linecolor=DarkOrange3](0,0.5)(0.5,0.45)(1,0.3)
    \pscurve[linecolor=DarkOrange3](0,0.75)(0.5,0.85)(1,1)
    \pscurve[linecolor=DarkOrange3](0,1.0)(0.5,1.1)(1,1.25)
  \end{pspicture}
\end{figure}

\end{notice*}

\begin{example*} Harmonischer Oszillator

Wir betrachten folgenden Hamiltonoperator:
\[
  H = \underbrace{\underbrace{\frac{p^2}{2m} + \frac{m}{2}\omega^2 x^2}_{\hateq \text{ Harm. Osz.}} + \lambda x^4}_{\hateq \text{  gestörtes System}}
\]
Für das Energiespektrum und das Potential gelten folgende Skizzen:

\begin{figure}[H]
  \centering
  \begin{pspicture}(-2,-0.3)(2,2.5)
    \psaxes[labels=none,ticks=none]{->}(0,0)(-2,-0.3)(2,2.5)[\color{DimGray} $x$, 0][\color{DimGray} $V(x)$, 0]
    \psplot[yMinValue=-0.3,yMaxValue=2.5]{-2}{2}{x^2}
    \psplot[yMinValue=-0.3,yMaxValue=2.5]{-2}{2}{2*x^2}
    \psplot[yMinValue=-0.3,yMaxValue=2.5,linecolor=DarkOrange3]{-2}{2}{0.5*x^2}
    \psplot[yMinValue=-0.3,yMaxValue=2.5,linecolor=MidnightBlue]{-1.57583}{1.57583}{0.5*x^2-0.25*x^4}
    \psplot[plotpoints=200]{-2}{2}{0.5*2.7182818284590451^(-(x/0.2)^2)}
  \end{pspicture}
  \hspace*{5em}
  \begin{pspicture}(-0.5,-0.5)(1.5,2.5)
    \psaxes[labels=none,ticks=none]{->}(0,0)(-1.5,-0.3)(1.5,2.5)[\color{DimGray} $\lambda$, 0][\color{DimGray} $E$ [$\hbar \omega$], 0]
    \psplot[linecolor=DarkOrange3]{-1}{1}{0.5*x/(1+0.28*x^2)+0.5}
    \psplot[linecolor=DarkOrange3]{-1}{1}{0.5*x/(1+0.28*x^2)+1}
    \psplot[linecolor=DarkOrange3]{-1}{1}{0.5*x/(1+0.28*x^2)+1.5}
    \psplot[linecolor=DarkOrange3]{-1}{1}{0.5*x/(1+0.28*x^2)+2}
    \psyTick[labelFontSize=\color{DimGray}](0.5){\frac{1}{2}}
    \psyTick[labelFontSize=\color{DimGray}](1){\frac{3}{2}}
    \psyTick[labelFontSize=\color{DimGray}](1.5){\frac{5}{2}}
    \psyTick[labelFontSize=\color{DimGray}](2){\frac{7}{2}}
  \end{pspicture}
\end{figure}

Es stellt sich heraus, dass in einem Potential mit negativen $\lambda$ $(\lambda < 0)$ es keine gebundenen Zustände gibt, da eine Gaußfunktion (Teilchen) in solch einem Potential zerfließen und tunneln würde (siehe Skizze).

Für negative $\lambda$ ist die Konvergenz also heikel, was die volle Rechnung zeigt:
\[
  E_n(\lambda) = \hbar \omega \left(n  + \frac{1}{2}\right) + A_1(\lambda) + A_2(\lambda).
\]
Hierbei folgt $A_1(\lambda)$ aus der Störungstheorie, $A_2(\lambda)$ ist von der Ordnung $\exp\left(-1/\lambda\right)$ und wird als \acct{nicht perturbartiver Term} bezeichnet.

Das Beispiel zeigt, dass die Störungstheorie nur für kleine positive $\lambda$ beim gestörten harmonischen Oszillator funktioniert. Für $\lambda < 0$ gibt es nicht perturbative Terme und keine gebundenen Zustände. Perturbative Terme folgen nicht aus der Störungstheorie.
\end{example*}

\subsection{Mit Entartung}

Relevant für die Aufhebung der Rotationsentartung.

Sei $\mathcal{D}$ die Menge der Zustände $\{\ket{\alpha}\}$, wobei der Eigenwert $\alpha$ $N$-fach entartet ist. $\mathcal{D}$ hat also $N$ Elemente.

Wir verwenden folgenden Ansatz:
\[
  \Ket{a} = \sum\limits_{\alpha \in \mathcal{D}} c_\alpha \Ket{\alpha} + \sum\limits_{\mu \neq \mathcal{D}} d_\mu \Ket{\mu} \quad \text{mit} \quad d_\mu = \mathcal{O}(\lambda).
\] 
Setzt man dies in 
\[
  \left(H - E_\alpha\right) \Ket{a} = 0 
\]
ein, so folgt:
\[
  \left(H_0 + \lambda H_1 - E_a\right) \Ket{\sum\limits_{\alpha \in \mathcal{D}} c_\alpha \Ket{\alpha} + \sum\limits_{\mu \neq \mathcal{D}} d_\mu \Ket{\mu}} = 0.
\]
\begin{enum-arab}
  \item Projektion auf $\Bra{\nu}\notin \mathcal{D}$:
  %
  \begin{align}
    \implies 0 &= \lambda \sum\limits_{\alpha \in \mathcal{D}} c_\alpha \Braket{\nu|H_1|\alpha} + d_\nu \left(E_\nu - E_a\right) + \mathcal{O}(\lambda^2) \nonumber \\
    \implies d_\nu &= \lambda \sum\limits_{\alpha \in \mathcal{D}} c_\alpha \frac{\Braket{\nu|H_1|\alpha}}{E_a - E_\nu} + \mathcal{O}(\lambda^2) \label{eq: Stör 1}
  \end{align}
  %
  \item Projektion auf $\Bra{\beta}\in \mathcal{D}$:
  %
  \begin{align}
    \implies \sum\limits_{\alpha } \left(E_\alpha - E_a\right) \delta_{\alpha,\beta} c_\alpha + \lambda \sum\limits_{\alpha} c_\alpha  \Braket{\beta|H_1|\alpha} + \lambda \sum\limits_{\mu} d_\mu \Braket{\beta|H_1|\mu} = 0 \label{eq: Stör 2}
  \end{align}
  %
\end{enum-arab}

Einsetzten von \eqref{eq: Stör 1} in \autoref{eq: Stör 2} liefert:
\[
  0 = \sum\limits_{\alpha} c_\alpha \bigg\{ \left(E_\alpha - E_a\right) \delta_{\alpha,\beta} + \underbrace{\lambda \Braket{\beta|H_a|\alpha} + \lambda^2 \sum\limits_{\mu} \frac{\left|\Braket{\beta|H_1|\mu}\right|^2}{E_a - E_\mu}}_{\equiv \Braket{\beta|H_{\text{eff}}|\alpha}} \bigg\}.
\]
$\Braket{\beta|H_{\text{eff}}|\alpha}$ wird als \acct{effektives Matrixelement} bezeichnet. Somit vereinfacht sich die Gleichung zu:
\[
  0 = \sum\limits_{\alpha} c_\alpha \bigg\{ \left(E_\alpha - E_a\right) \delta_{\alpha,\beta} + \Braket{\beta|H_{\text{eff}}|\alpha} \bigg\} \qquad \forall \beta .
\]
Dies sind $N$ Gleichungen für $N$ Unbekannte. Das lineare Gleichungssystem hat genau dann eine Lösung, wenn $\det=0$ gilt! Dies führt auf die $E_a$. Es muss gelten:
\[
  \det\left(\left(E_\alpha- E_a\right)\delta_{\alpha,\beta} + \Braket{\beta|H_{\text{eff}}|\alpha}\right) = 0.
\]  
Als Lösung erhält man ein Polynom $N$-ter Ordnung für $E_a$

\begin{example*} 3 Niveau System $\left\{\Ket{1},\Ket{2},\Ket{3}\right\}$

Wir betrachten folgenden ungestörten Teil $H_0$:
\[
  H_0 = \begin{pmatrix} 0 & 0 & 0 \\ 0 & 0 & 0 \\ 0 & 0  & \Delta   \end{pmatrix} \quad \Delta>0 
\]
Die Diagonalelemente geben die jeweiligen Grundzustandsenergie mit ihrer Entartung an, so kommt ein zweifach entarteter Grundzustand für 
\[
  \alpha = 1,2 \hateq 0 \quad \text{für} \quad E_\alpha,
\]  
vor. Mit dem Störterm $H_1$:
\[
  H_1 = \begin{pmatrix} 0 & 0 & M \\ 0 & 0 & M \\ M & M  & 0   \end{pmatrix} \quad \text{mit} \quad M=\Braket{1|H_1|3},
\]
folgt für das effektive Matrixelement:
%
\begin{align*}
  \Braket{\alpha|H_{\text{eff}}|\beta} &= \left(\lambda^2 \sum\limits_{\mu=3} \frac{M^2}{-\Delta}\right) \\
  &= -\underbrace{\frac{\lambda^2 M^2}{\Delta^2}}_{\equiv x^2} \begin{pmatrix} \Delta & \Delta  \\ \Delta & \Delta \end{pmatrix}
\end{align*}
%
Für die Berechnung der $E_a$ nach obigen Schema folgt:
%
\begin{align*}
  \det\left(-x^2 \begin{pmatrix} \Delta & \Delta  \\ \Delta & \Delta \end{pmatrix} - E_a \begin{pmatrix} 1 & 0 \\ 0 & 1 \end{pmatrix}\right) &\stackrel{!}{=} 0 \\
  \implies \left(x^2 \Delta + E_a^2\right) - x^4 \Delta^2 &= 0
\end{align*}
%
Diese Gleichung hat $2$ Lösungen:
\[
  E_a = 0 = E_\alpha \equiv E_A, \qquad E_a = E_S = -2 \Delta x^2.
\]
Der Index $A$, bzw. $S$ steht für antisymmetrische, bzw. symmetrische Lösung. Somit ergibt sich 
\[
  \Ket{E_S} = \frac{1}{\sqrt{2}} \left(\Ket{1}+\Ket{2}\right), \qquad \ket{E_A} = \frac{1}{\sqrt{2}} \left(\Ket{1} - \Ket{2}\right)
\]
Für $E_3$ gilt dann:
%
\begin{align*}
  E_3 &= \Delta + \Braket{3|H_1|3} + \sum\limits_{\alpha=1,2} \frac{\left|\Braket{\alpha|H_1|3}\right|^2}{-\Delta} \\
  &= \Delta + 2 x^2 \Delta
\end{align*}

\begin{figure}[H]
  \centering
  \begin{pspicture}(-0.3,-0.3)(1.5,1.5)
    \psaxes[labels=none,ticks=none]{->}(0,0)(-0.5,-0.5)(1.5,1.5)[\color{DimGray} $\lambda$, 0][\color{DimGray} $E$, 0]
    \psyTick(1.0){}
    \pscurve[linecolor=DarkOrange3](0,0)(0.5,0)(1,0)
    \pscurve[linecolor=DarkOrange3](0,0)(0.5,-0.1)(1,-0.3)
    \pscurve[linecolor=DarkOrange3](0,1.0)(0.5,1.1)(1,1.3)
    \psdots*[linecolor=DarkOrange3](0,0)(0,1.0)
  \end{pspicture}
\end{figure}

Vgl. mit der exakten Lösung ($H_0 + \lambda H_1$ diagonalisieren):
%
\begin{align*}
  E_A &= 0 \\
  E_S &= \frac{\Delta}{2} \left(1 - \sqrt{1+x^2}\right) \approx -2 \Delta x^2 \\
  E_3 &= \frac{\Delta}{2} \left(1 + \sqrt{1+x^2}\right) \approx \Delta + 2 x^2 \Delta .
\end{align*}
%
Das Beispiel zeigt, dass die Näherung durchaus gelungen ist.
\end{example*}
