% Henri Menke, Michael Schmid, Marcel Klett 2013 Universität Stuttgart.
%
% Dieses Werk ist unter einer Creative Commons Lizenz vom Typ
% Namensnennung - Nicht-kommerziell - Weitergabe unter gleichen Bedingungen 3.0 Deutschland
% zugänglich. Um eine Kopie dieser Lizenz einzusehen, konsultieren Sie
% http://creativecommons.org/licenses/by-nc-sa/3.0/de/ oder wenden Sie sich
% brieflich an Creative Commons, 444 Castro Street, Suite 900, Mountain View,
% California, 94041, USA.

%
% Vorlesung am 15.01.2013
%
\renewcommand{\printfile}{2013-01-15}

\begin{notice*}[Wiederholung zum Zentralpotential in $3$-Dimensionen:]
  Für den Hamiltoperator gilt:
  \[
    H = \frac{\bm{p}^2}{2 \mu} + V(x^2+y^2+z^2).
  \]
  Ziel ist es die Eigenwerte und und Eigenzustände zu bestimmen. Hierfür betrachten wir die stationäre Schrödingergleichung
  \[
    H\ket{\psi} = E \Ket{\psi} .
  \]
  Die Rotationsinvarianz des Potentials legt die Verwendung von Kugelkoordinaten in der Ortsdarstellung nahe. Es erweist sich auch als nützlich den Laplaceoperator in Kugelkoordinaten zu kennen.
  %
  \begin{align*}
    \Braket{r,\theta,\phi|H|\psi} &= E \psi(r,\theta,\phi) 
  \end{align*}
  %
   Es erweist sich auch als nützlich den Laplaceoperator in Kugelkoordinaten zu kennen, so dass weiter gilt:
  %
  \begin{align*}
    \Braket{r,\theta,\phi|H|\psi} &= \bigg[ -\frac{\hbar^2}{2 \mu} \left(\frac{1}{r^2} \partial_r (r^2 \partial_r)\right) + V(r) \\ & \qquad
    - \underbrace{\frac{\hbar^2}{2 \mu} \left(\frac{1}{\sin(\theta)} \partial_\theta \sin(\theta) \partial_\theta + \frac{1}{\sin^2(\theta) \partial_\phi^2} \right)}_{= \frac{\bm{L^2}}{2 \mu r^2}} \bigg] \psi(r,\theta,\phi) 
  \end{align*}
  %
  \begin{enum-roman}
    \item $\bm{L}^2$ hat die Eigenfunktionen $Y_{\ell m}(\theta,\phi)$ zu den Eigenwerten $\hbar^2 \ell(\ell+1)$ mit $\ell=0,1,2,\ldots$
    \item Separationsansatz:
    %
    \begin{align*}
      \psi(r,\theta,\phi) &= R_{n\ell}(r)Y_{\ell m}(\theta,\phi) 
    \end{align*}
    %
    Somit folgt:
    %
    \begin{align*}
      \bigg[ -\frac{\hbar^2}{2 \mu} \underbrace{\frac{1}{r^2} \partial_r r^2 \partial_r}_{\partial_r^2 + \frac{2}{r} \partial_r} + \frac{\hbar^2 \ell(\ell+1)}{2 \mu r^2} +V(r) \bigg] R_{n\ell}(r) &= E_n R_{n\ell}(r) 
    \intertext{$\implies$ neue Funktion $u_{n\ell}(r) \coloneq 1/r \, R_{n\ell}(r)$:}
      \bigg[ -\frac{\hbar^2}{2 \mu} \partial_r^2 + \underbrace{\frac{\hbar^2 \ell(\ell+1)}{2 \mu r^2} +V(r)}_{= V_{\text{eff}(r)}} \bigg] u_{n\ell}(r) &= E_n u_{n\ell}(r) \\
    \end{align*}
    %
  \end{enum-roman}
\end{notice*}

\subsection{Coulomb-Potential und Wasserstoffatom}

Das \acct{Wasserstoffatom} stellt eines der wenigen analytisch lösbaren physikalischen Probleme der Quantenmechanik dar und ist gerade deshalb prädestiniert dafür um Verständnis über den Aufbau und den Eigenschaften von Atomen zu erhalten. Zu diesem Zweck wählen wir für das Potential, dass \acct{Coulomb-Potential}
\[
  V(r) = -\frac{Z e^2}{r}.
\]  
Hierbei entspricht $Z$ der Kernladungszahl und $e$ der Elementarladung. Die  dreidimensionale Schrödinergleichung des Wasserstoffatoms führt uns wieder auf einen Separationsansatz und einer daraus resultierenden Radialgleichung (Rotationsinvarianz). Nach beidseitiger Multiplikation mit $\mu/\hbar^2$  geht die Radialgleichung in:
\[
  \bigg[\underbrace{ -\frac{1}{2} \partial_r^2 + \frac{\ell(\ell+1)}{2 r^2} + \frac{\gamma}{r}}_{\coloneq H_\ell} \bigg] u_{n\ell}(r) = \mathcal{E}_n u_{n\ell}(r)
\]
über. Für die einzelnen Terme gilt:
\[
  \gamma = \frac{Ze^2 \mu}{\hbar^2} \quad,\quad \mathcal{E}_n = \frac{\mu}{\hbar^2} E_n .
\]
Es gibt verschiedene Lösungsstrategien:
\begin{enum-arab}
  \item Asymptotik suchen und absapaltem, Potenzreihenansatz, Konvergenz durch Abbruch führt auf die quantisierten Energien (Fobenius-Methode)
  \item Supersymmetrische Quantenmechanik
\end{enum-arab}
Wir bedienen uns der Methode der SUSY. Dafür führen wir neue verallgemeinerte Leiteroperatoren ein:
%
\begin{align*}
  Q_\ell &= \frac{1}{\sqrt{2\ell}} \left(\partial_r - \frac{\ell+1}{r}+\frac{\gamma}{\ell+1}\right) \\
  Q_\ell^\dagger &= \frac{1}{\sqrt{2\ell}} \left(-\partial_r - \frac{\ell+1}{r}+\frac{\gamma}{\ell+1}\right).
\end{align*}
%
Mit $\eta_\ell = \frac{\gamma^2}{2 (\ell+1)^2}$ lautet $H_\ell$ somit:
\[
  \boxed{H_\ell = Q_\ell^\dagger Q_\ell - \eta_\ell}
\]
Der supersymmetrische Partner von $H_\ell$ ist $H_{\ell+1}$ und lautet:
\[
  \boxed{H_{\ell+1} = Q_\ell Q_\ell^\dagger - \eta_\ell}
\]
Wir suchen $\omega_\ell(r)$, so dass $Q_\ell \omega_\ell(r)= 0$. Es folgt also:
%
\begin{align*}
  \omega_\ell'(r) - \frac{\ell+1}{r} \omega_\ell(r) + \frac{\gamma}{\ell+1} \omega_\ell(r) &)= 0 
\end{align*}
%
Durch Umordnung der Gleichung erhält man:
\[
  \omega_\ell'(r) = \left(\frac{\ell+1}{r} - \frac{\gamma}{\ell+1}\right) \omega_\ell(r).
\]
Als Lösung dieser Differentialgleichung erhält man:
\[
\omega_\ell(r) = N r^{\ell+1} \exp\left(-\frac{\gamma}{\ell+1} r\right), \qquad N\in \mathbb{C}.
\]
Die Normierungskonstante $N$ erhält man aus der Normierung:
\[
  |N|^2 \int\limits_{0}^{\infty} r^{2(\ell+1)+1} \exp\left(-\frac{2\gamma}{\ell+1} r\right) \; \mathrm{d}r \stackrel{!}{=} 1.
\]
Die Normierungskonstante ist somit:
\[
  N = 4^{1+\ell/2} \left(\frac{\gamma}{\ell+1}\right)^{2+\ell} \frac{1}{(3+2\ell)!}.
\]
Dies bedeutet für:
%
\begin{align*}
  H_\ell \omega_\ell(r) &= \left(Q_\ell^\dagger Q_\ell - \eta_\ell\right) \omega_\ell(r) \\
  &= - \eta_\ell \omega_\ell(r) \\
  &= -\frac{\gamma^2}{2(\ell+1)^2} \omega_\ell(r).
\end{align*}
%
Es zeigt sich, dass die $\omega_\ell$ Eigenfunktionen von $H_\ell$ zum Eigenwert
\[
  -\frac{\gamma^2}{2} \frac{1}{(\ell+1)^2} \frac{\hbar^2}{\mu} \coloneq E_{\ell+1}
\]
sind. 

\begin{theorem*}[Behauptung:]
  $Q_\ell^\dagger Q_\ell$ hat keine negativen Eigenwerte.
  \begin{proof*}
    Wir führen einen Widerspruchsbeweis indem wir zunächst annehmen:
    \[
      Q_\ell^\dagger Q_\ell \Ket{f_\ell} = -\alpha_\ell \Ket{f_\ell} \quad \text{mit} \quad -\alpha_\ell<0.
    \]
    Somit folgt:
    %
    \begin{align*}
      \Braket{f_\ell|Q_\ell^\dagger Q_\ell|f_\ell} &= -\alpha_\ell \Braket{f_\ell|f_\ell} < 0 \\
      &= \Vert Q_\ell\Ket{f_\ell}\Vert >0
    \end{align*}
    %
    Dies stellt offensichtlich ein Widerspruch dar.
  \end{proof*}
\end{theorem*}
Es zeigt sich also, dass der minimale Eigenwert von $Q_\ell^\dagger Q_\ell$ gleich $0$ ist und der minimale Eigenwert von $H_\ell$ gleich $-\eta_\ell$ ist.
Somit haben wir von jedem $H_\ell$ die Eigenfunktionen zum niedrigsten Eigenwert
\[
  E_{\ell+1} = -\frac{Z^2 e^4 \mu}{2 \hbar (\ell+1)^2} .
\]

\begin{notice*}[Frage:]
  Was ist mt dem Rest?
  
  Sei $u_{n,\ell+1}$ ($n\geq \ell+2$) Eigenfunktion von $H_{\ell+1}$:
  \[
    H_{\ell+1} u_{n,\ell+1} = \mathcal{E}_n u_{n,\ell+1}
  \]
  Zunächst definieren wir:
  \[
    u_{n,\ell+1}' \coloneq Q_\ell^\dagger u_{n,\ell+1}.
  \] 
  Wirkt $H_\ell$ auf $u_{n,\ell+1}'$ folgt:
  %
  \begin{align*}
    H_\ell u_{n,\ell+1}' &= \left(Q_\ell^\dagger Q_\ell - \eta_\ell\right)Q_\ell^\dagger u_{n,\ell+1} \\
    &= Q_\ell^\dagger \left(Q_\ell^\dagger Q_\ell - \eta_\ell\right) u_{n,\ell+1} \\
    &= Q_\ell^\dagger H_{\ell+1} u_{n,\ell+1} \\
    &= \mathcal{E}_n  u_{n,\ell+1}'.
  \end{align*}
  %
  Es zeigt sich also, dass $Q_\ell^\dagger u_{n,\ell+1}$ eine Eigenfunktion von $H_\ell$ zum Eigenwert $E_n$ ist. Weiterhin zeigt sich, dass es ein $c_{n,\ell}\in \mathbb{C}$ gibt, so dass 
  \[
    Q_\ell^\dagger u_{n,\ell+1} = c_{n,\ell} u_{n,\ell}.
  \] 
  Fü die Normierung gilt:
  %
  \begin{align*}
    \Braket{ u_{n,\ell+1}| Q_\ell Q_\ell^\dagger |u_{n,\ell+1}} &= |c_{n,\ell}|^2 \underbrace{ \Braket{u_{n,\ell} |u_{n,\ell} } }_{=1} \\
    &= \Braket{ u_{n,\ell+1}| H_{\ell+1} + \eta_\ell | u_{n,\ell+1}} \\
    &= \left( \mathcal{E}_n - \eta_\ell \right) \underbrace{ \Braket{u_{n,\ell+1}|u_{n,\ell+1}}}_{=1}.
  \end{align*}
  %
  Somit folgt also für $c_{n,\ell}$:
  %
  \begin{align*}
    |c_{n,\ell}|^2 &= \left(\mathcal{E}_n -\eta_\ell\right) \\
    \implies c_{n,\ell} &= \sqrt{\mathcal{E}_n -\eta_\ell}.
  \end{align*}
  %
  Man erhält also folgende Leiteroperation:
  \[
    \boxed{Q_\ell^\dagger u_{n,\ell+1} = \sqrt{E_n -\eta_\ell} u_{n,\ell}}
  \]
  Diese ist für die folgende Darstellung des Spektrums des Wasserstoffatoms von Interesse.
\end{notice*}

\begin{figure}[H]
  \centering
  \begin{pspicture}(-1,1)(6,-6.5)
  \psaxes[labels=none,ticks=none]{->}(0,0)(-1,1)(6,-6.5)
  \psline(0,0)(-1,1)
  \uput{0.6}[120](0,0){\color{DimGray} $\ell$}
  \uput{0.6}[150](0,0){\color{DimGray} $n$}
  \psxTick[labelsep=-20pt](1){\color{DimGray} 0}
  \psxTick[labelsep=-20pt](2.5){\color{DimGray} 1}
  \psxTick[labelsep=-20pt](4){\color{DimGray} 2}
  \psxTick[labelsep=-20pt](5.5){\color{DimGray} 3}
  \psyTick(-1){\color{DimGray} 4}
  \psyTick(-2.5){\color{DimGray} 3}
  \psyTick(-4){\color{DimGray} 2}
  \psyTick(-6){\color{DimGray} 1}
  
  % Einfache Entartung
  \rput(0,0){
    \psline[linecolor=DarkOrange3](0.5,-6)(1.5,-6)
    \uput[0](1.5,-6){\color{DarkOrange3} $E_1 = - \kappa$}
    \uput[90](1,-6){\color{DimGray} $S_1$}
    \psdot[linecolor=Purple,dotstyle=x,dotscale=2](1,-6)
    
    \psline[linecolor=MidnightBlue](0.5,-4)(1.5,-4)
    \uput[90](1,-4){\color{DimGray} $S_2$}
    \psdot[linecolor=Purple,dotstyle=x,dotscale=2](1,-4)
    
    \psline[linecolor=MidnightBlue](0.5,-2.5)(1.5,-2.5)
    \uput[90](1,-2.5){\color{DimGray} $S_3$}
    \psdot[linecolor=Purple,dotstyle=x,dotscale=2](1,-2.5)
    
    \psline[linecolor=MidnightBlue](0.5,-1)(1.5,-1)
    \psdot[linecolor=Purple,dotstyle=x,dotscale=2](1,-1)
  }
  
  % Dreifache Entartung
  \rput(1.5,0){
    \psline[linecolor=DarkOrange3](0.5,-4)(1.5,-4)
    \uput[0](1.5,-4){\color{DarkOrange3} $E_2 = - \kappa/4$}
    \uput[90](1,-4){\color{DimGray} $P_2$}
    \psdots[linecolor=Purple,dotstyle=x,dotscale=2](0.75,-4)(1,-4)(1.25,-4)
    
    \psline[linecolor=MidnightBlue](0.5,-2.5)(1.5,-2.5)
    \uput[90](1,-2.5){\color{DimGray} $P_3$}
    \psdots[linecolor=Purple,dotstyle=x,dotscale=2](0.75,-2.5)(1,-2.5)(1.25,-2.5)
    
    \psline[linecolor=MidnightBlue](0.5,-1)(1.5,-1)
    \psdots[linecolor=Purple,dotstyle=x,dotscale=2](0.75,-1)(1,-1)(1.25,-1)
  }
  
  % Fünffache Entartung
  \rput(3,0){
    \psline[linecolor=DarkOrange3](0.5,-2.5)(1.5,-2.5)
    \uput[0](1.5,-2.5){\color{DarkOrange3} $E_3 = - \kappa/9$}
    \uput[90](1,-2.5){\color{DimGray} $d_3$}
    \psdots[linecolor=Purple,dotstyle=x,dotscale=2](0.6666,-2.5)(0.8333,-2.5)(1,-2.5)(1.1666,-2.5)(1.3333,-2.5)
    
    \psline[linecolor=MidnightBlue](0.5,-1)(1.5,-1)
    \psdots[linecolor=Purple,dotstyle=x,dotscale=2](0.6666,-1)(0.8333,-1)(1,-1)(1.1666,-1)(1.3333,-1)
  }
  
  % Siebenfache Entartung
  \rput(4.5,0){
    \psline[linecolor=DarkOrange3](0.5,-1)(1.5,-1)
    \uput[0](1.5,-1){\color{DarkOrange3} $E_4 = - \kappa/16$}
  }
  
  % Q und Q^\dagger
  \rput(1.5,-4){
    \pnode(-0.1,0.2){A1}
    \pnode(-0.1,-0.2){A2}
    \pnode(0.6,0.2){B1}
    \pnode(0.6,-0.2){B2}
    \ncarc[arcangle=30]{<-}{A1}{B1} %\naput{\color{DimGray} $Q_0^\dagger$}
    \uput{0.2}[90](0.25,0.2){\color{DimGray} $Q_0^\dagger$}
    \ncarc[arcangle=-30]{->}{A2}{B2} %\nbput{\color{DimGray} $Q_0$}
    \uput{0.2}[-90](0.25,-0.2){\color{DimGray} $Q_0$}
  }
  \rput(1.5,-2.5){
    \pnode(-0.1,0.2){A1}
    \pnode(0.6,0.2){B1}
    \ncarc[arcangle=30]{<-}{A1}{B1} %\naput{\color{DimGray} $Q_0^\dagger$}
    \uput{0.2}[90](0.25,0.2){\color{DimGray} $Q_0^\dagger$}
  }
  \rput(3,-2.5){
    \pnode(-0.1,0.2){A1}
    \pnode(-0.1,-0.2){A2}
    \pnode(0.6,0.2){B1}
    \pnode(0.6,-0.2){B2}
    \ncarc[arcangle=30]{<-}{A1}{B1} %\naput{\color{DimGray} $Q_1^\dagger$}
    \uput{0.2}[90](0.25,0.2){\color{DimGray} $Q_1^\dagger$}
    \ncarc[arcangle=-30]{->}{A2}{B2} %\nbput{\color{DimGray} $Q_1$}
    \uput{0.2}[-90](0.25,-0.2){\color{DimGray} $Q_1$}
  }
  \rput(1.5,-1){
    \pnode(-0.1,0.2){A1}
    \pnode(0.6,0.2){B1}
    \ncarc[arcangle=30]{<-}{A1}{B1} %\naput{\color{DimGray} $Q_0^\dagger$}
    \uput{0.2}[90](0.25,0.2){\color{DimGray} $Q_0^\dagger$}
  }
  \rput(3,-1){
    \pnode(-0.1,0.2){A1}
    \pnode(0.6,0.2){B1}
    \ncarc[arcangle=30]{<-}{A1}{B1} %\naput{\color{DimGray} $Q_1^\dagger$}
    \uput{0.2}[90](0.25,0.2){\color{DimGray} $Q_1^\dagger$}
  }
  \rput(4.5,-1){
    \pnode(-0.1,0.2){A1}
    \pnode(-0.1,-0.2){A2}
    \pnode(0.6,0.2){B1}
    \pnode(0.6,-0.2){B2}
    \ncarc[arcangle=30]{<-}{A1}{B1} %\naput{\color{DimGray} $Q_2^\dagger$}
    \uput{0.2}[90](0.25,0.2){\color{DimGray} $Q_2^\dagger$}
    \ncarc[arcangle=-30]{->}{A2}{B2} %\nbput{\color{DimGray} $Q_2$}
    \uput{0.2}[-90](0.25,-0.2){\color{DimGray} $Q_2$}
  }
  \end{pspicture}
\end{figure}

\begin{notice*}[Frage:]
  Haben wir alle Energieeigenwerte abgedeckt? Ja!
  \[
    Q_\ell u_{n,\ell} \sim u_{n,\ell+1}
  \]
  \begin{proof*}
    Wir nehmen an, es existiere ein Energieeigenwert $E_x\in \mathbb{R}$ so dass:
    \[
      E_1<E_x<E_2.
    \] 
    Damit lautet die Eigenwertgleichung:
    \[
      H_0 u_{x,0} = E_x u(x,0)
    \]
    Damit wäre $Q_0 u_{x,0}$ eine Eigenfunktion von $H_1$ zum Eigenwert $E_x$. Dies würde aber bedeuten, dass $E_x \geq E_2$ ist, dies ist aber ein Widerspruch. Folglich kann solch ein Energieeigenwert nicht existieren.
  \end{proof*}
\end{notice*}

Bemerkungen:
\begin{enum-arab}
  \item Wegen $|m|\leq \ell$ ist für ein festes $\ell$ jedes Niveau $(2\ell+1)$-fach entartet
  \item Die Gesamtentartung des Niveaus $u$ ist:
  \[
    g(u) = \sum\limits_{\ell=0}^{n-1} (2\ell+1) = n^2
  \]
  \item Die Entartung mit verschiedenen $\ell$ liegt an einer zusätzlichen Symmetrie (klass. Erhaltungsgröße: Runge-Lenz-Vektor)
  \item Es gibt unendlich viele gebundene Zustände
\end{enum-arab}
