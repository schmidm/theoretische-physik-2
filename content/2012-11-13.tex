% Henri Menke, Michael Schmid, Marcel Klett, 2012 Universität Stuttgart.
%
% Dieses Werk ist unter einer Creative Commons Lizenz vom Typ
% Namensnennung - Nicht-kommerziell - Weitergabe unter gleichen Bedingungen 3.0 Deutschland
% zugänglich. Um eine Kopie dieser Lizenz einzusehen, konsultieren Sie
% http://creativecommons.org/licenses/by-nc-sa/3.0/de/ oder wenden Sie sich
% brieflich an Creative Commons, 444 Castro Street, Suite 900, Mountain View,
% California, 94041, USA.

%
%    Vorlesung 13.11.2012
%
\renewcommand{\printfile}{2012-11-13}

Ortsdarstellung des Impulsoperators
%
\begin{align*}
  \text{Wdh:} \qquad
  \frac{\mathrm{d}}{\mathrm{d}t} \Braket{\hat{x}}_{\psi(x)} &\overset{!}{=} \frac{1}{m} \Braket{\hat{p}}_{\psi(x)} \\
  \implies \quad \left[ \hat{x},\hat{p} \right] &= \mathrm{i}\hbar \\
  \hat{x}\hat{p} - \hat{p}\hat{x} &= \mathrm{i}\hbar && \Big| \Ket{x} \quad \Big| \Bra{x'} \\
  \Braket{x'|\hat{x}\hat{p} - \hat{p}\hat{x}|x} &= \Braket{x' | \mathrm{i}\hbar | x} \\
  (x' - x) \underbrace{\Braket{x' | \hat{p} | x}}_{= \mathrm{i} \hbar g(x'-x)} &= \mathrm{i} \hbar \delta (x' - x)
\intertext{Sei $x' - x = u$}
  u \, g(u) &= \delta(u)
\end{align*}
%
\begin{notice*}[Behauptung:]
  $g(u) = -\delta'(u)$
  
  \begin{proof}
    \begin{align*}
      \int u \, g(u) \, f(u) \, \mathrm{d}u
      &\overset{\text{Beh.}}{=} - \int u \, f(u) \, \delta'(u) \, \mathrm{d}u \\
      &= -\left[ u \, f(u) \, \delta(u) - \int \Big(u \, f(u)\Big)' \, \delta(u) \, \mathrm{d}u \right] \\
      &= \int \Big(u \, f(u)\Big)' \, \delta(u) \, \mathrm{d}u \\
      &= \int \Big(f(u) - u \, f'(u)\Big) \, \delta(u) \, \mathrm{d}u \\
      &= f(0) + 0 \\
      &= \int f(u) \, \delta(u) \, \mathrm{d}u
    \end{align*}
  \end{proof}
\end{notice*}
%
Damit folgt
%
\begin{align*}
  \Braket{x' | \hat{p} | x} &= \mathrm{i} \hbar \delta'(x' - x)
\end{align*}
%
Wirkung auf einen beliebigen Zustand $\Ket{\psi}$
%
\begin{align*}
  \hat{p} \Ket{\psi}
  &= \hat{p} \mathds{1} \Ket{\psi} \\
  &= \hat{p} \int \mathrm{d}x \Ket{x} \Braket{x|\psi} && \Big| \Bra{x'} \\
  \Braket{x' | \hat{p} | \psi}
  &= \Bra{x'} \hat{p} \int \mathrm{d}x \Ket{x} \Braket{x|\psi} \\
  &= \int \mathrm{d}x \; \mathrm{i} \hbar \delta'(x' - x) \, \psi(x) \\
  &= \mathrm{i} \hbar \delta(x - x') \, \psi(x) - \int \mathrm{d}x \; \mathrm{i} \hbar \delta(x - x') \, \psi'(x) \\
  &= - \mathrm{i} \hbar \int \mathrm{d}x \; \delta(x - x') \, \psi'(x) \\
  &= - \mathrm{i} \hbar \psi'(x') \\
  &= - \mathrm{i} \hbar \partial_x \psi(x) \Big|_{x=x'}
\end{align*}
%
Zur Verdeutlichung
%
\begin{align*}
  \boxed{\Braket{x | \hat{p} | \psi} = - \mathrm{i} \hbar \partial_x \psi(x)}
\end{align*}
%
d.h. in der Ortsdarstellung gilt
%
\begin{align*}
  \hat{p} = - \mathrm{i} \hbar \partial_x
\end{align*}

\subsection{Schrödingergleichung in Ortsdarstellung}

\begin{align*}
  \mathrm{i} \hbar \partial_t \Ket{\psi} &= H \Ket{\psi} \\
  &= \left( \frac{\hat{p}^2}{2 m} + V(\hat{x}) \right) \Ket{\psi} && \Big| \Bra{x} \\
  \Aboxed{\mathrm{i} \hbar \partial_t \psi(x,t) &= \left( -\frac{\hbar^2}{2 m} \partial_x^2 + V(x) \right) \psi(x,t)}
\end{align*}
%
für $V(x,t)$ zeitunabhängig: $V(x)$

Lösungsansatz:
%
\begin{align*}
  \psi(x,t) = \exp\left( -\frac{\mathrm{i}}{\hbar} E t \right) \, \psi(x)
\end{align*}
%
Dies ist nicht die allgemeinste Lösung und gilt nur für stationäre Zustände.
%
\begin{align*}
  E \psi(x) &= \underbrace{\left( -\frac{\hbar^2}{2 m} \partial_x^2 + V(x) \right)}_{\hat{H}} \psi(x)
\end{align*}
%
Dies ist eine Eigenwertgleichung. Nur für bestimmte Werte von $E$ gibt es eine normierbare Lösung. Daraus folgt die \acct{Quantisierung der Energie}.

\subsection{Beispiel: Unendlicher Potentialtopf}

\begin{figure}[H]
  \centering
  \begin{pspicture}(-1.2,-0.2)(1.2,2)
    \psaxes[ticks=none,labels=none,yAxis=false](0,0)(-1.2,-0.2)(1.2,2)
    \psframe[linestyle=none,fillstyle=hlines,hatchcolor=DimGray,hatchsep=3pt](-1.2,0)(-1,2)
    \psframe[linestyle=none,fillstyle=hlines,hatchcolor=DimGray,hatchsep=3pt](1,0)(1.2,2)
    \psline(-1,-0.2)(-1,2)
    \psline(0,-0.1)(0,0.1)
    \psline(1,-0.2)(1,2)
    \uput[-90](-1,-0.1){\color{DimGray} $-a$}
    \uput[-90](0,-0.1){\color{DimGray} $0$}
    \uput[-90](1,-0.1){\color{DimGray} $a$}
  \end{pspicture}
\end{figure}
%
\begin{align*}
  V(x) =
  \begin{dcases}
    \infty & \text{für } |x|>a \\
    0 & \text{für } |x|\leq a
  \end{dcases}
\end{align*}

\minisec{klassisch:}

\begin{figure}[H]
  \centering
  \begin{pspicture}(-0.2,-1.2)(3,1.2)
    \psaxes[ticks=none,labels=none,arrows=->](0,0)(-0.2,-1.2)(3,1.3)[\color{DimGray} $t$,0][\color{DimGray} $x(t)$,90]
    \psline(-0.2,1)(3,1)
    \psline(-0.2,-1)(3,-1)
    \uput[180](-0.2,-1){\color{DimGray} $-a$}
    \uput[180](-0.2,0){\color{DimGray} $0$}
    \uput[180](-0.2,1){\color{DimGray} $a$}
    \psline[linecolor=DarkOrange3](0,0.5)(0.5,1)(2.5,-1)(3,-0.5)
    \psdot*[linecolor=DarkOrange3](0,0.5)
  \end{pspicture}
\end{figure}

\minisec{quantal:}
%
\begin{item-triangle}
  \item Stationäre Zustände
  %
  \begin{align*}
    \left( - \frac{\hbar^2}{2 m} \partial_x^2 + 0 \right) \phi(x) \overset{!}{=} E_n \phi(x)
  \end{align*}
  %
  mit den Randbedingungen
  %
  \begin{item-triangle}
    \item $\phi(\pm a)=0$ (denn $\phi(|x|\leq a) = 0 $)
    
    \item Stetigkeit:
    %
    \begin{align*}
      \partial_x^2 \phi(x) = -\varepsilon \, \phi(x) \quad \text{mit } \varepsilon = \frac{2 m E}{\hbar_2}
    \end{align*}
  \end{item-triangle}
\end{item-triangle}

\minisec{Lösung:}

\begin{enum-roman}
  \item symmetrische $\phi(x)$
  %
  \begin{align*}
    \phi(x) = A \cos kx \; , \quad k = \frac{\pi}{2 a} (1 + n) , \; n = 0,2,4,\ldots
  \end{align*}
  %
  \begin{figure}[H]
    \centering
    \begin{pspicture}(-1.2,-1)(1.2,1)
      \psaxes[ticks=none,labels=none,yAxis=false](0,0)(-1.2,-1)(1.2,1)
      \psframe[linestyle=none,fillstyle=hlines,hatchcolor=DimGray,hatchsep=3pt](-1.2,-1)(-1,1)
      \psframe[linestyle=none,fillstyle=hlines,hatchcolor=DimGray,hatchsep=3pt](1,-1)(1.2,1)
      \psline(-1,-1)(-1,1)
      \psline(1,-1)(1,1)
      \psplot[linecolor=DarkOrange3]{-1}{1}{0.7*cos(1.57079632679*x)}
      \psplot[linecolor=MidnightBlue]{-1}{1}{0.7*cos(4.71238898038*x)}
    \end{pspicture}
  \end{figure}
  %
  Normierung:
  %
  \begin{align*}
    \int\limits_{-a}^{+a} |\phi(x)|^2 \, \mathrm{d}x \overset{!}{=} 1
  \end{align*}
  %
  symmetrische stationäre Zustände
  %
  \begin{align*}
    \phi_n(x) = \frac{1}{\sqrt{a}} \cos k_n x \; , \quad k_n = \frac{\pi}{2 n} (1 + n)
  \end{align*}
  %
  mit der Energie
  %
  \begin{align*}
    E_n = \frac{\hbar^2}{2 m} \varepsilon_n = \frac{\hbar^2}{2 m} k_n^2 = \frac{\hbar^2}{2 m} \left( \frac{\pi}{2 n} \right)^2 (1 + n)^2
  \end{align*}
  
  \item antisymmetrisch
  %
  \begin{align*}
    \phi_n(x) = \frac{1}{\sqrt{a}} \sin kx \; , \quad k = \frac{\pi}{2 a} (1 + n) , \; n = 1,3,5,\ldots
  \end{align*}
  %
  mit der Energie
  %
  \begin{align*}
    E_n = \frac{\hbar^2}{2 m} \left( \frac{\pi}{2 a} \right)^2 (1 + n)^2
  \end{align*}
\end{enum-roman}

Die Energieeigenwerte sind also quantisiert
%
\begin{figure}[H]
  \centering
  \begin{pspicture}(-0.5,-0.2)(0.5,3)
    \psaxes[ticks=none,labels=none,arrows=->,xAxis=false](0,0)(-0.5,-0.2)(0.5,3)[\color{DimGray},0][\color{DimGray} $E$,180]
    \psline(-0.5,0)(0.5,0)
    \psline(-0.1,0.5)(0.1,0.5)\uput[0](0,0.5){\color{DimGray} $1$}
    \psline(-0.1,2)(0.1,2)\uput[0](0,2){\color{DimGray} $4$}
    \uput[0](0,3){\color{DimGray} $\uparrow 9,16,\ldots$}
  \end{pspicture}
\end{figure}

\minisec{Dynamik}

Sei nun $\psi(x,t_0)$ gegeben
%
\begin{notice*}[Ansatz:]
  \begin{align*}
    \psi(x,t) = \sum\limits_{n=0}^{\infty} c_n(t) \, \phi_n(x)
  \end{align*}
  %
  wobei $\phi_n(x)$ die Eigenfunktionen von $\hat{H}$ zu $E_n$ sind.
\end{notice*}
%
In die zeitabhängige Schrödingergleichung eingesetzt.
%
\begin{align*}
  \mathrm{i} \hbar \partial_t \psi(x,t) &= \left( -\frac{\hbar^2}{2 m} \partial_x^2 \right) \psi(x,t) \\
  \sum\limits_{n=0}^{\infty} \mathrm{i} \hbar \Big( \partial_t c_n(t) \Big) \phi_n(x)
  &= \left( -\frac{\hbar^2}{2 m} \partial_x^2 \right) \sum\limits_{n=0}^{\infty} c_n(t) \phi_n(x) \\
  &= \sum\limits_{n=0}^{\infty} c_n(t) \, E_n \, \phi_n(x)
\intertext{Die $\phi_n$ sind ein vollständig orthonormiertes System}
  \sum\limits_{n=0}^{\infty} \mathrm{i} \hbar \Big( \partial_t c_n(t) \Big) \phi_n(x)
  &= \sum\limits_{n=0}^{\infty} c_n(t) \, E_n \, \phi_n(x) && \left| \int \phi_m^*(x) \, \mathrm{d}x \right. \\
  \mathrm{i} \hbar \partial_t c_m(t) &= E_m \, c_m(t)
\end{align*}
%
mit der Zeitentwicklung
%
\begin{align*}
  c_m(t) = \exp\left( -\frac{\mathrm{i}}{\hbar} E_m t \right) \, c_m(0)
\end{align*}
%
und
%
\begin{align*}
  \int\limits_{-a}^{+a} \mathrm{d}x \, \phi_m^*(x) \, \psi(x,t_0) = c_m(0)
\end{align*}
%
folgt für die Lösung $\psi(x,t)$
%
\begin{align*}
  \boxed{\psi(x,t) = \sum\limits_{n=0}^{\infty} c_n(0) \, \exp\left( -\frac{\mathrm{i}}{\hbar} E_n t \right) \, \phi_n(x)}
\end{align*}

\minisec{Zusammenfassung}

Energiedarstellung:
%
\begin{align*}
  \hat{H} \Ket{\phi_n} = E_n \Ket{\phi_n}
\end{align*}
%
mit orthonormierten Zuständen $\phi_n$, d.h. $\Braket{\phi_m|\phi_n} = \delta_{mn}$
%
\begin{align*}
  \Ket{\psi(t)} = \sum\limits_{n=0}^{\infty} c_n(t) \, \Ket{\phi_n}
\end{align*}
%
Eine Energiemessung liefert den Energieeigenwert $E_n$ mit der Wahrscheinlichkeit
%
\begin{align*}
  \mathrm{prob}\left[ \hat{H} \hateq E_n \Big| \Ket{\psi(t)} \right]
  &= \left| \Braket{\phi_n|\psi(t)} \right|^2 \\
  &= \left| c_n(t) \right|^2 \\
  &= \left| c_n(0) \right|^2 \\
  &= \left|\Braket{\phi_n|\psi(0)}\right|^2 \\
  &= \left| \int \mathrm{d}x \, \phi_n^*(x) \, \psi(x,0) \right|^2
\end{align*}
%
damit kann man schreiben
%
\begin{align*}
  \psi(x,t)
  &= \sum\limits_{n=0}^{\infty} \exp\left( -\frac{\mathrm{i}}{\hbar} E_n t \right) \, c_n(0) \, \phi_n(x) \\
  &= \sum\limits_{n=0}^{\infty} \exp\left( -\frac{\mathrm{i}}{\hbar} E_n t \right) \, \left( \int \mathrm{d}x' \, \phi_n^*(x') \, \psi(x',0) \right) \, \phi_n(x) \\
  &= \int \mathrm{d}x' \, \sum\limits_{n=0}^{\infty} \exp\left( -\frac{\mathrm{i}}{\hbar} E_n t \right) \, \phi_n^*(x') \,  \phi_n(x) \, \psi(x',0) \\
  &= \int \mathrm{d}x' \, U(x,t,x',0) \, \psi(x',0)
\end{align*}
%
Vergleiche:
%
\begin{align*}
  \Ket{\psi(t)} = U(t,t_0=0) \Ket{\psi(t_0=0)}
\end{align*}
%
\begin{notice*}
  zur Gleichung
  %
  \begin{align*}
    \psi(x,t) &= \int \mathrm{d}x' \, \sum\limits_{n=0}^{\infty} \exp\left( -\frac{\mathrm{i}}{\hbar} E_n t \right) \, \phi_n^*(x') \,  \phi_n(x) \, \psi(x',0)
  \end{align*}
  %
  In dieser Gleichung gilt es zu bemerken, dass $\psi(x,t)$ den Zustand zum Zeitpunkt $t$ und $\psi(x',0)$ den Anfangszustand verkörpern. Dabei ist
  %
  \begin{align*}
    \sum\limits_{n=0}^{\infty} \exp\left( -\frac{\mathrm{i}}{\hbar} E_n t \right) \, \phi_n^*(x') \,  \phi_n(x) \eqcolon U(x,t,x',0)
  \end{align*}
  %
  der Zeitentwicklungsoperator in Ortsdarstellung. Diese Darstellung wird auch \acct[0]{Spektraldarstellung der Zeitentwicklungsoperators} genannt.
\end{notice*}
