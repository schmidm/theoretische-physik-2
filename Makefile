# # # # # # # # # # # # # # # # # #
# # # DO NOT EDIT THIS FILE # # # #
# # # # # # # # # # # # # # # # # #

FILE = Theo2
HARMONICS = harmonics
LATEX = dvilualatex -enable-write18 -shell-escape -interaction=nonstopmode
PDFLATEX = lualatex -enable-write18 -shell-escape -interaction=nonstopmode
DVIPS = dvips
PS2PDF = ps2pdf
BIBER = biber
INDEX = makeindex
VIEWER = xdg-open

all: complete booklet

quick: harmonic
	$(LATEX) "$(FILE).tex"
	$(DVIPS) "$(FILE).dvi"
	$(PS2PDF) "$(FILE).ps"

harmonic:
	$(MAKE) -C "$(HARMONICS)" all

complete: harmonic
	$(LATEX) "$(FILE).tex"
	$(BIBER) "$(FILE)"
	$(INDEX) "$(FILE).idx"
	$(LATEX) "$(FILE).tex"
	$(LATEX) "$(FILE).tex"
	$(DVIPS) "$(FILE).dvi"
	$(PS2PDF) "$(FILE).ps"

booklet: complete
	$(PDFLATEX) "$(FILE)-booklet.tex"

view:
	$(VIEWER) "$(FILE).pdf" &

# All clean commands will preserve output
.PHONY : clean
clean:
	$(RM) *.dvi *.ps
	$(MAKE) -C "$(HARMONCIS)" clean

distclean:
	$(RM) *.aux *.bbl *.bcf *.blg *.dvi *.lof *.log *.fdb_latexmk *.fls *.idx *.ilg *.ind *.out *.ps *.run.xml *.thm *.toc
	$(MAKE) -C "$(HARMONICS)" distclean
